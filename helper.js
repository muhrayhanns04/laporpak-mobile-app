export const capitalize = value => {
    const str2 =
        value?.toString().charAt(0).toUpperCase() + value?.toString().slice(1);
    return str2;
};

export const wait = timeout => {
    return new Promise(resolve => setTimeout(resolve, timeout));
};

export const nameCencor = (name, separator = '*') => {
    return name?.replace(/(?<=\S)\S(?=(\S|\S ))/g, separator);
};

export const dateConvertedV1 = value => {
    const TODAY = new Date(value);
    const YEAR = TODAY?.getFullYear();
    const MONTH = ('0' + (TODAY?.getMonth() + 1)).slice(-2);
    const DATE = ('0' + TODAY?.getDate()).slice(-2);
    return `${DATE}/${MONTH}/${YEAR}`;
};
export const dateConvertedV2 = value => {
    const TODAY = new Date(value);
    const YEAR = TODAY?.getFullYear();
    const MONTH = ('0' + (TODAY?.getMonth() + 1)).slice(-2);
    const DATE = ('0' + TODAY?.getDate()).slice(-2);
    return `${YEAR}/${MONTH}/${DATE}`;
};

export function formatDate(date) {
    var months = [
        'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember',
    ];

    const PAYMENTDATE = new Date(date && date);
    const YEAR = PAYMENTDATE.getFullYear();
    const MONTH = months[PAYMENTDATE.getMonth()];
    const DATE = ('0' + PAYMENTDATE.getDate()).slice(-2);
    let RESULT = `${DATE} ${MONTH}, ${YEAR}`;
    return RESULT;
}

export const formatAMPM = date => {
    let hours = date?.getHours();
    let minutes = date?.getMinutes();
    let ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12;
    minutes = minutes?.toString().padStart(2, '0');
    let strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
};

export const errorsValidation = user => {
    const errors = {};

    if (!user.name) {
        errors.name = 'Nama Lengkap wajib di isi tidak boleh kosong';
    }
    if (user.name.length > 1 && user.name.length < 4) {
        errors.name = 'Nama Lengkap tidak boleh kurang dari 4';
    }
    if (!user.telp) {
        errors.telp = 'No. Telp wajib di isi tidak boleh kosong';
    }
    if (user.telp.length > 1 && user.telp.length < 12) {
        errors.telp = 'No. Telp anda kurang dari 12 angka';
    }
    if (!user.nik) {
        errors.nik = 'NIK wajib di isi tidak boleh kosong';
    }
    if (user.nik.length > 1 && user.nik.length < 16) {
        errors.nik = `Panjang NIK tidak boleh KURANG DARI 16 angka`;
    }
    if (user.nik.length > 16) {
        errors.nik = `Panjang NIK tidak boleh LEBIH dari 16 angka`;
    }
    if (!user.username) {
        errors.username = 'Nama Pengguna di isi tidak boleh kosong';
    }
    if (user.username.length > 1 && user.username.length <= 3) {
        errors.username = 'Nama Pengguna tidak boleh kurang dari 4';
    }
    if (!user.password) {
        errors.password = 'Password tidak boleh kurang dari 8 karakter';
    }
    if (user.password.length > 1 && user.password.length < 8) {
        errors.password = 'Password tidak boleh kurang dari 8 karakter';
    }

    return errors;
};
