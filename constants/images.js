const CAROUSELA = require('../assets/illustrations/carousel/carousel-1.png');
const CAROUSELB = require('../assets/illustrations/carousel/carousel-2.png');
const CAROUSELC = require('../assets/illustrations/carousel/carousel-3.png');
import LOGIN from '../assets/illustrations/login.svg';
import REGISTER from '../assets/illustrations/register.svg';
export default {
    CAROUSELA,
    CAROUSELB,
    CAROUSELC,
    LOGIN,
    REGISTER,
};
