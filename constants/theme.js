import { Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');

export const COLORS = {
    BLACK: '#25282B',

    RED600: '#E52A34',
    GREEN800: '#0F752E',
    GREEN700: '#1C9444',
    GREEN600: '#31B057',
    GREEN500: '#51C273',
    GREEN100: '#E1F7E8',
    YELLOW: '#FBE72F',
    BROWN: '#642C14',
    SILVER: '#9595A7',

    WHITE: '#fff',
    WHITE2: '#F9F9F9',
    WHITE3: '#E0E0E0',
    GRAY: '#828282',
    GRAY2: '#F8F8F8',
    GRAY3: '#F5F5F5',
    GRAY4: '#F2F2F2',
    GRAY5: '#D3D3D3',
    GRAY6: '#C4C4C4',
    GRAY7: '#F6F6F6',
    GRAY8: '#BDBDBD',
    lightGRAY: '#DCE0EC',
    lightGRAY2: '#757575',
    lightGRAY3: '#B9C1D9',
    lightGRAY3: '#E8E8E8',
    lightGRAY4: '#FCFCFC',

    TRANSPARENTBLACK1: 'rgba(2, 2, 2, 0.1)',
    TRANSPARENTBLACK3: 'rgba(2, 2, 2, 0.3)',
    TRANSPARENTBLACK5: 'rgba(2, 2, 2, 0.5)',
    TRANSPARENTBLACK7: 'rgba(2, 2, 2, 0.7)',
    TRANSPARENTBLACK9: 'rgba(2, 2, 2, 0.9)',

    TRANSPARENTGRAY: 'rgba(77,77,77, 0.8)',
    TRANSPARENTDARKGRAY: 'rgba(20,20,20, 0.9)',

    TRANSPARENT: 'transparent',
};

export const SIZES = {
    // global sizes
    BASE: 10,
    FONT: 14,
    RADIUS: 12,
    PADDING: 24,

    // FONT sizes
    largeTitle: 48,
    h32: 32,
    h1: 30,
    h2: 28,
    h24: 24,
    h3: 22,
    h4: 16,
    h5: 18,
    h6: 14,
    h13: 13,
    h7: 12,
    h8: 10,
    bodyLarge: 40,
    body1: 30,
    body2: 28,
    body3: 22,
    body4: 16,
    body5: 14,
    body13: 13,
    body6: 12,
    body7: 10,

    // app dimensions
    width,
    height,
};

const REGULAR = 'Inter-Regular';
const MEDIUM = 'Inter-Medium';
const BOLD = 'Inter-Bold';
const SEMIBOLD = 'Inter-SemiBold';

export const FONTS = {
    largeTitle: {
        fontFamily: `${SEMIBOLD}`,
        fontSize: SIZES.largeTitle,
    },
    largeTitleBold: {
        fontFamily: `${BOLD}`,
        fontSize: SIZES.largeTitle,
    },
    h32Bold: {
        fontFamily: `${BOLD}`,
        fontSize: SIZES.h32,
        lineHeight: 36,
    },
    h1: {
        fontFamily: `${SEMIBOLD}`,
        fontSize: SIZES.h1,
        lineHeight: 36,
    },
    h1Bold: {
        fontFamily: `${BOLD}`,
        fontSize: SIZES.h1,
        lineHeight: 36,
    },
    h2: {
        fontFamily: `${SEMIBOLD}`,
        fontSize: SIZES.h2,
        // lineHeight: 24,
    },
    h2Bold: {
        fontFamily: `${BOLD}`,
        fontSize: SIZES.h2,
        // lineHeight: 24,
    },
    h3: {
        fontFamily: `${SEMIBOLD}`,
        fontSize: SIZES.h3,
        lineHeight: 28,
    },
    h3Bold: {
        fontFamily: `${BOLD}`,
        fontSize: SIZES.h3,
        lineHeight: 28,
    },
    h4: {
        fontFamily: `${MEDIUM}`,
        fontSize: SIZES.h4,
        lineHeight: 22,
    },
    h4Bold: {
        fontFamily: `${BOLD}`,
        fontSize: SIZES.h4,
        lineHeight: 22,
    },
    h4SemiBold: {
        fontFamily: `${SEMIBOLD}`,
        fontSize: SIZES.h4,
        lineHeight: 19.36,
    },
    h24Bold: {
        fontFamily: `${BOLD}`,
        fontSize: SIZES.h24,
        lineHeight: 26,
    },
    h24Heading: {
        fontFamily: `${BOLD}`,
        fontSize: SIZES.h24,
        lineHeight: 33,
    },
    h5: {
        fontFamily: `${SEMIBOLD}`,
        fontSize: SIZES.h5,
        lineHeight: 27,
    },
    h5Bold: {
        fontFamily: `${BOLD}`,
        fontSize: SIZES.h5,
        lineHeight: 27,
    },
    h6: {
        fontFamily: `${SEMIBOLD}`,
        fontSize: SIZES.h6,
        lineHeight: 22,
    },
    h6Bold: {
        fontFamily: `${BOLD}`,
        fontSize: SIZES.h6,
        lineHeight: 22,
    },
    h7: {
        fontFamily: `${BOLD}`,
        fontSize: SIZES.h7,
        lineHeight: 18,
    },
    h13: {
        fontFamily: `${SEMIBOLD}`,
        fontSize: SIZES.h7,
        lineHeight: 18,
    },
    h13Bold: {
        fontFamily: `${BOLD}`,
        fontSize: SIZES.h7,
        lineHeight: 18,
    },
    h8Bold: {
        fontFamily: `${BOLD}`,
        fontSize: SIZES.h8,
        lineHeight: 15,
    },
    // Body
    bodyLarge: {
        fontFamily: `${REGULAR}`,
        fontSize: SIZES.bodyLarge,
        lineHeight: 36,
    },
    body1: {
        fontFamily: `${REGULAR}`,
        fontSize: SIZES.body1,
        lineHeight: 36,
    },
    body2: {
        fontFamily: `${REGULAR}`,
        fontSize: SIZES.body2,
        lineHeight: 30,
    },
    body3: {
        fontFamily: `${REGULAR}`,
        fontSize: SIZES.body3,
        lineHeight: 22,
    },
    body4: {
        fontFamily: `${REGULAR}`,
        fontSize: SIZES.body4,
        lineHeight: 22,
    },
    body5: {
        fontFamily: `${REGULAR}`,
        fontSize: SIZES.body5,
        lineHeight: 22,
    },
    body6: {
        fontFamily: `${REGULAR}`,
        fontSize: SIZES.body6,
        lineHeight: 18,
    },
    body13: {
        fontFamily: `${REGULAR}`,
        fontSize: SIZES.body13,
        lineHeight: 18,
    },
    body7: {
        fontFamily: `${REGULAR}`,
        fontSize: SIZES.body7,
        lineHeight: 16,
    },
};

export const FLEXTYPE = {
    flexrow: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    default: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
};

const appTheme = { COLORS, FONTS, SIZES, FLEXTYPE };

export default appTheme;
