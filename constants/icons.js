import CHEVRON from '../assets/icons/chevron.svg';
import CREATE from '../assets/icons/create.svg';
import IMAGE from '../assets/icons/image.svg';
import MEMOJIEXA from '../assets/icons/memoji/ekspresi-1.svg';
import LAPORPAK from '../assets/icons/laporpak-logo.svg';
import REPORT from '../assets/icons/memoji/report.svg';
import ASPIRATION from '../assets/icons/memoji/aspiration.svg';
import THUNDER from '../assets/icons/thunder.svg';
import FULLNAME from '../assets/icons/profile-fullname.svg';
import USERNAME from '../assets/icons/profile-username.svg';
import CALL from '../assets/icons/call.svg';
import LOGOUT from '../assets/icons/logout.svg';
import FIRE from '../assets/icons/fire.svg';
import PIN from '../assets/icons/piin.svg';
import CLOCK from '../assets/icons/clock.svg';
import SPARKLES from '../assets/icons/sparkles.svg';
import BISSEP from '../assets/icons/bissep.svg';
import CHECK from '../assets/icons/check.svg';
import SCISSORS from '../assets/icons/scissors.svg';
import MAINTENANCE from '../assets/icons/maintenance.svg';
import SEEDLING from '../assets/icons/seedling.svg';
import USER from '../assets/icons/user.svg';
import SEARCH from '../assets/icons/search.svg';
import WATCH from '../assets/icons/watch.svg';
import SEARCHILLUS from '../assets/icons/search-illustration.svg';
import CALENDER from '../assets/icons/calendar.svg';

export default {
    CALENDER,
    SEARCHILLUS,
    WATCH,
    USER,
    SEARCH,
    SEEDLING,
    MAINTENANCE,
    SCISSORS,
    CHECK,
    FIRE,
    PIN,
    CLOCK,
    SPARKLES,
    BISSEP,
    THUNDER,
    FULLNAME,
    USERNAME,
    CALL,
    LOGOUT,
    CHEVRON,
    CREATE,
    IMAGE,
    MEMOJIEXA,
    LAPORPAK,
    REPORT,
    ASPIRATION,
};
