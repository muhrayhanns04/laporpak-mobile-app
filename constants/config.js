import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';

export const INSTANCE = async () => {
    const token = await AsyncStorage.getItem('@token');

    let instance = axios.create({
        baseURL: 'https://laporpakinc.solusi.vip/api',
        timeout: 1000,
        headers: { Authorization: 'Bearer ' + token },
    });

    return instance;
};

export const setBaseUrl = () => {
    // return (axios.defaults.baseURL = 'https://laporpakinc.solusi.vip/api');
    return (axios.defaults.baseURL = 'http://192.168.100.6:8000/api');
};

export const setBaseUrlV2 = () => {
    return (axios.defaults.baseURL = 'http://192.168.100.6:8000');
};

export const setDefaultHeader = async () => {
    const token = await AsyncStorage.getItem('@token');

    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
    axios.defaults.headers.common['Content-Type'] =
        'multipart/form-data; boundary=<calculated when request is sent>';
};
