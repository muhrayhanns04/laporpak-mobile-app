import React from 'react';
import {
    ScrollView,
    View,
    Text,
    SafeAreaView,
    ActivityIndicator,
    RefreshControl,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { COLORS, FONTS } from '../../constants';
import Ripple from 'react-native-material-ripple';
import { capitalize, wait } from '../../helper';
import { setBaseUrl, setDefaultHeader } from '../../constants/config';
import axios from 'axios';
import RBSheet from 'react-native-raw-bottom-sheet';
import { useNavigation } from '@react-navigation/native';
import { ReportContext } from '../../context/ReportContext';
import SheetReportFinish from './BottomSheet/SheetReportFinish';
import moment from 'moment';

const ReportFinishContainer = () => {
    const sheetResponse = React.useRef(null);
    const [refreshing, setRefreshing] = React.useState(false);
    const [propsItemFinish, setPropsItemFinish] = React.useState(null);
    const { setDateFilter, setTypeScreen, reportsFinish, setReportsFinish } =
        React.useContext(ReportContext);
    const [isLoading, setIsLoading] = React.useState(false);
    const navigation = useNavigation();

    const getReportsFinish = async () => {
        setBaseUrl();
        setDefaultHeader();
        try {
            setIsLoading(true);
            const profile = await axios.get(`/profile`);
            let item = profile.data.results;

            const response = await axios.get(`/complaints/filter/${item.nik}`, {
                params: {
                    sort: 'status',
                    value: 'selesai',
                },
            });

            setIsLoading(false);
            setReportsFinish(response.data.results);
            setDateFilter(null);
        } catch (error) {
            setIsLoading(false);
            console.log(error?.response?.data?.message, 'FINISH');
        }
    };

    function handleForPropsSheetResponse(item) {
        setPropsItemFinish(item);
        sheetResponse.current.open();
    }

    React.useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            getReportsFinish();
            setTypeScreen('finish');
        });

        return unsubscribe;
    }, [navigation]);

    React.useEffect(() => {
        getReportsFinish();
    }, []);

    const onRefresh = React.useCallback(() => {
        setRefreshing(true);
        getReportsFinish();
        wait(1200).then(() => setRefreshing(false));
    }, []);

    return isLoading ? (
        <SafeAreaView
            style={{
                flex: 1,
                backgroundColor: COLORS.WHITE,
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
            }}
        >
            <ActivityIndicator size="small" color={COLORS.GREEN600} />
            <Text
                style={{
                    ...FONTS.body6,
                    marginTop: 4,
                    color: COLORS.GREEN500,
                }}
            >
                Memuat
            </Text>
        </SafeAreaView>
    ) : (
        <SafeAreaView
            style={{
                flex: 1,
                backgroundColor: COLORS.WHITE,
                paddingHorizontal: 16,
            }}
        >
            <RBSheet
                ref={sheetResponse}
                closeOnDragDown={true}
                closeOnPressMask={true}
                keyboardAvoidingViewEnabled={true}
                height={700}
                customStyles={{
                    draggableIcon: {
                        backgroundColor: '#000',
                    },
                    container: {
                        borderTopRightRadius: 16,
                        borderTopLeftRadius: 16,
                    },
                }}
            >
                <SheetReportFinish propsItemFinish={propsItemFinish} />
            </RBSheet>

            <ScrollView
                style={{
                    flex: 1,
                }}
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{
                    backgroundColor: COLORS.WHITE,
                    paddingTop: 16,
                }}
                refreshControl={
                    <RefreshControl
                        refreshing={refreshing}
                        onRefresh={onRefresh}
                    />
                }
            >
                {reportsFinish?.map((item, index) => {
                    return (
                        <Ripple
                            onPress={() => {
                                handleForPropsSheetResponse(item);
                            }}
                        >
                            <LinearGradient
                                key={index}
                                colors={['#FFFFFF', '#E1F7E8']}
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                style={{
                                    position: 'relative',
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    borderRadius: 15,
                                    paddingVertical: 16,
                                    paddingHorizontal: 24,
                                    marginBottom: 16,
                                }}
                            >
                                <View
                                    style={{
                                        position: 'absolute',
                                        left: -4,
                                        width: 6,
                                        borderRadius: 999,
                                        height: 74,
                                        backgroundColor: COLORS.GREEN600,
                                        zIndex: 9999,
                                    }}
                                />
                                <View>
                                    <Text
                                        style={{
                                            ...FONTS.h4SemiBold,
                                            color: COLORS.GREEN800,
                                        }}
                                    >
                                        {capitalize(item?.type_of_report)}
                                    </Text>

                                    <Text
                                        style={{
                                            ...FONTS.h4SemiBold,
                                            color: COLORS.BLACK,
                                            marginVertical: 8,
                                        }}
                                        numberOfLines={1}
                                    >
                                        {item.message}
                                    </Text>

                                    <Text
                                        style={{
                                            ...FONTS.h4,
                                            color: COLORS.GRAY,
                                        }}
                                        numberOfLines={1}
                                    >
                                        {moment(item.created_at).format('L')} -{' '}
                                        {moment(item.reply?.created_at).format(
                                            'L',
                                        )}
                                    </Text>
                                </View>
                            </LinearGradient>
                        </Ripple>
                    );
                })}
            </ScrollView>
        </SafeAreaView>
    );
};

export default ReportFinishContainer;
