import React from 'react';
import { View, Text, TouchableOpacity, ScrollView } from 'react-native';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import { formatDate } from '../../helper';
import axios from 'axios';
import { setBaseUrl } from '../../constants/config';
import { ReportContext } from '../../context/ReportContext';
import { COLORS, FONTS, ICONS, SIZES } from '../../constants';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useNavigation } from '@react-navigation/native';
import Ripple from 'react-native-material-ripple';
import moment from 'moment';

const ICONSIZE = SIZES.width / 16.3;
const CIRCLESIZE = SIZES.width / 9.8;

const SubHeader = () => {
    const navigation = useNavigation();
    const [DatePickerVisibility, setDatePickerVisibility] =
        React.useState(false);
    const [isLoading, setIsLoading] = React.useState(false);
    const [result, setResult] = React.useState(null);
    const {
        dateFilter,
        setDateFilter,
        setReportsProcess,
        typeScreen,
        setReportsFinish,
    } = React.useContext(ReportContext);

    const showDatePicker = () => {
        setDatePickerVisibility(true);
    };
    const hideDatePicker = () => {
        setDatePickerVisibility(false);
    };

    const handleConfirmDate = async params => {
        setIsLoading(true);
        setBaseUrl();

        try {
            const YEAR = params?.getFullYear();
            const MONTH = ('0' + (params?.getMonth() + 1)).slice(-2);
            const DATE = ('0' + params?.getDate()).slice(-2);
            const result = YEAR + '-' + MONTH + '-' + DATE;
            setDateFilter(result);
            setResult(result);

            const profile = await axios.get(`/profile`);
            let item = profile.data.results;
            let token = await AsyncStorage.getItem('@token');

            const responseProcess = await axios.get(
                `/complaints/filter/${item.nik}`,
                {
                    params: {
                        sort: 'status_with_time',
                        value: 'proses',
                        second_value: result,
                    },
                    headers: {
                        Authorization: 'Bearer ' + token,
                    },
                },
            );

            console.log(responseProcess);

            const responseFinish = await axios.get(
                `/complaints/filter/${item.nik}`,
                {
                    params: {
                        sort: 'status_with_time',
                        value: 'selesai',
                        second_value: result,
                    },
                },
            );

            setIsLoading(false);
            if (typeScreen === 'process') {
                setReportsProcess(responseProcess.data.results);
            } else if (typeScreen === 'finish') {
                setReportsFinish(responseFinish.data.results);
            } else {
                setReportsProcess(responseProcess.data.results);
            }
            console.log(responseProcess.data.results);
            console.log(responseFinish.data.results);
        } catch (error) {
            setIsLoading(false);
            console.log(error?.response?.data?.message);
        }
        hideDatePicker();
    };

    // const getNews = async () => {
    //     setBaseUrl()
    //     try {
    //         let token = await AsyncStorage.getItem('@token');
    //         const response = axios.get("")
    //     } catch (e) {
    //         console.log(e)
    //     }
    // }

    return (
        <View
            style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                marginTop: 24,
                paddingHorizontal: 16,
            }}
        >
            <Ripple
                style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    borderWidth: 1,
                    borderColor: COLORS.GRAY5,
                    paddingVertical: 13,
                    paddingRight: 6,
                    paddingLeft: 16,
                    borderRadius: 9999,
                    justifyContent: 'space-between',
                    width: 230,
                }}
                rippleContainerBorderRadius={9999}
                onPress={() => navigation.navigate('FormReportSreen')}
            >
                <Text style={{ ...FONTS.body4, color: COLORS.GRAY8 }}>
                    Tulisakan laporan hari ini...
                </Text>

                <View
                    style={{
                        position: 'absolute',
                        width: CIRCLESIZE,
                        height: CIRCLESIZE,
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: COLORS.GREEN600,
                        borderRadius: 9999,
                        right: 5,
                    }}
                >
                    <ICONS.CREATE width={ICONSIZE} height={ICONSIZE} />
                </View>
            </Ripple>

            <View
                style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'flex-end',
                }}
            >
                <DateTimePickerModal
                    isVisible={DatePickerVisibility}
                    mode="date"
                    onConfirm={handleConfirmDate}
                    onCancel={hideDatePicker}
                />
                <TouchableOpacity
                    onPress={() => showDatePicker()}
                    style={{
                        borderColor: COLORS.GREEN800,
                        backgroundColor: COLORS.GREEN100,
                        borderWidth: 1,
                        paddingVertical: 10,
                        paddingHorizontal: 12,
                        borderRadius: 8,
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'center',
                    }}
                >
                    {dateFilter ? (
                        <Text
                            style={{
                                ...FONTS.body13,
                                color: COLORS.GREEN600,
                            }}
                        >
                            {moment(dateFilter).format('L')}
                        </Text>
                    ) : (
                        <ICONS.CALENDER style={{ color: COLORS.GREEN800 }} />
                    )}
                    <ICONS.CHEVRON
                        width={26}
                        height={26}
                        style={{
                            color: COLORS.BLACK,
                            transform: [{ rotate: '-90deg' }],
                            marginLeft: 4,
                        }}
                    />
                </TouchableOpacity>
            </View>
        </View>
    );
};

export default SubHeader;
