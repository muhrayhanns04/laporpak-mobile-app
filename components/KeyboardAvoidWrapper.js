import React from 'react';
import {
    View,
    Text,
    KeyboardAvoidingView,
    ScrollView,
    TouchableWithoutFeedback,
    Keyboard,
} from 'react-native';
import { COLORS } from '../constants';

const KeyboardAvoidWrapper = ({ children }) => {
    return (
        <KeyboardAvoidingView
            style={{ flex: 1, backgroundColor: COLORS.WHITE }}
        >
            <ScrollView>
                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                    {children}
                </TouchableWithoutFeedback>
            </ScrollView>
        </KeyboardAvoidingView>
    );
};

export default KeyboardAvoidWrapper;
