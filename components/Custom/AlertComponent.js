import React from 'react';
import { View, Text } from 'react-native';
import RBSheet from 'react-native-raw-bottom-sheet';
import { COLORS, FONTS, SIZES, ICONS } from '../../constants';
import Ripple from 'react-native-material-ripple';

const AlertComponent = ({ ref, title }) => {
    return (
        <RBSheet
            ref={ref}
            closeOnDragDown={true}
            keyboardAvoidingViewEnabled={true}
            height={300}
            customStyles={{
                wrapper: {
                    backgroundColor: COLORS.TRANSPARENT,
                    marginBottom: 50,
                    marginHorizontal: 24,
                },
                draggableIcon: {
                    backgroundColor: COLORS.GREEN600,
                },
                container: {
                    backgroundColor: COLORS.WHITE,
                    borderRadius: 16,

                    shadowColor: '#000',
                    shadowOffset: {
                        width: 0,
                        height: 2,
                    },
                    shadowOpacity: 0.25,
                    shadowRadius: 3.84,

                    elevation: 5,
                },
            }}
        >
            <View
                style={{
                    flexDirection: 'column',
                    alignItems: 'center',
                    flex: 1,
                    paddingVertical: 16,
                    paddingHorizontal: 16,
                    justifyContent: 'space-between',
                }}
            >
                <Text
                    style={{
                        ...FONTS.h4Bold,
                        textAlign: 'center',
                        width: SIZES.width / 1.2,
                        color: COLORS.BLACK,
                        textTransform: 'uppercase',
                    }}
                >
                    {title}
                </Text>

                <ICONS.MAINTENANCE width={120} height={120} />

                <Ripple
                    style={{
                        backgroundColor: COLORS.GREEN600,
                        paddingVertical: 13,
                        borderRadius: 8,
                        width: '100%',
                    }}
                    rippleColor={COLORS.WHITE}
                    rippleDuration={500}
                    onPress={() => ref.current.close()}
                >
                    <Text
                        style={{
                            ...FONTS.h13,
                            textAlign: 'center',
                            color: COLORS.WHITE,
                        }}
                    >
                        Close
                    </Text>
                </Ripple>
            </View>
        </RBSheet>
    );
};

export default AlertComponent;
