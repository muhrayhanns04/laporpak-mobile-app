import React from 'react';
import { View, Text } from 'react-native';
import { COLORS, FONTS } from '../../constants/index';

const SettingsHeader = () => {
    return (
        <View>
            <Text style={{ ...FONTS.h24Bold, color: COLORS.BLACK }}>
                Pengaturan
            </Text>
        </View>
    );
};

export default SettingsHeader;
