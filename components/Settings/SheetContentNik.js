import { BottomSheetTextInput, BottomSheetView } from '@gorhom/bottom-sheet';
import axios from 'axios';
import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableHighlight,
    Alert,
    ActivityIndicator,
} from 'react-native';
import Ripple from 'react-native-material-ripple';
import { COLORS, FONTS, SIZES, ICONS } from '../../constants';
import { UserContext } from '../../context/UserContext';

const SheetContentNik = ({ INPUTSTYLE }) => {
    const [isLoading, setIsLoading] = React.useState(false);
    const {
        location,
        setLocation,
        report,
        setReport,
        proofPhotos,
        setProofPhotos,
    } = React.useContext(UserContext);

    return (
        <BottomSheetView
            style={{
                backgroundColor: COLORS.WHITE,
                paddingHorizontal: SIZES.PADDING - 8,
                paddingBottom: 24,
                flex: 1,
                paddingTop: 16,
            }}
        >
            {/* Title */}
            <View style={{ zIndex: -1 }}>
                <Text
                    style={{
                        ...FONTS.largeTitleBold,
                        color: COLORS.BLACK,
                        marginBottom: 16,
                    }}
                >
                    Edit{'\n'}Nomer Induk Kependudukan
                </Text>

                <BottomSheetTextInput
                    placeholder="Ketik Detail Alamat"
                    style={styles.input}
                    multiline
                    value={location.addressDetails}
                    onChangeText={value =>
                        setLocation({ ...location, addressDetails: value })
                    }
                />
            </View>
        </BottomSheetView>
    );
};

export default SheetContentNik;

const styles = StyleSheet.create({
    input: {
        ...FONTS.body4,
        paddingHorizontal: 16,
        paddingLeft: 16,
        paddingRight: 42,
        backgroundColor: COLORS.GRAY7,
        borderRadius: 8,
        color: COLORS.GRAY,
        marginBottom: 16,
        marginTop: 8,
    },
});
