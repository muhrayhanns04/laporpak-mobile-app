import React from 'react';
import { View, Text, TouchableOpacity, ActivityIndicator } from 'react-native';
import { COLORS, FONTS, ICONS } from '../../constants';

const SettingItem = ({ value, onPress, containerStyle, icon, isLoading }) => {
    return (
        <TouchableOpacity
            style={{
                flexDirection: 'row',
                alignItems: 'center',
                ...containerStyle,
            }}
            onPress={() => (onPress ? onPress() : undefined)}
        >
            <View
                style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    flex: 1,
                }}
            >
                {icon()}

                {isLoading ? (
                    <ActivityIndicator
                        size="small"
                        color={COLORS.GREEN500}
                        style={{ marginLeft: 16 }}
                    />
                ) : (
                    <Text
                        style={{
                            ...FONTS.h13,
                            color: COLORS.BLACK,
                            marginLeft: 16,
                        }}
                    >
                        {value}
                    </Text>
                )}
            </View>

            <ICONS.CHEVRON
                width={24}
                height={24}
                style={{
                    transform: [{ rotate: '180deg' }],
                    color: COLORS.GRAY8,
                }}
            />
        </TouchableOpacity>
    );
};

export default SettingItem;
