import React from 'react';
import {
    Animated,
    View,
    TouchableOpacity,
    Text,
    ScrollView,
} from 'react-native';
import { COLORS, SIZES } from '../../constants/index';
import { ReportFinishContainer, ReportProcessContainer } from '../Report';

const SlidingTabContainer = ({
    contentContainerStyle,
    sheetProcess,
    reports,
    getReportsProcess,
    reportsFinish,
    getReportsFinish,
    sheetFinish,
}) => {
    const [activeTab, setActiveTab] = React.useState('Proses');
    const [tabs, setTabs] = React.useState({
        xTabOne: 0,
        xTabTwo: 0,
        xTabThree: 0,
    });
    const [tabsWidth, setTabsWidth] = React.useState(new Animated.Value(0));
    const [translateX] = React.useState(new Animated.Value(0));
    const [scroll, setScroll] = React.useState({ horizontal: 0, vertical: 0 });
    const scrollViewRef = React.useRef();

    function handleSlide(type) {
        Animated.spring(translateX, {
            toValue: type,
            duration: 100,
            useNativeDriver: false,
        }).start();
    }

    return (
        <View
            style={{
                flex: 1,
                backgroundColor: COLORS.WHITE,
                ...contentContainerStyle,
            }}
        >
            <View
                style={{
                    flex: 0.08,
                    paddingHorizontal: 16,
                    marginBottom: 32,
                }}
            >
                <View
                    style={{
                        flexDirection: 'row',
                        height: 52,
                    }}
                >
                    {/* Sliding */}
                    <Animated.View
                        style={{
                            position: 'absolute',
                            width: tabsWidth,
                            height: '100%',
                            top: 0,
                            left: 0,
                            borderBottomWidth: 4,
                            borderBottomStartRadius: 4,
                            borderBottomEndRadius: 4,
                            borderBottomColor: COLORS.GREEN600,
                            transform: [
                                {
                                    translateX,
                                },
                            ],
                        }}
                    />

                    {/* Button */}
                    <TouchableOpacity
                        style={{
                            flex: 0.5,
                            justifyContent: 'center',
                            alignItems: 'center',
                        }}
                        onLayout={event => {
                            setTabs({
                                ...tabs,
                                xTabOne: event.nativeEvent.layout.x,
                            });

                            setTabsWidth(event.nativeEvent.layout.width);
                        }}
                        onPress={() => {
                            handleSlide(tabs.xTabOne);
                            setActiveTab('Proses');
                            // Y use for Vertical
                            let VerticalScroll = scroll.vertical;
                            scrollViewRef.current.scrollTo({
                                x: 0,
                                y: VerticalScroll,
                                animated: true,
                            });
                        }}
                    >
                        <Text
                            style={{
                                fontFamily:
                                    activeTab !== 'Proses'
                                        ? 'Inter-Regular'
                                        : 'Inter-SemiBold',
                                fontSize: 16,
                                color:
                                    activeTab !== 'Proses'
                                        ? COLORS.BLACK
                                        : COLORS.GREEN700,
                            }}
                        >
                            Proses
                        </Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={{
                            flex: 0.5,
                            justifyContent: 'center',
                            alignItems: 'center',
                        }}
                        onLayout={event => {
                            setTabs({
                                ...tabs,
                                xTabTwo: event.nativeEvent.layout.x,
                            });
                            setTabsWidth(event.nativeEvent.layout.width);
                        }}
                        onPress={() => {
                            handleSlide(tabs.xTabTwo);
                            // Y use for Vertical
                            let VerticalScroll = scroll.vertical;
                            scrollViewRef.current.scrollTo({
                                x: 392,
                                y: VerticalScroll,
                                animated: true,
                            });
                        }}
                    >
                        <Text
                            style={{
                                fontFamily:
                                    activeTab !== 'Selesai'
                                        ? 'Inter-Regular'
                                        : 'Inter-SemiBold',
                                fontSize: 16,
                                color:
                                    activeTab !== 'Selesai'
                                        ? COLORS.BLACK
                                        : COLORS.GREEN700,
                            }}
                        >
                            Selesai
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>

            {/* Paste your component sreen at below */}
            {/* <View style={{ flex: 1 }}>
                {activeTab === 'Proses' ? (
                    <ReportProcessContainer
                        sheetProcess={item => sheetProcess(item)}
                        reports={reports}
                        getReportsProcess={() => getReportsProcess()}
                    />
                ) : (
                    <ReportFinishContainer
                        sheetFinish={item => sheetFinish(item)}
                        reports={reportsFinish}
                        getReportsFinish={() => getReportsFinish()}
                    />
                )}
            </View> */}
            <ScrollView
                pagingEnabled={true}
                horizontal={true}
                decelerationRate={0}
                // snapToInterval={width}
                style={{ paddingTop: 16 }}
                ref={scrollViewRef}
                snapToAlignment={'center'}
                onScroll={e => {
                    let logdata = `Scrolled to X = ${e.nativeEvent.contentOffset.x}, y = ${e.nativeEvent.contentOffset.y}`;
                    console.log(logdata);
                    setScroll({
                        horizontal: e.nativeEvent.contentOffset.x,
                        vertical: e.nativeEvent.contentOffset.y,
                    });
                    if (e.nativeEvent.contentOffset.x) {
                        return handleSlide(tabs.xTabTwo);
                    } else if (e.nativeEvent.contentOffset.x < SIZES.width) {
                        return handleSlide(tabs.xTabOne);
                    }
                }}
                scrollEventThrottle={10}
            >
                <View
                    style={{
                        width: SIZES.width,
                        flex: 1,
                    }}
                >
                    <ReportProcessContainer
                        sheetProcess={item => sheetProcess(item)}
                        reports={reports}
                        getReportsProcess={() => getReportsProcess()}
                    />
                </View>
                <View
                    style={{
                        width: SIZES.width,
                        flex: 1,
                    }}
                >
                    <ReportFinishContainer
                        sheetFinish={item => sheetFinish(item)}
                        reports={reportsFinish}
                        getReportsFinish={() => getReportsFinish()}
                    />
                </View>
            </ScrollView>
        </View>
    );
};

export default SlidingTabContainer;
