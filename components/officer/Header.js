import AsyncStorage from '@react-native-async-storage/async-storage';
import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { View, Text } from 'react-native';
import Ripple from 'react-native-material-ripple';
import { SIZES, COLORS, FONTS, ICONS } from '../../constants';
const AVATARSIZE = SIZES.width / 4.5;

const Header = () => {
    const navigation = useNavigation();

    return (
        <View
            style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                paddingHorizontal: 16,
            }}
        >
            <Text
                style={{
                    ...FONTS.h24Heading,
                    color: COLORS.BLACK,
                    width: SIZES.width / 1.6,
                }}
            >
                Hi Hans{'\n'}Segera selesaikan{' '}
                <Text style={{ color: COLORS.GREEN600 }}>Laporan</Text> Penduduk
            </Text>

            <Ripple
                onPress={() => navigation.navigate('SettingsScreen')}
                style={{
                    width: AVATARSIZE,
                    height: AVATARSIZE,
                    backgroundColor: COLORS.GRAY4,
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderRadius: 9999,
                }}
            >
                <ICONS.MEMOJIEXA />
            </Ripple>
        </View>
    );
};

export default Header;
