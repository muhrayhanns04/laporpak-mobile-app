import {
    ActivityIndicator,
    Alert,
    ScrollView,
    Text,
    TouchableHighlight,
    TouchableOpacity,
    TouchableWithoutFeedback,
    View,
} from 'react-native';
import { COLORS, FONTS, ICONS, SIZES } from '../../../constants';
import { setBaseUrl, setDefaultHeader } from '../../../constants/config';

import CarouselContainer from '../../carousel/CarouselContainer';
import ImageCropPicker from 'react-native-image-crop-picker';
import React from 'react';
import { ReportContext } from '../../../context/ReportContext';
import TextComponent from '../../Home/TextComponent';
import TextInputComponent from '../../FormCreate/TextInputComponent';
import axios from 'axios';

const SheetReportProcess = ({ propsItem, sheetReport, getRepliesProcess }) => {
    const [isLoadingSend, setIsLoadingSend] = React.useState(false);
    const { proofPhotos, setProofPhotos } = React.useContext(ReportContext);
    const [reply, setReply] = React.useState({
        message: '',
    });

    React.useEffect(() => {
        setProofPhotos(null);
    }, []);

    const toggleSubmitResponse = async report_id => {
        setBaseUrl();
        setDefaultHeader();
        try {
            setIsLoadingSend(true);
            const data = new FormData();
            data.append('message', reply.message);
            proofPhotos?.map((item, index) => {
                return data.append('file[]', {
                    type: item.mime,
                    uri: item.path,
                    name: `photo_proof_${index + 1}.jpg`,
                });
            });
            data.append('report_id', report_id);
            await axios.post('/responses/create', data);
            setReply({
                message: '',
            });
            sheetReport.current.close();
            setIsLoadingSend(false);
        } catch (e) {
            setIsLoadingSend(false);
            console.log(e);
        }
    };

    // Choose image from library
    const choosePhotoFromLibrary = () => {
        ImageCropPicker.openPicker({
            width: 128,
            height: 128,
            multiple: true,
            cropping: true,
            enableRotationGesture: true,
        }).then(image => {
            return setProofPhotos(image);
        });
    };

    const renderDevider = () => {
        return (
            <View
                style={{
                    height: 1,
                    backgroundColor: COLORS.WHITE3,
                    marginBottom: 24,
                }}
            />
        );
    };

    return (
        <ScrollView
            style={{
                flex: 1,
                backgroundColor: COLORS.WHITE,
            }}
            contentContainerStyle={{
                backgroundColor: COLORS.WHITE,
                paddingHorizontal: 16,
                paddingBottom: 24,
            }}
        >
            <TouchableOpacity activeOpacity={1}>
                <Text style={{ ...FONTS.largeTitleBold, color: COLORS.BLACK }}>
                    Segera{'\n'}Tanggapi{'\n'}Laporan
                </Text>
                {/* title pengaduan */}
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        marginTop: 24,
                        marginBottom: 12,
                    }}
                >
                    <View
                        style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                        }}
                    >
                        <ICONS.BISSEP width={24} height={24} />
                        <Text
                            style={{
                                ...FONTS.h4Bold,
                                color: COLORS.YELLOW,
                                marginLeft: 10,
                            }}
                        >
                            Tanggapan
                        </Text>
                    </View>
                    <TouchableHighlight
                        underlayColor="rgba(0,0,0,0.5)"
                        onPress={() => {
                            if (reply.message !== '') {
                                toggleSubmitResponse(propsItem.id);

                                setTimeout(() => {
                                    getRepliesProcess();
                                }, 1000);
                            } else {
                                Alert.alert(
                                    'Peringatan',
                                    'Tanggapan belum diberikan!s',
                                );
                            }
                        }}
                        style={{
                            borderRadius: 9999,
                        }}
                    >
                        {isLoadingSend ? (
                            <View
                                style={{
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    backgroundColor: COLORS.GRAY7,
                                    paddingVertical: 4,
                                    paddingHorizontal: 8,
                                    borderRadius: 9999,
                                }}
                            >
                                <ActivityIndicator
                                    size="small"
                                    color={COLORS.GREEN500}
                                />
                            </View>
                        ) : (
                            <View
                                style={{
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    backgroundColor: COLORS.GRAY7,
                                    paddingVertical: 4,
                                    paddingHorizontal: 8,
                                    borderRadius: 9999,
                                }}
                            >
                                <ICONS.CHECK width={24} height={24} />
                                <Text
                                    style={{
                                        ...FONTS.h4Bold,
                                        color: COLORS.GREEN500,
                                        marginLeft: 10,
                                    }}
                                >
                                    Simpan
                                </Text>
                            </View>
                        )}
                    </TouchableHighlight>
                </View>

                <TextInputComponent
                    placeholder="Ketikkan tanggapan terhadap laporan ini"
                    value={reply.message}
                    onChangeText={value =>
                        setReply({ ...reply, message: value })
                    }
                    keyboardType="web-search"
                    multiline={true}
                />

                {/* title time */}
                <TouchableWithoutFeedback
                    onPress={() =>
                        Alert.alert(
                            '🎁 Petunjuk',
                            'Unggah foto bukti dengan cara tekan gambar dibawah ini!',
                        )
                    }
                >
                    <View
                        style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            marginBottom: 16,
                        }}
                    >
                        <ICONS.SPARKLES width={24} height={24} />
                        <Text
                            style={{
                                ...FONTS.h4Bold,
                                color: COLORS.BLACK,
                                marginLeft: 10,
                            }}
                        >
                            Unggah Foto Bukti{' '}
                            <Text style={{ color: COLORS.GREEN600 }}>
                                *(Lihat Petunjuk)
                            </Text>
                        </Text>
                    </View>
                </TouchableWithoutFeedback>
                {proofPhotos ? (
                    <CarouselContainer
                        contentContainerStyle={{
                            marginTop: 8,
                            paddingHorizontal: 16,
                        }}
                        dotColor={[
                            COLORS.GREEN100,
                            COLORS.GREEN600,
                            COLORS.GREEN100,
                        ]}
                        onPress={choosePhotoFromLibrary}
                        datas={proofPhotos}
                        type="upload"
                    />
                ) : (
                    <TouchableOpacity
                        onPress={choosePhotoFromLibrary}
                        style={{
                            marginBottom: 24,
                            flexDirection: 'column',
                            alignItems: 'center',
                            justifyContent: 'center',
                            backgroundColor: COLORS.GRAY3,
                            height: SIZES.width / 1.7,
                            borderRadius: 15,
                        }}
                    >
                        <ICONS.IMAGE
                            style={{
                                color: COLORS.GRAY,
                            }}
                        />
                    </TouchableOpacity>
                )}

                {renderDevider()}
                {/* title pengaduan */}
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                    }}
                >
                    <ICONS.FIRE width={24} height={24} />
                    <Text
                        style={{
                            ...FONTS.h4Bold,
                            color: COLORS.BLACK,
                            marginLeft: 10,
                        }}
                    >
                        Pengaduan
                    </Text>
                </View>
                <TextComponent numberOfLines={4} seeMore={true}>
                    {propsItem?.message}
                </TextComponent>
                {/* title location */}
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        marginTop: 24,
                    }}
                >
                    <ICONS.PIN width={24} height={24} />
                    <Text
                        style={{
                            ...FONTS.h4Bold,
                            color: COLORS.BLACK,
                            marginLeft: 10,
                        }}
                    >
                        Lokasi
                    </Text>
                </View>
                <Text
                    style={{
                        ...FONTS.body5,
                        color: COLORS.GRAY,
                        marginTop: 12,
                    }}
                >
                    {propsItem?.detail_place}
                    {'\n'}
                    {propsItem?.incident_place}
                </Text>
                {/* title time */}
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        marginTop: 24,
                    }}
                >
                    <ICONS.CLOCK width={24} height={24} />
                    <Text
                        style={{
                            ...FONTS.h4Bold,
                            color: COLORS.BLACK,
                            marginLeft: 10,
                        }}
                    >
                        {propsItem?.time_of_occurrence} -{' '}
                        {propsItem?.incident_date}
                    </Text>
                </View>
                {/* proof photo */}
                {propsItem?.files >= 1 ? (
                    <View
                        style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            marginTop: 24,
                        }}
                    >
                        <ICONS.SPARKLES width={24} height={24} />
                        <Text
                            style={{
                                ...FONTS.h4Bold,
                                color: COLORS.BLACK,
                                marginLeft: 10,
                            }}
                        >
                            Foto Bukti
                        </Text>
                    </View>
                ) : null}

                {propsItem?.files ? (
                    <CarouselContainer
                        contentContainerStyle={{
                            marginTop: 24,
                            paddingHorizontal: 16,
                        }}
                        dotColor={[
                            COLORS.GREEN100,
                            COLORS.GREEN600,
                            COLORS.GREEN100,
                        ]}
                        datas={propsItem?.files}
                    />
                ) : null}
            </TouchableOpacity>
        </ScrollView>
    );
};

export default SheetReportProcess;
