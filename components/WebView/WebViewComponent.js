import React from 'react';
import { StatusBar } from 'react-native';
import WebView from 'react-native-webview';
import * as Progress from 'react-native-progress';
import { COLORS } from '../../constants';

const WebViewComponent = ({ route, navigation }) => {
    const { _link } = route.params;
    const [progress, setProgress] = React.useState(0);
    console.log(progress);
    return (
        <>
            <StatusBar
                barStyle="light-content"
                backgroundColor={COLORS.GREEN600}
            />

            <Progress.Bar
                progress={progress}
                color={COLORS.GREEN800}
                width={null}
                borderRadius={0}
                borderWidth={0}
            />
            <WebView
                source={{ uri: _link, headers: { key: 'value' } }}
                onLoadProgress={({ nativeEvent }) => {
                    setProgress(nativeEvent.progress);
                }}
            />
        </>
    );
};

export default WebViewComponent;
