import { useRoute } from '@react-navigation/native';
import React from 'react';
import { View, Text } from 'react-native';
import { COLORS, FONTS } from '../../constants';

const WebViewHeader = () => {
    const route = useRoute();
    const { _title } = route.params;
    return (
        <Text
            style={{
                ...FONTS.h5Bold,
                color: COLORS.WHITE,
            }}
        >
            {_title}
        </Text>
    );
};

export default WebViewHeader;
