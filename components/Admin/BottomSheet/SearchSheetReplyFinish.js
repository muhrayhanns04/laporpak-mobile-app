import React from 'react';
import {
    ScrollView,
    View,
    Text,
    TouchableHighlight,
    TouchableOpacity,
    TouchableWithoutFeedback,
    Alert,
    ActivityIndicator,
} from 'react-native';
import { COLORS, FONTS, ICONS, SIZES } from '../../../constants';
import TextComponent from '../../Home/TextComponent';
import CarouselContainer from '../../carousel/CarouselContainer';
import { BottomSheetScrollView } from '@gorhom/bottom-sheet';
import { capitalize } from '../../../helper';

const SearchSheetReplyFinish = ({ propsItemFinish }) => {
    const [isLoadingSend, setIsLoadingSend] = React.useState(false);

    const renderDevider = () => {
        return (
            <View
                style={{
                    height: 1,
                    backgroundColor: COLORS.WHITE3,
                    marginBottom: 24,
                }}
            />
        );
    };

    return (
        <ScrollView
            style={{
                flex: 1,
                backgroundColor: COLORS.WHITE,
            }}
            contentContainerStyle={{
                backgroundColor: COLORS.WHITE,
                paddingHorizontal: 16,
                paddingBottom: 24,
            }}
        >
            <TouchableOpacity activeOpacity={1}>
                <Text
                    style={{
                        ...FONTS.largeTitleBold,
                        color: COLORS.BLACK,
                    }}
                >
                    Laporan{'\n'}Sudah{'\n'}Ditanggapi
                </Text>

                {/* title pengaduan */}
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        marginTop: 24,
                        marginBottom: 12,
                    }}
                >
                    <View
                        style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                        }}
                    >
                        <ICONS.BISSEP width={24} height={24} />
                        <Text
                            style={{
                                ...FONTS.h4Bold,
                                color: COLORS.YELLOW,
                                marginLeft: 10,
                            }}
                        >
                            Tanggapan
                        </Text>
                    </View>
                </View>

                <TextComponent
                    numberOfLines={4}
                    seeMore={true}
                    containerStyle={{ marginBottom: 24 }}
                >
                    {propsItemFinish?.reply?.message}
                </TextComponent>

                {/* title time */}
                {propsItemFinish?.reply?.files.length >= 1 ? (
                    <View
                        style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            marginBottom: 16,
                        }}
                    >
                        <ICONS.SPARKLES width={24} height={24} />
                        <Text
                            style={{
                                ...FONTS.h4Bold,
                                color: COLORS.BLACK,
                                marginLeft: 10,
                            }}
                        >
                            Foto Bukti
                        </Text>
                    </View>
                ) : null}
                {/* slider proof photos */}
                {propsItemFinish?.reply?.files.length >= 1 ? (
                    <CarouselContainer
                        contentContainerStyle={{
                            paddingHorizontal: 16,
                        }}
                        dotColor={[
                            COLORS.GREEN100,
                            COLORS.GREEN600,
                            COLORS.GREEN100,
                        ]}
                        datas={propsItemFinish?.reply?.files}
                    />
                ) : null}

                {renderDevider()}

                {/* title pengaduan */}
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                    }}
                >
                    <ICONS.FIRE width={24} height={24} />
                    <Text
                        style={{
                            ...FONTS.h4Bold,
                            color: COLORS.BLACK,
                            marginLeft: 10,
                        }}
                    >
                        Pengaduan
                    </Text>
                </View>
                <TextComponent numberOfLines={4} seeMore={true}>
                    {propsItemFinish?.message}
                </TextComponent>
                {/* title location */}
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        marginTop: 24,
                    }}
                >
                    <ICONS.PIN width={24} height={24} />
                    <Text
                        style={{
                            ...FONTS.h4Bold,
                            color: COLORS.BLACK,
                            marginLeft: 10,
                        }}
                    >
                        Lokasi
                    </Text>
                </View>
                <Text
                    style={{
                        ...FONTS.body5,
                        color: COLORS.GRAY,
                        marginTop: 12,
                    }}
                >
                    {propsItemFinish?.detail_place}
                    {'\n'}
                    {propsItemFinish?.incident_place}
                </Text>

                {/* title status */}
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        marginTop: 24,
                    }}
                >
                    <ICONS.WATCH width={24} height={24} />
                    <Text
                        style={{
                            ...FONTS.h4Bold,
                            color: COLORS.BLACK,
                            marginLeft: 10,
                        }}
                    >
                        {capitalize(propsItemFinish?.status)}
                    </Text>
                </View>

                {/* title time */}
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        marginTop: 24,
                    }}
                >
                    <ICONS.CLOCK width={24} height={24} />
                    <Text
                        style={{
                            ...FONTS.h4Bold,
                            color: COLORS.BLACK,
                            marginLeft: 10,
                        }}
                    >
                        {propsItemFinish?.time_of_occurrence} -{' '}
                        {propsItemFinish?.incident_date}
                    </Text>
                </View>
                {/* proof photo */}
                {propsItemFinish?.files >= 1 ? (
                    <View
                        style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            marginTop: 24,
                        }}
                    >
                        <ICONS.SPARKLES width={24} height={24} />
                        <Text
                            style={{
                                ...FONTS.h4Bold,
                                color: COLORS.BLACK,
                                marginLeft: 10,
                            }}
                        >
                            Foto Bukti
                        </Text>
                    </View>
                ) : null}

                {propsItemFinish?.files ? (
                    <CarouselContainer
                        contentContainerStyle={{
                            marginTop: 24,
                            paddingHorizontal: 16,
                        }}
                        dotColor={[
                            COLORS.GREEN100,
                            COLORS.GREEN600,
                            COLORS.GREEN100,
                        ]}
                        datas={propsItemFinish?.files}
                    />
                ) : null}
            </TouchableOpacity>
        </ScrollView>
    );
};

export default SearchSheetReplyFinish;
