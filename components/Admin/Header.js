import AsyncStorage from '@react-native-async-storage/async-storage';
import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { View, Text } from 'react-native';
import Ripple from 'react-native-material-ripple';
import { SIZES, COLORS, FONTS, ICONS } from '../../constants';
const AVATARSIZE = SIZES.width / 4.5;

const Header = () => {
    const navigation = useNavigation();

    return (
        <View
            style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                paddingHorizontal: 16,
                marginBottom: 24,
            }}
        >
            <Text
                style={{
                    ...FONTS.h24Heading,
                    color: COLORS.BLACK,
                    width: SIZES.width / 1.6,
                }}
            >
                @Admin Lapor<Text style={{ color: COLORS.GREEN600 }}>Pak</Text>
            </Text>

            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Ripple
                    onPress={() => navigation.navigate('SearchScreen')}
                    style={{
                        flexDirection: 'column',
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderRadius: 9999,
                        marginRight: 24,
                    }}
                >
                    <ICONS.SEARCH style={{ color: COLORS.GREEN600 }} />
                </Ripple>
                <Ripple
                    onPress={() => navigation.navigate('SettingsScreen')}
                    style={{
                        flexDirection: 'column',
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderRadius: 9999,
                    }}
                >
                    <ICONS.USER />
                </Ripple>
            </View>
        </View>
    );
};

export default Header;
