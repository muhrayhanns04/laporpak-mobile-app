import React from 'react';
import {
    ScrollView,
    View,
    Text,
    SafeAreaView,
    ActivityIndicator,
    RefreshControl,
    Alert,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { COLORS, FONTS } from '../../../constants';
import Ripple from 'react-native-material-ripple';
import { capitalize, formatDate, nameCencor, wait } from '../../../helper';
import { setBaseUrl, setDefaultHeader } from '../../../constants/config';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import BottomSheet, {
    BottomSheetBackdrop,
    BottomSheetModalProvider,
} from '@gorhom/bottom-sheet';
import SheetReplyFinish from '../BottomSheet/SheetReplyFinish';
import RBSheet from 'react-native-raw-bottom-sheet';
import { useNavigation } from '@react-navigation/native';
import { ReportContext } from '../../../context/ReportContext';

const ReportFinishContainer = () => {
    const sheetResponse = React.useRef(null);
    const [refreshing, setRefreshing] = React.useState(false);
    const [propsItemFinish, setPropsItemFinish] = React.useState(null);
    const [isLoading, setIsLoading] = React.useState(false);
    const navigation = useNavigation();
    const { setDateFilter, setTypeScreen, reportsFinish, setReportsFinish } =
        React.useContext(ReportContext);

    const getReportsFinish = async () => {
        setIsLoading(true);
        setBaseUrl();
        try {
            const response = await axios.get(`/responses/free`);

            setIsLoading(false);
            setReportsFinish(response.data.results);
            setDateFilter(null);
        } catch (error) {
            setIsLoading(false);
            console.log(error?.response?.data?.message);
        }
    };

    function handleForPropsSheetResponse(item) {
        setPropsItemFinish(item);
        sheetResponse.current.open();
    }

    React.useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            getReportsFinish();
            setTypeScreen('finish');
        });

        return unsubscribe;
    }, [navigation]);

    React.useEffect(() => {
        getReportsFinish();
    }, []);

    const onRefresh = React.useCallback(() => {
        setRefreshing(true);
        getReportsFinish();
        wait(1200).then(() => setRefreshing(false));
    }, []);

    return isLoading ? (
        <SafeAreaView
            style={{
                flex: 1,
                backgroundColor: COLORS.WHITE,
                justifyContent: 'center',
                alignItems: 'center',
            }}
        >
            <ActivityIndicator size="large" color={COLORS.GREEN600} />
        </SafeAreaView>
    ) : (
        <SafeAreaView
            style={{
                flex: 1,
                backgroundColor: COLORS.WHITE,
                paddingHorizontal: 16,
            }}
        >
            <RBSheet
                ref={sheetResponse}
                closeOnDragDown={true}
                closeOnPressMask={true}
                keyboardAvoidingViewEnabled={true}
                height={700}
                customStyles={{
                    draggableIcon: {
                        backgroundColor: '#000',
                    },
                    container: {
                        borderTopRightRadius: 16,
                        borderTopLeftRadius: 16,
                    },
                }}
            >
                <SheetReplyFinish propsItemFinish={propsItemFinish} />
            </RBSheet>

            <ScrollView
                style={{
                    flex: 1,
                }}
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{
                    backgroundColor: COLORS.WHITE,
                    paddingTop: 16,
                }}
                refreshControl={
                    <RefreshControl
                        refreshing={refreshing}
                        onRefresh={onRefresh}
                    />
                }
            >
                {reportsFinish?.map((item, index) => {
                    return (
                        <Ripple
                            onPress={() => {
                                handleForPropsSheetResponse(item);
                            }}
                            onLongPress={() => {
                                Alert.alert(
                                    'Download PDF',
                                    'Yakin ingin mengunduh laporan format PDF?',
                                    [
                                        {
                                            text: 'Batal',
                                            onPress: () =>
                                                console.log('Cancel Pressed'),
                                            style: 'cancel',
                                        },
                                        {
                                            text: 'Download',
                                            onPress: () =>
                                                navigation.navigate(
                                                    'WebViewComponent',
                                                    {
                                                        _link: `http://192.168.100.6:8000/print/${item.id}`,
                                                        _title: 'Downlod Laporan',
                                                    },
                                                ),
                                        },
                                    ],
                                );
                            }}
                        >
                            <LinearGradient
                                key={index}
                                colors={['#FFFFFF', '#E1F7E8']}
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                style={{
                                    position: 'relative',
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    borderRadius: 15,
                                    paddingVertical: 16,
                                    paddingHorizontal: 24,
                                    marginBottom: 16,
                                }}
                            >
                                <View
                                    style={{
                                        position: 'absolute',
                                        left: -4,
                                        width: 6,
                                        borderRadius: 999,
                                        height: 74,
                                        backgroundColor: COLORS.GREEN600,
                                        zIndex: 9999,
                                    }}
                                />
                                <View>
                                    <Text
                                        style={{
                                            ...FONTS.h4SemiBold,
                                            color: COLORS.GREEN800,
                                        }}
                                    >
                                        {nameCencor(
                                            item?.report?.user?.username,
                                        )}
                                    </Text>

                                    <Text
                                        style={{
                                            ...FONTS.h4SemiBold,
                                            color: COLORS.GREEN800,
                                        }}
                                    >
                                        {capitalize(
                                            item?.report?.type_of_report,
                                        )}
                                    </Text>

                                    <Text
                                        style={{
                                            ...FONTS.h4SemiBold,
                                            color: COLORS.BLACK,
                                            marginVertical: 8,
                                        }}
                                        numberOfLines={1}
                                    >
                                        {item?.report?.message}
                                    </Text>

                                    <Text
                                        style={{
                                            ...FONTS.h4,
                                            color: COLORS.GRAY,
                                        }}
                                        numberOfLines={1}
                                    >
                                        {formatDate(item.created_at)}
                                    </Text>
                                </View>
                            </LinearGradient>
                        </Ripple>
                    );
                })}
            </ScrollView>
        </SafeAreaView>
    );
};

export default ReportFinishContainer;
