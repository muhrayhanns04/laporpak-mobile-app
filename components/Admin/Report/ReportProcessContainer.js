import {
    ActivityIndicator,
    Alert,
    PermissionsAndroid,
    RefreshControl,
    SafeAreaView,
    ScrollView,
    Text,
    View,
} from 'react-native';
import { COLORS, FONTS } from '../../../constants';
import { capitalize, formatDate, nameCencor, wait } from '../../../helper';
import {
    setBaseUrl,
    setBaseUrlV2,
    setDefaultHeader,
} from '../../../constants/config';

import AsyncStorage from '@react-native-async-storage/async-storage';
import LinearGradient from 'react-native-linear-gradient';
import RBSheet from 'react-native-raw-bottom-sheet';
import RNFetchBlob from 'rn-fetch-blob';
import React from 'react';
import { ReportContext } from '../../../context/ReportContext';
import Ripple from 'react-native-material-ripple';
import SheetReportProcess from '../BottomSheet/SheetReportProcess';
import axios from 'axios';
import { useNavigation } from '@react-navigation/native';

const ReportProcessContainer = () => {
    const [refreshing, setRefreshing] = React.useState(false);
    const sheetReport = React.useRef(null);
    const [propsItem, setPropsItem] = React.useState(null);
    const { reportsProcess, setReportsProcess, setDateFilter, setTypeScreen } =
        React.useContext(ReportContext);
    const [isLoading, setIsLoading] = React.useState(false);
    const navigation = useNavigation();

    const getRepliesProcess = async () => {
        setIsLoading(true);
        setBaseUrl();
        try {
            const response = await axios.get(`/complaints/free`, {
                params: {
                    sort: 'status',
                    value: 'proses',
                    type: 'free',
                },
            });

            setIsLoading(false);
            setReportsProcess(response.data.results);
            setDateFilter(null);
        } catch (error) {
            setIsLoading(false);
            console.log(error?.response?.data?.message);
        }
    };

    const fileUrl = 'http://192.168.100.6:8000/print/3';

    const checkPermission = async () => {
        // Function to check the platform
        // If Platform is Android then check for permissions.

        if (Platform.OS === 'ios') {
            downloadFile();
        } else {
            try {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                    {
                        title: 'Storage Permission Required',
                        message:
                            'Application needs access to your storage to download File',
                    },
                );
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    // Start downloading
                    downloadFile();
                    console.log('Storage Permission Granted.');
                } else {
                    // If permission denied then show alert
                    Alert.alert('Error', 'Storage Permission Not Granted');
                }
            } catch (err) {
                // To handle permission related exception
                console.log('++++' + err);
            }
        }
    };

    const downloadFile = () => {
        // Get today's date to add the time suffix in filename
        let date = new Date();
        // File URL which we want to download
        let FILE_URL = fileUrl;
        // Function to get extention of the file url
        let file_ext = getFileExtention(FILE_URL);

        file_ext = '.' + file_ext[0];

        // config: To get response by passing the downloading related options
        // fs: Root directory path to download
        const { config, fs } = RNFetchBlob;
        let RootDir = fs.dirs.PictureDir;
        let options = {
            fileCache: true,
            addAndroidDownloads: {
                path:
                    RootDir +
                    '/file_' +
                    Math.floor(date.getTime() + date.getSeconds() / 2) +
                    file_ext,
                description: 'downloading file...',
                notification: true,
                // useDownloadManager works with Android only
                useDownloadManager: true,
            },
        };
        config(options)
            .fetch('GET', FILE_URL)
            .then(res => {
                // Alert after successful downloading
                console.log('res -> ', JSON.stringify(res));
                alert('File Downloaded Successfully.');
            });
    };

    const getFileExtention = fileUrl => {
        // To get the file extension
        return /[.]/.exec(fileUrl) ? /[^.]+$/.exec(fileUrl) : undefined;
    };

    const onRefresh = React.useCallback(() => {
        setRefreshing(true);
        getRepliesProcess();
        wait(1200).then(() => setRefreshing(false));
    }, []);

    // sheet open
    function handleForPropsSheetProcess(item) {
        setPropsItem(item);
        sheetReport.current.open();
    }

    React.useEffect(() => {
        getRepliesProcess();
    }, []);

    React.useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            getRepliesProcess();
            setTypeScreen('process');
        });

        return unsubscribe;
    }, [navigation]);

    return isLoading ? (
        <SafeAreaView
            style={{
                flex: 1,
                backgroundColor: COLORS.WHITE,
                justifyContent: 'center',
                alignItems: 'center',
            }}
        >
            <ActivityIndicator size="large" color={COLORS.GREEN600} />
        </SafeAreaView>
    ) : (
        <SafeAreaView
            style={{
                flex: 1,
                backgroundColor: COLORS.WHITE,
                paddingHorizontal: 16,
            }}
        >
            <RBSheet
                ref={sheetReport}
                closeOnDragDown={true}
                closeOnPressMask={true}
                keyboardAvoidingViewEnabled={true}
                height={700}
                customStyles={{
                    draggableIcon: {
                        backgroundColor: '#000',
                    },
                    container: {
                        borderTopRightRadius: 16,
                        borderTopLeftRadius: 16,
                    },
                }}
            >
                <SheetReportProcess
                    propsItem={propsItem}
                    sheetReport={sheetReport}
                    getRepliesProcess={() => getRepliesProcess()}
                />
            </RBSheet>

            <ScrollView
                style={{
                    flex: 1,
                }}
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{
                    backgroundColor: COLORS.WHITE,
                    paddingTop: 16,
                }}
                refreshControl={
                    <RefreshControl
                        refreshing={refreshing}
                        onRefresh={onRefresh}
                    />
                }
            >
                {reportsProcess?.map((item, index) => {
                    return (
                        <Ripple
                            onLongPress={() => {
                                // checkPermission();
                                Alert.alert(
                                    'Download PDF',
                                    'Yakin ingin mengunduh laporan format PDF?',
                                    [
                                        {
                                            text: 'Batal',
                                            onPress: () =>
                                                console.log('Cancel Pressed'),
                                            style: 'cancel',
                                        },
                                        {
                                            text: 'Download',
                                            onPress: () =>
                                                navigation.navigate(
                                                    'WebViewComponent',
                                                    {
                                                        _link: `http://192.168.100.6:8000/print/${item.id}`,
                                                        _title: 'Downlod Laporan',
                                                    },
                                                ),
                                        },
                                    ],
                                );
                            }}
                            onPress={() => {
                                handleForPropsSheetProcess(item);
                            }}
                            style={{ zIndex: -1 }}
                        >
                            <LinearGradient
                                key={index}
                                colors={['#E1F7E8', '#F7EEE1']}
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                style={{
                                    position: 'relative',
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    borderRadius: 15,
                                    paddingVertical: 16,
                                    paddingHorizontal: 24,
                                    marginBottom: 16,
                                }}
                            >
                                <View
                                    style={{
                                        position: 'absolute',
                                        left: -4,
                                        width: 6,
                                        borderRadius: 999,
                                        height: 74,
                                        backgroundColor: COLORS.GREEN600,
                                        zIndex: 9999,
                                    }}
                                />
                                <View>
                                    <Text
                                        style={{
                                            ...FONTS.h4SemiBold,
                                            color: COLORS.GREEN800,
                                        }}
                                    >
                                        {nameCencor(item?.user?.username)}
                                    </Text>
                                    <Text
                                        style={{
                                            ...FONTS.h4SemiBold,
                                            color: COLORS.GREEN800,
                                        }}
                                    >
                                        {capitalize(item?.type_of_report)}
                                    </Text>

                                    <Text
                                        style={{
                                            ...FONTS.h4SemiBold,
                                            color: COLORS.BLACK,
                                            marginVertical: 8,
                                        }}
                                        numberOfLines={1}
                                    >
                                        {item.message}
                                    </Text>

                                    <Text
                                        style={{
                                            ...FONTS.h4,
                                            color: COLORS.GRAY,
                                        }}
                                        numberOfLines={1}
                                    >
                                        {formatDate(item.created_at)}
                                    </Text>
                                </View>
                            </LinearGradient>
                        </Ripple>
                    );
                })}
            </ScrollView>
        </SafeAreaView>
    );
};

export default ReportProcessContainer;
