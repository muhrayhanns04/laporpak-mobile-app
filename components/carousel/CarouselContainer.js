import React from 'react';
import {
    View,
    FlatList,
    Animated,
    TouchableWithoutFeedback,
} from 'react-native';
import CarouselItem from './CarouselItem';
import { SIZES } from '../../constants/index';
import Paginator from './Paginator';

const CarouselContainer = ({
    contentContainerStyle,
    dotColor,
    datas,
    type,
    onPress,
}) => {
    const [newLayout, setNewLayout] = React.useState({
        x: 0,
        y: 0,
        width: 0,
        height: 0,
    });

    const [currentIndex, setCurrentIndex] = React.useState(0);
    const scrollX = React.useRef(new Animated.Value(0)).current;
    const slidesRef = React.useRef(null);

    const viewableItemsChanged = React.useRef(({ viewableItems }) => {
        // setCurrentIndex(viewableItems[0].index);
    }).current;
    const viewConfig = React.useRef({
        viewAreaCoveragePercentThreshold: 50,
    }).current;

    // To get view width & height
    // console.log(newLayout);

    function findDimensions(layout) {
        const { x, y, width, height } = layout;

        return setNewLayout({ x: x, y: y, width: width, height: height });
    }

    return (
        // <TouchableWithoutFeedback onPress={type === 'upload' ? onPress : null}>
        <View
            style={{
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
                ...contentContainerStyle,
            }}
        >
            <FlatList
                data={datas}
                keyExtractor={item => `${item.id}`}
                showsHorizontalScrollIndicator={false}
                horizontal={true}
                pagingEnabled
                bounces={false}
                onLayout={event => findDimensions(event.nativeEvent.layout)}
                style={{
                    position: 'relative',
                    width: SIZES.width / 1.1,
                }}
                contentContainerStyle={{
                    borderRadius: 105,
                }}
                ref={slidesRef}
                scrollEventThrottle={32}
                onViewableItemsChanged={viewableItemsChanged}
                viewabilityConfig={viewConfig}
                renderItem={({ item }) => (
                    <CarouselItem
                        newLayout={newLayout}
                        item={item}
                        type={type}
                        onPress={onPress}
                    />
                )}
                onScroll={Animated.event(
                    [
                        {
                            nativeEvent: {
                                contentOffset: {
                                    x: scrollX,
                                },
                            },
                        },
                    ],
                    {
                        useNativeDriver: false,
                    },
                )}
            />

            {/* reusable Paginator */}
            <Paginator
                data={datas}
                scrollX={scrollX}
                containerStyles={{
                    marginTop: 8,
                }}
                outputRange={dotColor}
                outputRangeDotWidth={[8, 24, 8]}
                dotStyle={{
                    height: 8,
                    marginHorizontal: 4,
                }}
            />
        </View>
        // </TouchableWithoutFeedback>
    );
};

export default CarouselContainer;
