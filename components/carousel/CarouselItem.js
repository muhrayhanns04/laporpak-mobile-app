import { COLORS, SIZES } from '../../constants';
import { Image, StyleSheet, TouchableWithoutFeedback } from 'react-native';

import FastImage from 'react-native-fast-image';
import React from 'react';

const CarouselItem = ({ item, type, onPress }) => {
    return (
        <TouchableWithoutFeedback onPress={type === 'upload' ? onPress : null}>
            <FastImage
                style={{
                    backgroundColor: COLORS.GREEN600,
                    width: SIZES.width / 1.1,
                    height: SIZES.width / 1.7,
                    borderRadius: 15,
                }}
                source={{
                    uri: type === 'upload' ? item.path : item.url,
                    headers: { Authorization: 'someAuthToken' },
                    priority: FastImage.priority.normal,
                }}
                // resizeMode={FastImage.resizeMode.contain}
            />
        </TouchableWithoutFeedback>
    );
};

export default CarouselItem;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
});
