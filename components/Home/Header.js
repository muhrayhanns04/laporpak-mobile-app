import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { View, Text } from 'react-native';
import Ripple from 'react-native-material-ripple';
import { SIZES, COLORS, FONTS, ICONS } from '../../constants';
import { UserContext } from '../../context/UserContext';
const AVATARSIZE = SIZES.width / 4.5;

const Header = () => {
    const navigation = useNavigation();
    const { user } = React.useContext(UserContext);

    return (
        <View
            style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                paddingHorizontal: 16,
                marginBottom: 16,
            }}
        >
            <Text
                style={{
                    ...FONTS.h24Heading,
                    color: COLORS.black,
                    width: SIZES.width / 1.6,
                }}
            >
                Hi {user.username}
                {'\n'}Laporkan semua
                <Text style={{ color: COLORS.GREEN600 }}> Keluhanmu</Text>
            </Text>

            <Ripple
                onPress={() => navigation.navigate('SettingsScreen')}
                style={{
                    width: AVATARSIZE,
                    height: AVATARSIZE,
                    backgroundColor: COLORS.GRAY4,
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderRadius: 9999,
                }}
                rippleDuration={800}
                rippleContainerBorderRadius={99999}
            >
                <ICONS.MEMOJIEXA />
            </Ripple>
        </View>
    );
};

export default Header;
