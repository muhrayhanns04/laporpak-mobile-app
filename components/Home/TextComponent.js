import React from 'react';
import { View, Text } from 'react-native';
import { COLORS, FONTS } from '../../constants';

const TextComponent = ({
    children,
    numberOfLines,
    seeMore,
    containerStyle,
    // textShown,
    // setTextShown,
}) => {
    const [textShown, setTextShown] = React.useState(false);
    const [lengthMore, setLengthMore] = React.useState(false);

    const toggleNumberOfLines = () => {
        setTextShown(!textShown);
    };
    const onTextLayout = React.useCallback(e => {
        setLengthMore(e.nativeEvent.lines.length >= numberOfLines);
    }, []);

    return (
        <View style={{ ...containerStyle }}>
            <Text
                style={{
                    ...FONTS.body5,
                    color: COLORS.GRAY,
                    marginTop: 12,
                }}
                onTextLayout={seeMore ? onTextLayout : undefined}
                numberOfLines={
                    seeMore
                        ? textShown
                            ? undefined
                            : numberOfLines
                        : undefined
                }
            >
                {children}
            </Text>
            {seeMore ? (
                lengthMore ? (
                    <Text
                        style={{
                            ...FONTS.h6,
                            color: COLORS.BLACK,
                            marginTop: 12,
                        }}
                        onPress={toggleNumberOfLines}
                    >
                        {textShown
                            ? 'Baca lebih sedikit...'
                            : 'Baca selengkapnya...'}
                    </Text>
                ) : null
            ) : null}
        </View>
    );
};

export default TextComponent;
