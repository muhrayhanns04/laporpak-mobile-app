import { BottomSheetView } from '@gorhom/bottom-sheet';
import React from 'react';
import { View, Text, TouchableHighlight } from 'react-native';
import { COLORS, FONTS, ICONS, SIZES } from '../../constants';

const SheetContentClassification = ({ report, setReport }) => {
    const type = [
        {
            type: 'Pengaduan',
        },
        {
            type: 'Aspirasi',
        },
    ];
    return (
        <BottomSheetView
            style={{
                backgroundColor: COLORS.WHITE,
                paddingHorizontal: SIZES.PADDING - 8,
                paddingBottom: 24,
                flex: 1,
                paddingTop: 16,
            }}
        >
            {/* Title */}
            <View style={{ zIndex: -1 }}>
                <Text
                    style={{
                        ...FONTS.largeTitleBold,
                        color: COLORS.BLACK,
                        marginBottom: 16,
                    }}
                >
                    Pilih{'\n'}Klasifikasi
                </Text>

                <TouchableHighlight
                    underlayColor={COLORS.lightGRAY3}
                    style={{
                        paddingVertical: 16,
                        paddingHorizontal: 13,
                        borderRadius: 8,
                        borderWidth: 2,
                        borderColor:
                            report.type === 'Pengaduan'
                                ? COLORS.BLACK
                                : COLORS.GRAY7,
                        marginBottom: 16,
                        backgroundColor:
                            report.type === 'Pengaduan' ? COLORS.GRAY7 : null,
                    }}
                    onPress={() => {
                        setReport({ ...report, type: 'Pengaduan' });
                    }}
                >
                    <View
                        style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                        }}
                    >
                        <ICONS.REPORT width={24} height={24} />
                        <Text
                            style={{
                                ...FONTS.body4,
                                color:
                                    report.type === 'Pengaduan'
                                        ? COLORS.BLACK
                                        : COLORS.GRAY,
                                marginLeft: 16,
                            }}
                        >
                            Pengaduan
                        </Text>
                    </View>
                </TouchableHighlight>

                <TouchableHighlight
                    underlayColor={COLORS.lightGRAY3}
                    style={{
                        paddingVertical: 16,
                        paddingHorizontal: 13,
                        borderRadius: 8,
                        borderWidth: 2,
                        borderColor:
                            report.type === 'Aspirasi'
                                ? COLORS.BLACK
                                : COLORS.GRAY7,
                        marginBottom: 16,
                        backgroundColor:
                            report.type === 'Aspirasi' ? COLORS.GRAY7 : null,
                    }}
                    onPress={() => {
                        setReport({ ...report, type: 'Aspirasi' });
                    }}
                >
                    <View
                        style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                        }}
                    >
                        <ICONS.ASPIRATION width={24} height={24} />
                        <Text
                            style={{
                                ...FONTS.body4,
                                color:
                                    report.type === 'Aspirasi'
                                        ? COLORS.BLACK
                                        : COLORS.GRAY,
                                marginLeft: 16,
                            }}
                        >
                            Aspirasi
                        </Text>
                    </View>
                </TouchableHighlight>
            </View>
        </BottomSheetView>
    );
};

export default SheetContentClassification;
