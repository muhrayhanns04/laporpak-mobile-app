import React from 'react';
import { View, TextInput } from 'react-native';
import { COLORS, FONTS } from '../../constants';

const TextInputComponent = ({
    value,
    onChangeText,
    keyboardType,
    multiline,
    placeholder,
}) => {
    return (
        <View>
            <TextInput
                placeholder={placeholder ? placeholder : 'Ketikkan sesuatu'}
                style={{
                    ...FONTS.body4,
                    paddingHorizontal: 16,
                    paddingLeft: 16,
                    paddingRight: 42,
                    backgroundColor: COLORS.GRAY7,
                    borderRadius: 8,
                    color: COLORS.GRAY,
                    marginBottom: 16,
                }}
                multiline={multiline}
                value={value}
                onChangeText={value => onChangeText(value)}
                keyboardType={keyboardType ? keyboardType : 'default'}
            />
        </View>
    );
};

export default TextInputComponent;
