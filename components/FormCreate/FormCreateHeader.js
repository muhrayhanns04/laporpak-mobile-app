import React from 'react';
import { View, Text } from 'react-native';
import { COLORS, FONTS } from '../../constants/index';

const FormCreateHeader = () => {
    return (
        <View>
            <Text style={{ ...FONTS.h24Bold, color: COLORS.BLACK }}>
                Form <Text style={{ color: COLORS.GREEN600 }}>Laporan</Text>
            </Text>
        </View>
    );
};

export default FormCreateHeader;
