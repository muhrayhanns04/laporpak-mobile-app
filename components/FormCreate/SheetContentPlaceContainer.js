import {
    BottomSheetFlatList,
    BottomSheetModal,
    BottomSheetModalProvider,
} from '@gorhom/bottom-sheet';
import axios from 'axios';
import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableHighlight,
    Alert,
    ActivityIndicator,
    TextInput,
} from 'react-native';
import Ripple from 'react-native-material-ripple';
import { COLORS, FONTS, SIZES, ICONS } from '../../constants';
import { setBaseUrl } from '../../constants/config';
import { ReportContext } from '../../context/ReportContext';

const SheetContentPlaceContainer = ({ INPUTSTYLE }) => {
    const bottomSheetModalRef = React.useRef(null);
    const bottomSheetModalCitiesRef = React.useRef(null);
    const [isLoading, setIsLoading] = React.useState(false);
    const { location, setLocation } = React.useContext(ReportContext);

    const [provinces, setProvinces] = React.useState(null);
    const [cities, setCities] = React.useState(null);
    const snapPointsModal = React.useMemo(() => ['50%'], []);

    const getProvinces = async () => {
        setBaseUrl();
        try {
            setIsLoading(true);
            const response = await axios.get('/location/provinces');
            setIsLoading(false);
            setProvinces(response.data.results);
        } catch (error) {
            setIsLoading(false);
            console.log(error.response.data.message);
        }
    };
    const getCities = async ID => {
        setBaseUrl();
        try {
            setIsLoading(true);
            const response = await axios.get(
                `/location/provinces/${ID}/cities`,
            );

            setIsLoading(false);
            setCities(response.data.results);
        } catch (error) {
            setIsLoading(false);
            console.log(error.response.data.message);
        }
    };

    // callbacks
    const handlePresentModalPress = React.useCallback(() => {
        bottomSheetModalRef.current?.present();
    }, []);
    const handleCloseModalPress = React.useCallback(ID => {
        getCities(ID);
        bottomSheetModalRef.current?.dismiss();
    }, []);

    const handlePresentModalCitiesPress = React.useCallback(() => {
        bottomSheetModalCitiesRef.current?.present();
    }, []);
    const handleCloseModalCitiesPress = React.useCallback(ID => {
        bottomSheetModalCitiesRef.current?.dismiss();
    }, []);

    React.useEffect(() => {
        getProvinces();
    }, []);

    return (
        <View
            style={{
                backgroundColor: COLORS.WHITE,
                paddingHorizontal: SIZES.PADDING - 8,
                paddingBottom: 24,
                flex: 1,
                paddingTop: 16,
            }}
        >
            {/* Choose province */}
            <BottomSheetModalProvider>
                <View style={{ zIndex: 99999 }}>
                    <BottomSheetModal
                        ref={bottomSheetModalRef}
                        index={0}
                        snapPoints={snapPointsModal}
                        handleComponent={() => (
                            <View style={{ alignSelf: 'center' }}>
                                <View
                                    style={{
                                        width: 40,
                                        height: 6,
                                        borderRadius: 3,
                                        backgroundColor: COLORS.BLACK,
                                        marginTop: 9,
                                    }}
                                ></View>
                            </View>
                        )}
                        backgroundComponent={() => (
                            <View
                                style={{
                                    ...StyleSheet.absoluteFillObject,
                                    borderTopLeftRadius: 10,
                                    borderTopRightRadius: 10,
                                    backgroundColor: COLORS.GRAY7,
                                }}
                            />
                        )}
                    >
                        <BottomSheetFlatList
                            data={provinces}
                            keyExtractor={item => `${item.province_id}`}
                            style={{ backgroundColor: COLORS.GRAY7, flex: 1 }}
                            contentContainerStyle={{
                                backgroundColor: COLORS.GRAY7,
                            }}
                            renderItem={({ item }) => {
                                return (
                                    <TouchableHighlight
                                        underlayColor={COLORS.GREEN500}
                                        style={{
                                            paddingVertical: 16,
                                            paddingHorizontal: 16,
                                            marginHorizontal: 16,
                                            borderRadius: 8,
                                            backgroundColor:
                                                location.province.id ===
                                                item.province_id
                                                    ? COLORS.GREEN500
                                                    : null,
                                        }}
                                        onPress={() => {
                                            console.log(item.province_name);
                                            setLocation({
                                                ...location,
                                                province: {
                                                    id: item.province_id,
                                                    name: item.province,
                                                },
                                            });
                                            handleCloseModalPress(
                                                item.province_id,
                                            );
                                        }}
                                    >
                                        <View
                                            style={{
                                                flexDirection: 'row',
                                                alignItems: 'center',
                                            }}
                                        >
                                            <Text
                                                style={{
                                                    ...FONTS.body4,
                                                    color: COLORS.BLACK,
                                                }}
                                            >
                                                {item.province}
                                            </Text>
                                        </View>
                                    </TouchableHighlight>
                                );
                            }}
                        />
                    </BottomSheetModal>
                </View>
            </BottomSheetModalProvider>

            {/* Choose city */}
            <BottomSheetModalProvider>
                <View style={{ zIndex: 99999 }}>
                    <BottomSheetModal
                        ref={bottomSheetModalCitiesRef}
                        index={0}
                        snapPoints={snapPointsModal}
                        handleComponent={() => (
                            <View style={{ alignSelf: 'center' }}>
                                <View
                                    style={{
                                        width: 40,
                                        height: 6,
                                        borderRadius: 3,
                                        backgroundColor: COLORS.BLACK,
                                        marginTop: 9,
                                    }}
                                ></View>
                            </View>
                        )}
                        backgroundComponent={() => (
                            <View
                                style={{
                                    ...StyleSheet.absoluteFillObject,
                                    borderTopLeftRadius: 10,
                                    borderTopRightRadius: 10,
                                    backgroundColor: COLORS.GRAY7,
                                }}
                            />
                        )}
                    >
                        {isLoading ? (
                            <View
                                style={{
                                    flex: 1,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}
                            >
                                <ActivityIndicator
                                    color={COLORS.GREEN500}
                                    size="large"
                                />
                            </View>
                        ) : (
                            <BottomSheetFlatList
                                data={cities}
                                keyExtractor={item => `${item.id}`}
                                style={{
                                    backgroundColor: COLORS.GRAY7,
                                    flex: 1,
                                }}
                                contentContainerStyle={{
                                    backgroundColor: COLORS.GRAY7,
                                }}
                                renderItem={({ item }) => {
                                    return (
                                        <TouchableHighlight
                                            underlayColor={COLORS.GREEN500}
                                            style={{
                                                paddingVertical: 16,
                                                paddingHorizontal: 16,
                                                marginHorizontal: 16,
                                                borderRadius: 8,
                                                backgroundColor:
                                                    location.city.id ===
                                                    item.city_id
                                                        ? COLORS.GREEN500
                                                        : null,
                                            }}
                                            onPress={() => {
                                                setLocation({
                                                    ...location,
                                                    city: {
                                                        id: item.city_id,
                                                        name: item.city_name,
                                                    },
                                                });
                                                handleCloseModalCitiesPress(
                                                    item.city_id,
                                                );
                                            }}
                                        >
                                            <View
                                                style={{
                                                    flexDirection: 'row',
                                                    alignItems: 'center',
                                                }}
                                            >
                                                <Text
                                                    style={{
                                                        ...FONTS.body4,
                                                        color:
                                                            location.city.id ===
                                                            item.city_id
                                                                ? COLORS.WHITE
                                                                : COLORS.BLACK,
                                                    }}
                                                >
                                                    {item.city_name}
                                                </Text>
                                            </View>
                                        </TouchableHighlight>
                                    );
                                }}
                            />
                        )}
                    </BottomSheetModal>
                </View>
            </BottomSheetModalProvider>

            {/* Title */}
            <View style={{ zIndex: -1 }}>
                <Text
                    style={{
                        ...FONTS.largeTitleBold,
                        color: COLORS.BLACK,
                        marginBottom: 16,
                    }}
                >
                    Lokasi{'\n'}Kejadian
                </Text>
                {/* Provinsi */}
                <Ripple
                    style={{
                        ...FONTS.body4,
                        paddingHorizontal: 16,
                        paddingVertical: 16,
                        backgroundColor: COLORS.GRAY7,
                        borderRadius: 8,
                        color: COLORS.GRAY,
                        marginBottom: 16,
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                    }}
                    rippleContainerBorderRadius={16}
                    onPress={handlePresentModalPress}
                >
                    <Text style={{ ...FONTS.body4, color: COLORS.GRAY }}>
                        {location.province.name !== ''
                            ? location.province.name
                            : 'Pilih Provinsi'}
                    </Text>

                    <ICONS.CHEVRON
                        style={{ transform: [{ rotate: '-90deg' }] }}
                    />
                </Ripple>
                {/* Kota */}
                <Ripple
                    style={{
                        ...FONTS.body4,
                        paddingHorizontal: 16,
                        paddingVertical: 16,
                        backgroundColor: COLORS.GRAY7,
                        borderRadius: 8,
                        color: COLORS.GRAY,
                        marginBottom: 16,
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                    }}
                    onPress={() => {
                        if (location.province.id) {
                            handlePresentModalCitiesPress();
                        } else {
                            Alert.alert(
                                'Peringatan!',
                                'Pilih Provinsi terlebih dahulu',
                            );
                        }
                    }}
                    rippleContainerBorderRadius={16}
                >
                    <Text style={{ ...FONTS.body4, color: COLORS.GRAY }}>
                        {location.city.name !== ''
                            ? location.city.name
                            : ' Pilih Kota'}
                    </Text>

                    <ICONS.CHEVRON
                        style={{ transform: [{ rotate: '-90deg' }] }}
                    />
                </Ripple>

                <TextInput
                    placeholder="Ketik Detail Alamat"
                    style={INPUTSTYLE}
                    multiline
                    value={location.addressDetails}
                    onChangeText={value =>
                        setLocation({ ...location, addressDetails: value })
                    }
                />
            </View>
        </View>
    );
};

export default SheetContentPlaceContainer;
