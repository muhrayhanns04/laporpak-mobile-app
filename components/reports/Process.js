import React from 'react';
import { View, Text, ScrollView, RefreshControl } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { COLORS, FONTS } from '../../constants';
import Ripple from 'react-native-material-ripple';
import { capitalize, wait } from '../../helper';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import { setBaseUrl } from '../../constants/config';

const Process = ({ sheetProcess, navigation }) => {
    const [refreshing, setRefreshing] = React.useState(false);
    const [reports, setReports] = React.useState([]);
    const [isLoading, setIsLoading] = React.useState(false);

    const onRefresh = React.useCallback(() => {
        setRefreshing(true);
        getReportsProcess();
        wait(1200).then(() => setRefreshing(false));
    }, []);

    React.useEffect(() => {
        getReportsProcess();
    }, []);

    React.useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            getReportsProcess();
        });

        return unsubscribe;
    }, [navigation]);

    const getReportsProcess = async () => {
        setBaseUrl();
        // setDefaultHeader();
        try {
            setIsLoading(true);
            let token = await AsyncStorage.getItem('@token');
            const profile = await axios.get(`/profile`);
            let item = profile.data.results;

            const response = await axios.get(`/complaints/filter/${item.nik}`, {
                params: {
                    sort: 'status',
                    value: 'proses',
                },
                headers: {
                    Authorization: 'Bearer ' + token,
                },
            });

            setIsLoading(false);
            setReports(response.data.results);
        } catch (error) {
            setIsLoading(false);
            console.log(error?.response?.data?.message, 'PROSES');
        }
    };

    return (
        <ScrollView
            style={{ zIndex: -9999 }}
            contentContainerStyle={{
                paddingHorizontal: 16,
            }}
            refreshControl={
                <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
            }
        >
            {reports?.map((item, index) => {
                return (
                    <Ripple
                        onPress={() => {
                            sheetProcess(item);
                        }}
                        style={{ zIndex: -1 }}
                    >
                        <LinearGradient
                            key={index}
                            colors={['#E1F7E8', '#F7EEE1']}
                            start={{ x: 0, y: 0 }}
                            end={{ x: 1, y: 0 }}
                            style={{
                                position: 'relative',
                                flexDirection: 'row',
                                alignItems: 'center',
                                borderRadius: 15,
                                paddingVertical: 16,
                                paddingHorizontal: 24,
                                marginBottom: 16,
                            }}
                        >
                            <View
                                style={{
                                    position: 'absolute',
                                    left: -4,
                                    width: 6,
                                    borderRadius: 999,
                                    height: 74,
                                    backgroundColor: COLORS.GREEN600,
                                    zIndex: 9999,
                                }}
                            />
                            <View>
                                <Text
                                    style={{
                                        ...FONTS.h4SemiBold,
                                        color: COLORS.GREEN800,
                                    }}
                                >
                                    {capitalize(item?.type_of_report)}
                                </Text>

                                <Text
                                    style={{
                                        ...FONTS.h4SemiBold,
                                        color: COLORS.BLACK,
                                        marginVertical: 8,
                                    }}
                                    numberOfLines={1}
                                >
                                    {item.message}
                                </Text>

                                <Text
                                    style={{
                                        ...FONTS.h4,
                                        color: COLORS.GRAY,
                                    }}
                                    numberOfLines={1}
                                >
                                    {item.created_at}
                                </Text>
                            </View>
                        </LinearGradient>
                    </Ripple>
                );
            })}
        </ScrollView>
    );
};

export default Process;
