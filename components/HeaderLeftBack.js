import { useNavigation } from '@react-navigation/native';
import React from 'react';
import Ripple from 'react-native-material-ripple';
import { COLORS, ICONS } from '../constants';

const HeaderLeftBack = () => {
    const navigation = useNavigation();
    return (
        <Ripple
            onPress={() => navigation.goBack()}
            style={{
                padding: 8,
            }}
            rippleContainerBorderRadius={9999}
        >
            <ICONS.CHEVRON
                width={24}
                height={24}
                style={{ color: COLORS.BLACK }}
            />
        </Ripple>
    );
};

export default HeaderLeftBack;
