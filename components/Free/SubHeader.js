import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import { dateConvertedV1, dateConvertedV2, formatDate } from '../../helper';
import axios from 'axios';
import { setBaseUrl } from '../../constants/config';
import { ReportContext } from '../../context/ReportContext';
import { COLORS, FONTS, ICONS } from '../../constants';

const SubHeader = () => {
    const [DatePickerVisibility, setDatePickerVisibility] =
        React.useState(false);
    const [isLoading, setIsLoading] = React.useState(false);
    const {
        dateFilter,
        setDateFilter,
        setReportsProcess,
        typeScreen,
        setReportsFinish,
    } = React.useContext(ReportContext);

    const showDatePicker = () => {
        setDatePickerVisibility(true);
    };
    const hideDatePicker = () => {
        setDatePickerVisibility(false);
    };

    const handleConfirmDate = async params => {
        setIsLoading(true);
        setBaseUrl();
        try {
            const YEAR = params?.getFullYear();
            const MONTH = ('0' + (params?.getMonth() + 1)).slice(-2);
            const DATE = ('0' + params?.getDate()).slice(-2);
            const result = YEAR + '-' + MONTH + '-' + DATE;
            setDateFilter(result);

            const responseProcess = await axios.get(`/complaints/free`, {
                params: {
                    sort: 'status_with_time',
                    value: 'proses',
                    type: 'free',
                    second_value: result,
                },
            });

            const responseFinish = await axios.get(`/responses/free`, {
                params: {
                    sort: 'time',
                    type: 'free',
                    value: result,
                },
            });

            // console.log(responseFinish.data.results);

            setIsLoading(false);
            if (typeScreen === 'process') {
                setReportsProcess(responseProcess.data.results);
            } else if (typeScreen === 'finish') {
                setReportsFinish(responseFinish.data.results);
            } else {
                setReportsProcess(responseProcess.data.results);
            }
            console.log(responseProcess.data.results);
            console.log(responseFinish.data.results);
        } catch (error) {
            setIsLoading(false);
            console.log(error?.response?.data?.message);
        }
        hideDatePicker();
    };

    console.log(typeScreen);

    return (
        <View
            style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'flex-end',
                paddingHorizontal: 16,
            }}
        >
            <DateTimePickerModal
                isVisible={DatePickerVisibility}
                mode="date"
                onConfirm={handleConfirmDate}
                onCancel={hideDatePicker}
            />
            <TouchableOpacity
                onPress={() => showDatePicker()}
                style={{
                    borderColor: COLORS.GREEN800,
                    backgroundColor: COLORS.GREEN100,
                    borderWidth: 1,
                    paddingVertical: 10,
                    paddingHorizontal: 12,
                    borderRadius: 8,
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center',
                }}
            >
                {dateFilter ? (
                    <Text
                        style={{
                            ...FONTS.body13,
                            color: COLORS.BLACK,
                        }}
                    >
                        {formatDate(dateFilter)}
                    </Text>
                ) : (
                    <ICONS.CALENDER style={{ color: COLORS.GREEN800 }} />
                )}
                <ICONS.CHEVRON
                    width={26}
                    height={26}
                    style={{
                        color: COLORS.BLACK,
                        transform: [{ rotate: '-90deg' }],
                        marginLeft: 8,
                    }}
                />
            </TouchableOpacity>
        </View>
    );
};

export default SubHeader;
