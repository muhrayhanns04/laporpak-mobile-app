import React from 'react';
import { ScrollView, View, Text, TouchableOpacity } from 'react-native';
import { COLORS, FONTS, ICONS } from '../../../constants';
import TextComponent from '../../Home/TextComponent';
import CarouselContainer from '../../carousel/CarouselContainer';
import { capitalize } from '../../../helper';

const SheetReportProcess = ({ propsItem }) => {
    return (
        <ScrollView
            style={{
                flex: 1,
                backgroundColor: COLORS.WHITE,
            }}
            contentContainerStyle={{
                backgroundColor: COLORS.WHITE,
                paddingHorizontal: 16,
                paddingBottom: 24,
            }}
        >
            <TouchableOpacity activeOpacity={1}>
                <Text
                    style={{
                        ...FONTS.largeTitleBold,
                        color: COLORS.BLACK,
                        marginBottom: 24,
                    }}
                >
                    Laporan{'\n'}Warga{'\n'}Jakarta
                </Text>
                {/* title pengaduan */}
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                    }}
                >
                    <ICONS.FIRE width={24} height={24} />
                    <Text
                        style={{
                            ...FONTS.h4Bold,
                            color: COLORS.BLACK,
                            marginLeft: 10,
                        }}
                    >
                        Pengaduan
                    </Text>
                </View>
                <TextComponent numberOfLines={4} seeMore={true}>
                    {propsItem?.message}
                </TextComponent>
                {/* title location */}
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        marginTop: 24,
                    }}
                >
                    <ICONS.PIN width={24} height={24} />
                    <Text
                        style={{
                            ...FONTS.h4Bold,
                            color: COLORS.BLACK,
                            marginLeft: 10,
                        }}
                    >
                        Lokasi
                    </Text>
                </View>
                <Text
                    style={{
                        ...FONTS.body5,
                        color: COLORS.GRAY,
                        marginTop: 12,
                    }}
                >
                    {propsItem?.detail_place}
                    {'\n'}
                    {propsItem?.incident_place}
                </Text>

                {/* title status */}
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        marginTop: 24,
                    }}
                >
                    <ICONS.WATCH width={24} height={24} />
                    <Text
                        style={{
                            ...FONTS.h4Bold,
                            color: COLORS.BLACK,
                            marginLeft: 10,
                        }}
                    >
                        {capitalize(propsItem?.status)}
                    </Text>
                </View>

                {/* title time */}
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        marginTop: 24,
                    }}
                >
                    <ICONS.CLOCK width={24} height={24} />
                    <Text
                        style={{
                            ...FONTS.h4Bold,
                            color: COLORS.BLACK,
                            marginLeft: 10,
                        }}
                    >
                        {propsItem?.time_of_occurrence} -{' '}
                        {propsItem?.incident_date}
                    </Text>
                </View>

                {/* proof photo */}
                {propsItem?.files >= 1 ? (
                    <View
                        style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            marginTop: 24,
                        }}
                    >
                        <ICONS.SPARKLES width={24} height={24} />
                        <Text
                            style={{
                                ...FONTS.h4Bold,
                                color: COLORS.BLACK,
                                marginLeft: 10,
                            }}
                        >
                            Foto Bukti
                        </Text>
                    </View>
                ) : null}

                {propsItem?.files ? (
                    <CarouselContainer
                        contentContainerStyle={{
                            marginTop: 24,
                            paddingHorizontal: 16,
                        }}
                        dotColor={[
                            COLORS.GREEN100,
                            COLORS.GREEN600,
                            COLORS.GREEN100,
                        ]}
                        datas={propsItem?.files}
                    />
                ) : null}
            </TouchableOpacity>
        </ScrollView>
    );
};

export default SheetReportProcess;
