import React from 'react';
import {
    ScrollView,
    View,
    Text,
    TouchableHighlight,
    TouchableOpacity,
    TouchableWithoutFeedback,
    Alert,
    ActivityIndicator,
} from 'react-native';
import { COLORS, FONTS, ICONS, SIZES } from '../../../constants';
import TextComponent from '../../Home/TextComponent';
import CarouselContainer from '../../carousel/CarouselContainer';
import { BottomSheetScrollView } from '@gorhom/bottom-sheet';

const SheetReplyFinish = ({ propsItemFinish }) => {
    const [isLoadingSend, setIsLoadingSend] = React.useState(false);

    const renderDevider = () => {
        return (
            <View
                style={{
                    height: 1,
                    backgroundColor: COLORS.WHITE3,
                    marginBottom: 24,
                }}
            />
        );
    };

    return (
        <ScrollView
            style={{
                flex: 1,
                backgroundColor: COLORS.WHITE,
            }}
            contentContainerStyle={{
                backgroundColor: COLORS.WHITE,
                paddingHorizontal: 16,
                paddingBottom: 24,
            }}
        >
            <TouchableOpacity activeOpacity={1}>
                <Text
                    style={{
                        ...FONTS.largeTitleBold,
                        color: COLORS.BLACK,
                    }}
                >
                    Laporan{'\n'}Sudah{'\n'}Ditanggapi
                </Text>

                {/* title pengaduan */}
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        marginTop: 24,
                        marginBottom: 12,
                    }}
                >
                    <View
                        style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                        }}
                    >
                        <ICONS.BISSEP width={24} height={24} />
                        <Text
                            style={{
                                ...FONTS.h4Bold,
                                color: COLORS.YELLOW,
                                marginLeft: 10,
                            }}
                        >
                            Tanggapan
                        </Text>
                    </View>
                </View>

                <TextComponent
                    numberOfLines={4}
                    seeMore={true}
                    containerStyle={{ marginBottom: 24 }}
                >
                    {propsItemFinish?.message}
                </TextComponent>

                {/* title time */}
                {propsItemFinish?.files.length >= 1 ? (
                    <View
                        style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            marginBottom: 16,
                        }}
                    >
                        <ICONS.SPARKLES width={24} height={24} />
                        <Text
                            style={{
                                ...FONTS.h4Bold,
                                color: COLORS.BLACK,
                                marginLeft: 10,
                            }}
                        >
                            Foto Bukti
                        </Text>
                    </View>
                ) : null}
                {/* slider proof photos */}
                {propsItemFinish?.files.length >= 1 ? (
                    <CarouselContainer
                        contentContainerStyle={{
                            paddingHorizontal: 16,
                        }}
                        dotColor={[
                            COLORS.GREEN100,
                            COLORS.GREEN600,
                            COLORS.GREEN100,
                        ]}
                        datas={propsItemFinish?.files}
                    />
                ) : null}

                {renderDevider()}

                {/* title pengaduan */}
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                    }}
                >
                    <ICONS.FIRE width={24} height={24} />
                    <Text
                        style={{
                            ...FONTS.h4Bold,
                            color: COLORS.BLACK,
                            marginLeft: 10,
                        }}
                    >
                        Pengaduan
                    </Text>
                </View>
                <TextComponent numberOfLines={4} seeMore={true}>
                    {propsItemFinish?.report?.message}
                </TextComponent>
                {/* title location */}
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        marginTop: 24,
                    }}
                >
                    <ICONS.PIN width={24} height={24} />
                    <Text
                        style={{
                            ...FONTS.h4Bold,
                            color: COLORS.BLACK,
                            marginLeft: 10,
                        }}
                    >
                        Lokasi
                    </Text>
                </View>
                <Text
                    style={{
                        ...FONTS.body5,
                        color: COLORS.GRAY,
                        marginTop: 12,
                    }}
                >
                    {propsItemFinish?.report?.detail_place}
                    {'\n'}
                    {propsItemFinish?.report?.incident_place}
                </Text>
                {/* title time */}
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        marginTop: 24,
                    }}
                >
                    <ICONS.CLOCK width={24} height={24} />
                    <Text
                        style={{
                            ...FONTS.h4Bold,
                            color: COLORS.BLACK,
                            marginLeft: 10,
                        }}
                    >
                        {propsItemFinish?.report?.time_of_occurrence} -{' '}
                        {propsItemFinish?.report?.incident_date}
                    </Text>
                </View>
                {/* proof photo */}
                {propsItemFinish?.report?.files >= 1 ? (
                    <View
                        style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            marginTop: 24,
                        }}
                    >
                        <ICONS.SPARKLES width={24} height={24} />
                        <Text
                            style={{
                                ...FONTS.h4Bold,
                                color: COLORS.BLACK,
                                marginLeft: 10,
                            }}
                        >
                            Foto Bukti
                        </Text>
                    </View>
                ) : null}

                {propsItemFinish?.report?.files ? (
                    <CarouselContainer
                        contentContainerStyle={{
                            marginTop: 24,
                            paddingHorizontal: 16,
                        }}
                        dotColor={[
                            COLORS.GREEN100,
                            COLORS.GREEN600,
                            COLORS.GREEN100,
                        ]}
                        datas={propsItemFinish?.report?.files}
                    />
                ) : null}
            </TouchableOpacity>
        </ScrollView>
    );
};

export default SheetReplyFinish;
