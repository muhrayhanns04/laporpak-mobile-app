import React from 'react';
import {
    ScrollView,
    View,
    Text,
    SafeAreaView,
    ActivityIndicator,
    RefreshControl,
    Alert,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { COLORS, FONTS } from '../../../constants';
import Ripple from 'react-native-material-ripple';
import { capitalize, formatDate, nameCencor, wait } from '../../../helper';
import {
    setBaseUrl,
    setBaseUrlV2,
    setDefaultHeader,
} from '../../../constants/config';
import RBSheet from 'react-native-raw-bottom-sheet';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import SheetReportProcess from '../BottomSheet/SheetReportProcess';
import { useNavigation } from '@react-navigation/native';
import { ReportContext } from '../../../context/ReportContext';

const ReportProcessContainer = () => {
    const [refreshing, setRefreshing] = React.useState(false);
    const sheetReport = React.useRef(null);
    const [propsItem, setPropsItem] = React.useState(null);
    const { reportsProcess, setReportsProcess, setDateFilter, setTypeScreen } =
        React.useContext(ReportContext);
    const [isLoading, setIsLoading] = React.useState(false);
    const navigation = useNavigation();

    const getRepliesProcess = async () => {
        setIsLoading(true);
        setBaseUrl();
        try {
            const response = await axios.get(`/complaints/free`, {
                params: {
                    sort: 'status',
                    value: 'proses',
                    type: 'free',
                },
            });

            setIsLoading(false);
            setReportsProcess(response.data.results);
            setDateFilter(null);
        } catch (error) {
            setIsLoading(false);
            console.log(error?.response?.data?.message);
        }
    };

    const handleDownload = async id => {
        // setBaseUrlV2();
        try {
            setIsLoading(true);
            const response = await axios.get(
                `http://192.168.100.6:8000/print/3`,
            );

            setIsLoading(false);
            console.log('Berhasli');
        } catch (error) {
            setIsLoading(false);
            console.log(error);
        }
    };

    const onRefresh = React.useCallback(() => {
        setRefreshing(true);
        getRepliesProcess();
        wait(1200).then(() => setRefreshing(false));
    }, []);

    // sheet open
    function handleForPropsSheetProcess(item) {
        setPropsItem(item);
        sheetReport.current.open();
    }

    React.useEffect(() => {
        getRepliesProcess();
    }, []);

    React.useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            getRepliesProcess();
            setTypeScreen('process');
        });

        return unsubscribe;
    }, [navigation]);

    return isLoading ? (
        <SafeAreaView
            style={{
                flex: 1,
                backgroundColor: COLORS.WHITE,
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
            }}
        >
            <ActivityIndicator size="small" color={COLORS.GREEN600} />
            <Text
                style={{
                    ...FONTS.body6,
                    marginTop: 4,
                    color: COLORS.GREEN500,
                }}
            >
                Memuat
            </Text>
        </SafeAreaView>
    ) : (
        <SafeAreaView
            style={{
                flex: 1,
                backgroundColor: COLORS.WHITE,
                paddingHorizontal: 16,
            }}
        >
            <RBSheet
                ref={sheetReport}
                closeOnDragDown={true}
                closeOnPressMask={true}
                keyboardAvoidingViewEnabled={true}
                height={700}
                customStyles={{
                    draggableIcon: {
                        backgroundColor: '#000',
                    },
                    container: {
                        borderTopRightRadius: 16,
                        borderTopLeftRadius: 16,
                    },
                }}
            >
                <SheetReportProcess
                    propsItem={propsItem}
                    sheetReport={sheetReport}
                    getRepliesProcess={() => getRepliesProcess()}
                />
            </RBSheet>

            <ScrollView
                style={{
                    flex: 1,
                }}
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{
                    backgroundColor: COLORS.WHITE,
                    paddingTop: 16,
                }}
                refreshControl={
                    <RefreshControl
                        refreshing={refreshing}
                        onRefresh={onRefresh}
                    />
                }
            >
                {reportsProcess?.map((item, index) => {
                    return (
                        <Ripple
                            onPress={() => {
                                handleForPropsSheetProcess(item);
                            }}
                            style={{ zIndex: -1 }}
                        >
                            <LinearGradient
                                key={index}
                                colors={['#E1F7E8', '#F7EEE1']}
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                style={{
                                    position: 'relative',
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    borderRadius: 15,
                                    paddingVertical: 16,
                                    paddingHorizontal: 24,
                                    marginBottom: 16,
                                }}
                            >
                                <View
                                    style={{
                                        position: 'absolute',
                                        left: -4,
                                        width: 6,
                                        borderRadius: 999,
                                        height: 74,
                                        backgroundColor: COLORS.GREEN600,
                                        zIndex: 9999,
                                    }}
                                />
                                <View>
                                    <Text
                                        style={{
                                            ...FONTS.h4SemiBold,
                                            color: COLORS.GREEN800,
                                        }}
                                    >
                                        {nameCencor(item?.user?.username)}
                                    </Text>
                                    <Text
                                        style={{
                                            ...FONTS.h4SemiBold,
                                            color: COLORS.GREEN800,
                                        }}
                                    >
                                        {capitalize(item?.type_of_report)}
                                    </Text>

                                    <Text
                                        style={{
                                            ...FONTS.h4SemiBold,
                                            color: COLORS.BLACK,
                                            marginVertical: 8,
                                        }}
                                        numberOfLines={1}
                                    >
                                        {item.message}
                                    </Text>

                                    <Text
                                        style={{
                                            ...FONTS.h4,
                                            color: COLORS.GRAY,
                                        }}
                                        numberOfLines={1}
                                    >
                                        {formatDate(item.created_at)}
                                    </Text>
                                </View>
                            </LinearGradient>
                        </Ripple>
                    );
                })}
            </ScrollView>
        </SafeAreaView>
    );
};

export default ReportProcessContainer;
