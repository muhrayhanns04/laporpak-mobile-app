import React from 'react';
import Ripple from 'react-native-material-ripple';
import { ActivityIndicator, Text } from 'react-native';
import { COLORS, FONTS } from '../constants';

const GreenButton = ({ containerStyle, title, onPress, isLoading }) => {
    return (
        <Ripple
            onPress={() => onPress()}
            style={{
                paddingVertical: 16,
                backgroundColor: COLORS.GREEN600,
                borderRadius: 99999,
                ...containerStyle,
            }}
            rippleContainerBorderRadius={9999}
        >
            {isLoading ? (
                <ActivityIndicator size="small" color={COLORS.WHITE} />
            ) : (
                <Text
                    style={{
                        ...FONTS.h4SemiBold,
                        color: COLORS.WHITE,
                        textAlign: 'center',
                    }}
                >
                    {title}
                </Text>
            )}
        </Ripple>
    );
};

export default GreenButton;
