export default [
    {
        id: 1,
        url: require('../illustrations/carousel/carousel-1.png'),
    },
    {
        id: 2,
        url: require('../illustrations/carousel/carousel-2.png'),
    },
    {
        id: 3,
        url: require('../illustrations/carousel/carousel-3.png'),
    },
];
