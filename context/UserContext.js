import React, { createContext } from 'react';
export const UserContext = createContext();

const UserProvider = ({ children }) => {
    const [user, setUser] = React.useState({
        username: '',
        password: '',
        nik: '',
        name: '',
        telp: '',
        level: 'masyarakat',
    });

    const [report, setReport] = React.useState({
        text: '',
        type: 'Pengaduan',
        time: new Date(),
        date: new Date(),
    });
    const [proofPhotos, setProofPhotos] = React.useState(null);
    const [location, setLocation] = React.useState({
        province: {
            id: '',
            name: '',
        },
        city: {
            id: '',
            name: '',
        },
        subdistrict: {
            id: '',
            name: '',
        },
        addressDetails: '',
    });

    return (
        <UserContext.Provider
            value={{
                user,
                setUser,
                location,
                setLocation,
                report,
                setReport,
                proofPhotos,
                setProofPhotos,
            }}
        >
            {children}
        </UserContext.Provider>
    );
};

export default UserProvider;
