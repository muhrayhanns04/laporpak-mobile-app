import React, { createContext } from 'react';
export const ReportContext = createContext();

const ReportProvider = ({ children }) => {
    const [report, setReport] = React.useState({
        text: '',
        type: 'Pengaduan',
        time: new Date(),
        date: new Date(),
    });
    const [dateFilter, setDateFilter] = React.useState(null);
    const [typeScreen, setTypeScreen] = React.useState('process');
    const [proofPhotos, setProofPhotos] = React.useState(null);
    const [location, setLocation] = React.useState({
        province: {
            id: '',
            name: '',
        },
        city: {
            id: '',
            name: '',
        },
        subdistrict: {
            id: '',
            name: '',
        },
        addressDetails: '',
    });
    const [reportsProcess, setReportsProcess] = React.useState(null);
    const [reportsFinish, setReportsFinish] = React.useState(null);

    return (
        <ReportContext.Provider
            value={{
                location,
                setLocation,
                report,
                setReport,
                proofPhotos,
                setProofPhotos,
                reportsProcess,
                setReportsProcess,
                dateFilter,
                setDateFilter,
                typeScreen,
                setTypeScreen,
                reportsFinish,
                setReportsFinish,
            }}
        >
            {children}
        </ReportContext.Provider>
    );
};

export default ReportProvider;
