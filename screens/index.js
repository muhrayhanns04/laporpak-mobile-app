import LoginScreen from './LoginScreen';
import RegisterScreen from './RegisterScreen';
import HomeScreen from './HomeScreen';
import FormReportScreen from './FormReportScreen';
import OfficerHomeScreen from './Officer/OfficerHomeScreen';
import SettingsScreen from './SettingsScreen';

export {
    LoginScreen,
    SettingsScreen,
    RegisterScreen,
    HomeScreen,
    FormReportScreen,
    OfficerHomeScreen,
};
