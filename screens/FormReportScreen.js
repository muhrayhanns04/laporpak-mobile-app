import {
    Alert,
    Image,
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    TextInput,
    View,
} from 'react-native';
import BottomSheet, { BottomSheetBackdrop } from '@gorhom/bottom-sheet';
import { FONTS, ICONS, SIZES } from '../constants';
import { dateConvertedV1, formatAMPM, formatDate } from '../helper';
import { setBaseUrl, setDefaultHeader } from '../constants/config';

import AsyncStorage from '@react-native-async-storage/async-storage';
import { COLORS } from '../constants/index';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import GreenButton from '../components/GreenButton';
import ImagePicker from 'react-native-image-crop-picker';
import RBSheet from 'react-native-raw-bottom-sheet';
import React from 'react';
import { ReportContext } from '../context/ReportContext';
import Ripple from 'react-native-material-ripple';
import SheetContentClassification from '../components/FormCreate/SheetContentClassification';
import SheetContentPlaceContainer from '../components/FormCreate/SheetContentPlaceContainer';
import axios from 'axios';

const INPUTSTYLE = {
    ...FONTS.body4,
    paddingHorizontal: 16,
    paddingLeft: 16,
    paddingRight: 42,
    backgroundColor: COLORS.GRAY7,
    borderRadius: 8,
    color: COLORS.GRAY,
    marginBottom: 16,
};

const FormReportScreen = ({ navigation }) => {
    const [isDatePickerVisible, setDatePickerVisibility] =
        React.useState(false);
    const [isTimePickerVisible, setTimePickerVisibility] =
        React.useState(false);
    const sheetReport = React.useRef(null);
    const sheetClassification = React.useRef(null);
    const snapPointsClassification = React.useMemo(() => ['45%'], []);
    const [isLoading, setIsLoading] = React.useState(false);
    const {
        report,
        setReport,
        location,
        setLocation,
        proofPhotos,
        setProofPhotos,
    } = React.useContext(ReportContext);

    React.useEffect(() => {
        setProofPhotos(null);
        setReport({
            text: '',
            type: 'Pengaduan',
            time: new Date(),
            date: new Date(),
        });
        setLocation({
            province: {
                id: '',
                name: '',
            },
            city: {
                id: '',
                name: '',
            },
            subdistrict: {
                id: '',
                name: '',
            },
            addressDetails: '',
        });
    }, []);

    // handle change date & time value
    const showDatePicker = () => {
        setDatePickerVisibility(true);
    };
    const hideDatePicker = () => {
        setDatePickerVisibility(false);
    };
    const handleConfirmDate = date => {
        setReport({ ...report, date: date });
        hideDatePicker();
    };
    const showTimePicker = () => {
        setTimePickerVisibility(true);
    };
    const hideTimePicker = () => {
        setDatePickerVisibility(false);
    };
    const handleConfirmTime = time => {
        setReport({ ...report, time: time });
        hideTimePicker();
    };

    // Choose image from library
    const choosePhotoFromLibrary = () => {
        ImagePicker.openPicker({
            width: 128,
            height: 128,
            multiple: true,
            cropping: true,
            enableRotationGesture: true,
        }).then(image => {
            return setProofPhotos(image);
        });
    };

    // bottom sheet property
    const handleSnapPress = React.useCallback(index => {
        sheetReport.current?.snapToIndex(index);
    }, []);
    function handleForPropsSheetProcess() {
        sheetReport.current.open();
    }
    const handleSnapPressClassification = React.useCallback(index => {
        sheetClassification.current?.snapToIndex(index);
    }, []);
    function handleForPropsSheetClassification() {
        handleSnapPressClassification(0);
    }

    const handleSubmit = async () => {
        setBaseUrl();
        setDefaultHeader();
        try {
            const token = await AsyncStorage.getItem('@token');

            setIsLoading(true);
            const data = new FormData();
            data.append('message', report.text);
            data.append('time_of_occurrence', formatAMPM(report.time));
            data.append('incident_date', dateConvertedV1(report.date));
            data.append(
                'incident_place',
                `${location.province.name}, ${location.city.name}, ${location.subdistrict.name}`,
            );
            data.append('detail_place', location.addressDetails);
            data.append('type_of_report', report?.type.toLowerCase());
            proofPhotos?.map((item, index) => {
                return data.append('file[]', {
                    type: item.mime,
                    uri: item.path,
                    name: `photo_proof_${index + 1}.jpg`,
                });
            });

            if (token == null || token === '') {
                navigation.replace('LoginScreen');
            } else {
                await axios.post(`/complaints/create`, data, {
                    headers: {
                        'content-type': 'multipart/form-data',
                    },
                });

                navigation.replace('HomeScreen');
                setReport({
                    text: '',
                    type: 'Pengaduan',
                    time: new Date(),
                    date: new Date(),
                });
                setLocation({
                    province: {
                        id: '',
                        name: '',
                    },
                    city: {
                        id: '',
                        name: '',
                    },
                    subdistrict: {
                        id: '',
                        name: '',
                    },
                    addressDetails: '',
                });
                Alert.alert(
                    'Berhasil',
                    'Berhasil mengirimkan laporan sebagai Anonim',
                );
                setIsLoading(false);
            }
        } catch (error) {
            setIsLoading(false);
            console.log(error?.response?.data?.message);
            console.log(error);
            if (error?.response?.data?.message.includes('serupa sudah ada')) {
                Alert.alert('Peringatan', error?.response?.data?.message);
            }
        }
    };

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: COLORS.WHITE }}>
            <StatusBar barStyle="dark-content" backgroundColor={COLORS.WHITE} />

            <RBSheet
                ref={sheetReport}
                closeOnDragDown={true}
                closeOnPressMask={true}
                height={500}
                customStyles={{
                    draggableIcon: {
                        backgroundColor: '#000',
                    },
                    container: {
                        borderTopRightRadius: 16,
                        borderTopLeftRadius: 16,
                    },
                }}
            >
                <SheetContentPlaceContainer INPUTSTYLE={INPUTSTYLE} />
            </RBSheet>

            <BottomSheet
                ref={sheetClassification}
                index={-1}
                snapPoints={snapPointsClassification}
                enablePanDownToClose={true}
                enableContentPanningGesture={true}
                backdropComponent={backdropProps => (
                    <BottomSheetBackdrop
                        {...backdropProps}
                        enableTouchThrough={true}
                        disappearsOnIndex={-1}
                    />
                )}
                handleComponent={() => (
                    <View style={{ alignSelf: 'center' }}>
                        <View
                            style={{
                                width: 40,
                                height: 6,
                                borderRadius: 3,
                                backgroundColor: COLORS.GREEN500,
                                marginTop: 9,
                            }}
                        ></View>
                    </View>
                )}
                backgroundComponent={() => (
                    <View
                        style={{
                            ...StyleSheet.absoluteFillObject,
                            borderTopLeftRadius: 10,
                            borderTopRightRadius: 10,
                            backgroundColor: COLORS.WHITE,
                        }}
                    />
                )}
            >
                <SheetContentClassification
                    report={report}
                    setReport={setReport}
                />
            </BottomSheet>

            <ScrollView
                contentContainerStyle={{
                    paddingHorizontal: 16,
                    paddingTop: 24,
                    paddingBottom: 24,
                    backgroundColor: COLORS.WHITE,
                }}
                style={{ flex: 1, zIndex: -999 }}
            >
                <DateTimePickerModal
                    isVisible={isDatePickerVisible}
                    mode="date"
                    onConfirm={handleConfirmDate}
                    onCancel={hideDatePicker}
                />
                <DateTimePickerModal
                    isVisible={isTimePickerVisible}
                    mode="time"
                    is24Hour={true}
                    onConfirm={handleConfirmTime}
                    onCancel={hideTimePicker}
                />

                {/* Klasifikasi */}
                <Ripple
                    style={{
                        ...FONTS.body4,
                        paddingHorizontal: 16,
                        paddingVertical: 16,
                        backgroundColor: COLORS.GRAY7,
                        borderRadius: 8,
                        color: COLORS.GRAY,
                        marginBottom: 16,
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                    }}
                    rippleContainerBorderRadius={16}
                    onPress={handleForPropsSheetClassification}
                >
                    <Text style={{ ...FONTS.body4, color: COLORS.GRAY }}>
                        {report.type}
                    </Text>

                    <ICONS.CHEVRON
                        style={{ transform: [{ rotate: '-90deg' }] }}
                    />
                </Ripple>

                {/* Message */}
                <TextInput
                    placeholder="Ketik Laporan"
                    style={INPUTSTYLE}
                    multiline
                    value={report.text}
                    onChangeText={value =>
                        setReport({ ...report, text: value })
                    }
                />

                {/* Time */}
                <Ripple
                    style={{
                        ...FONTS.body4,
                        paddingHorizontal: 16,
                        paddingVertical: 16,
                        backgroundColor: COLORS.GRAY7,
                        borderRadius: 8,
                        color: COLORS.GRAY,
                        marginBottom: 16,
                    }}
                    rippleContainerBorderRadius={16}
                    onPress={() => showTimePicker()}
                >
                    <Text style={{ ...FONTS.body4, color: COLORS.GRAY }}>
                        {formatAMPM(report?.time)}
                    </Text>
                </Ripple>

                {/* Date */}
                <Ripple
                    style={{
                        ...FONTS.body4,
                        paddingHorizontal: 16,
                        paddingVertical: 16,
                        backgroundColor: COLORS.GRAY7,
                        borderRadius: 8,
                        color: COLORS.GRAY,
                        marginBottom: 16,
                    }}
                    rippleContainerBorderRadius={16}
                    onPress={() => showDatePicker()}
                >
                    <Text style={{ ...FONTS.body4, color: COLORS.GRAY }}>
                        {formatDate(report.date)}
                    </Text>
                </Ripple>

                {/* Place */}
                <Ripple
                    style={{
                        ...FONTS.body4,
                        paddingHorizontal: 16,
                        paddingVertical: 16,
                        backgroundColor: COLORS.GRAY7,
                        borderRadius: 8,
                        color: COLORS.GRAY,
                        marginBottom: 16,
                    }}
                    rippleContainerBorderRadius={16}
                    onPress={handleForPropsSheetProcess}
                >
                    <Text style={{ ...FONTS.body4, color: COLORS.GRAY }}>
                        {location.province.name === ''
                            ? 'Pilih Tempat Kejadian'
                            : `${location.province.name}, ${location.city.name}`}
                    </Text>
                </Ripple>

                {proofPhotos == null ? (
                    <Ripple
                        onPress={() => choosePhotoFromLibrary()}
                        style={{
                            width: SIZES.width / 1.1,
                            height: SIZES.width / 1.84,
                            borderRadius: 15,
                            backgroundColor: COLORS.GRAY7,
                            flexDirection: 'column',
                            alignItems: 'center',
                            alignSelf: 'center',
                            justifyContent: 'center',
                        }}
                    >
                        <View
                            style={{
                                flexDirection: 'column',
                                alignItems: 'center',
                                position: 'absolute',
                            }}
                        >
                            <ICONS.IMAGE
                                style={{ color: COLORS.GRAY }}
                                width={42}
                                height={42}
                            />
                            <Text
                                style={{
                                    ...FONTS.h4,
                                    color: COLORS.GRAY,
                                    marginTop: 4,
                                }}
                            >
                                Pilih foto bukti
                            </Text>
                        </View>
                    </Ripple>
                ) : null}

                {/* digital proof */}
                {proofPhotos?.map(item => {
                    return (
                        <Ripple
                            onPress={() => choosePhotoFromLibrary()}
                            style={{
                                width: SIZES.width / 1.1,
                                height: SIZES.width / 1.84,
                                borderRadius: 15,
                                backgroundColor: COLORS.GRAY7,
                                flexDirection: 'column',
                                alignItems: 'center',
                                alignSelf: 'center',
                                justifyContent: 'center',
                                marginBottom: 16,
                                shadowColor: '#000',
                                shadowOffset: {
                                    width: 0,
                                    height: 1,
                                },
                                shadowOpacity: 0.22,
                                shadowRadius: 2.22,

                                elevation: 3,
                            }}
                        >
                            <Image
                                source={{
                                    uri: item.path,
                                }}
                                style={{
                                    width: SIZES.width / 1.1,
                                    height: SIZES.width / 1.84,
                                    borderRadius: 15,
                                    backgroundColor:
                                        item.path !== null ? '' : COLORS.GRAY5,
                                }}
                            />
                        </Ripple>
                    );
                })}
                <GreenButton
                    isLoading={isLoading}
                    title="Kirim"
                    onPress={() => handleSubmit()}
                    containerStyle={{ marginTop: 24 }}
                />
            </ScrollView>
        </SafeAreaView>
    );
};

export default FormReportScreen;
