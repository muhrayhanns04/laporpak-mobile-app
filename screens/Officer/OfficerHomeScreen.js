import {
    ActivityIndicator,
    Alert,
    Image,
    SafeAreaView,
    ScrollView,
    StatusBar,
    Text,
    TouchableHighlight,
    TouchableOpacity,
    TouchableWithoutFeedback,
    View,
} from 'react-native';
import BottomSheet, {
    BottomSheetBackdrop,
    BottomSheetScrollView,
} from '@gorhom/bottom-sheet';
import { COLORS, FONTS, ICONS, SIZES } from '../../constants';
import { INSTANCE, setBaseUrl, setDefaultHeader } from '../../constants/config';

import AsyncStorage from '@react-native-async-storage/async-storage';
import CarouselContainer from '../../components/carousel/CarouselContainer';
import { Header } from '../../components/Home';
import ImageCropPicker from 'react-native-image-crop-picker';
import RBSheet from 'react-native-raw-bottom-sheet';
import React from 'react';
import { ReportContext } from '../../context/ReportContext';
import Ripple from 'react-native-material-ripple';
import SlidingTabContainer from '../../components/officer/SlidingTab/SlidingTabContainer';
import TextComponent from '../../components/Home/TextComponent';
import TextInputComponent from '../../components/FormCreate/TextInputComponent';
import { UserContext } from '../../context/UserContext';
import axios from 'axios';

const OfficerHomeScreen = ({ navigation }) => {
    const sheetReport = React.useRef(null);
    const sheetResponse = React.useRef(null);
    const [propsItem, setPropsItem] = React.useState(null);
    const [propsItemFinish, setPropsItemFinish] = React.useState(null);
    const [reportsProcess, setReportsProcess] = React.useState(null);
    const [reportsFinish, setReportsFinish] = React.useState(null);
    const [isLoading, setIsLoading] = React.useState(false);
    const [isLoadingSend, setIsLoadingSend] = React.useState(false);
    const { user, setUser } = React.useContext(UserContext);
    const { proofPhotos, setProofPhotos } = React.useContext(ReportContext);
    const [reply, setReply] = React.useState({
        message: '',
    });
    const snapPoints = React.useMemo(() => ['30%', '90%'], []);

    // Fetching
    const getProfile = async () => {
        setBaseUrl();
        setDefaultHeader();
        try {
            let token = await AsyncStorage.getItem('@token');

            setIsLoading(true);
            const response = await axios.get(`/profile`);

            let item = response.data.results;

            setUser({
                ...user,
                username: item.username,
                nik: item.nik,
                name: item.name,
                telp: item.telp,
                level: item.level,
            });

            setIsLoading(false);
        } catch (error) {
            setIsLoading(false);
            console.log(error);
            console.log(error?.response?.data?.message);
        }
    };
    const getRepliesProcess = async () => {
        setIsLoading(true);
        setBaseUrl();
        setDefaultHeader();
        try {
            let token = await AsyncStorage.getItem('@token');
            const profile = await axios.get(`/profile`);
            let item = profile.data.results;
            console.log(item.id);

            const response = await axios.get(`/complaints/free`, {
                params: {
                    sort: 'status',
                    value: 'proses',
                    type: 'free',
                },
            });

            setIsLoading(false);
            setReportsProcess(response.data.results);
        } catch (error) {
            setIsLoading(false);
            console.log(error?.response?.data?.message);
        }
    };
    const getReportsFinish = async () => {
        setIsLoading(true);
        setBaseUrl();
        setDefaultHeader();
        try {
            let token = await AsyncStorage.getItem('@token');
            const profile = await axios.get(`/profile`);
            let item = profile.data.results;

            const response = await axios.get(`/responses/filter/${item.id}`, {
                params: {
                    sort: 'byID',
                },
            });

            setIsLoading(false);
            setReportsFinish(response.data.results);
        } catch (error) {
            setIsLoading(false);
            console.log(error?.response?.data?.message);
        }
    };
    const toggleSubmitResponse = async report_id => {
        setBaseUrl();
        setDefaultHeader();
        try {
            setIsLoadingSend(true);
            const data = new FormData();
            data.append('message', reply.message);
            proofPhotos?.map((item, index) => {
                return data.append('file[]', {
                    type: item.mime,
                    uri: item.path,
                    name: `photo_proof_${index + 1}.jpg`,
                });
            });
            data.append('report_id', report_id);
            await axios.post('/responses/create', data);

            getRepliesProcess();
            getReportsFinish();
            setReply({
                message: '',
            });
            sheetReport.current.close();
            setIsLoadingSend(false);
        } catch (e) {
            setIsLoadingSend(false);
            console.log(e.response.data);
        }
    };

    const toggleSubmitUpdate = async reply_id => {
        setBaseUrl();
        setDefaultHeader();
        try {
            setIsLoadingSend(true);
            const data = new FormData();
            data.append('message', reply.message);

            await axios.post('/responses/update/' + reply_id, data);

            getRepliesProcess();
            getReportsFinish();
            sheetResponse.current.close();
            setIsLoadingSend(false);
        } catch (e) {
            setIsLoadingSend(false);
            console.log(e.response.data);
        }
    };

    // Choose image from library
    const choosePhotoFromLibrary = () => {
        ImageCropPicker.openPicker({
            width: 128,
            height: 128,
            multiple: true,
            cropping: true,
            enableRotationGesture: true,
        }).then(image => {
            return setProofPhotos(image);
        });
    };

    // sheet content
    const renderSheetContent = () => {
        return (
            <ScrollView
                style={{
                    flex: 1,
                    backgroundColor: COLORS.WHITE,
                }}
                contentContainerStyle={{
                    backgroundColor: COLORS.WHITE,
                    paddingHorizontal: 16,
                    paddingBottom: 24,
                }}
            >
                <TouchableOpacity activeOpacity={1}>
                    <Text
                        style={{ ...FONTS.largeTitleBold, color: COLORS.BLACK }}
                    >
                        Segera{'\n'}Tanggapi{'\n'}Laporan
                    </Text>
                    {/* title pengaduan */}
                    <View
                        style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'space-between',
                            marginTop: 24,
                            marginBottom: 12,
                        }}
                    >
                        <View
                            style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                            }}
                        >
                            <ICONS.BISSEP width={24} height={24} />
                            <Text
                                style={{
                                    ...FONTS.h4Bold,
                                    color: COLORS.YELLOW,
                                    marginLeft: 10,
                                }}
                            >
                                Tanggapan
                            </Text>
                        </View>
                        <TouchableHighlight
                            underlayColor="rgba(0,0,0,0.5)"
                            onPress={() => toggleSubmitResponse(propsItem.id)}
                            style={{
                                borderRadius: 9999,
                            }}
                        >
                            {isLoadingSend ? (
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        alignItems: 'center',
                                        backgroundColor: COLORS.GRAY7,
                                        paddingVertical: 4,
                                        paddingHorizontal: 8,
                                        borderRadius: 9999,
                                    }}
                                >
                                    <ActivityIndicator
                                        size="small"
                                        color={COLORS.GREEN500}
                                    />
                                </View>
                            ) : (
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        alignItems: 'center',
                                        backgroundColor: COLORS.GRAY7,
                                        paddingVertical: 4,
                                        paddingHorizontal: 8,
                                        borderRadius: 9999,
                                    }}
                                >
                                    <ICONS.CHECK width={24} height={24} />
                                    <Text
                                        style={{
                                            ...FONTS.h4Bold,
                                            color: COLORS.GREEN500,
                                            marginLeft: 10,
                                        }}
                                    >
                                        Simpan
                                    </Text>
                                </View>
                            )}
                        </TouchableHighlight>
                    </View>

                    <TextInputComponent
                        placeholder="Ketikkan tanggapan terhadap laporan ini"
                        value={reply.message}
                        onChangeText={value =>
                            setReply({ ...reply, message: value })
                        }
                        keyboardType="web-search"
                        multiline={true}
                    />

                    {/* title time */}
                    <TouchableWithoutFeedback
                        onPress={() =>
                            Alert.alert(
                                '🎁 Petunjuk',
                                'Unggah foto bukti dengan cara tekan gambar dibawah ini!',
                            )
                        }
                    >
                        <View
                            style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                                marginBottom: 16,
                            }}
                        >
                            <ICONS.SPARKLES width={24} height={24} />
                            <Text
                                style={{
                                    ...FONTS.h4Bold,
                                    color: COLORS.BLACK,
                                    marginLeft: 10,
                                }}
                            >
                                Unggah Foto Bukti{' '}
                                <Text style={{ color: COLORS.GREEN600 }}>
                                    *(Lihat Petunjuk)
                                </Text>
                            </Text>
                        </View>
                    </TouchableWithoutFeedback>
                    {proofPhotos ? (
                        <CarouselContainer
                            contentContainerStyle={{
                                marginTop: 8,
                                paddingHorizontal: 16,
                            }}
                            dotColor={[
                                COLORS.GREEN100,
                                COLORS.GREEN600,
                                COLORS.GREEN100,
                            ]}
                            onPress={choosePhotoFromLibrary}
                            datas={proofPhotos}
                            type="upload"
                        />
                    ) : (
                        <TouchableOpacity
                            onPress={choosePhotoFromLibrary}
                            style={{
                                marginBottom: 24,
                                flexDirection: 'column',
                                alignItems: 'center',
                                justifyContent: 'center',
                                backgroundColor: COLORS.GRAY3,
                                height: SIZES.width / 1.7,
                                borderRadius: 15,
                            }}
                        >
                            <ICONS.IMAGE
                                style={{
                                    color: COLORS.GRAY,
                                }}
                            />
                        </TouchableOpacity>
                    )}

                    {renderDevider()}
                    {/* title pengaduan */}
                    <View
                        style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                        }}
                    >
                        <ICONS.FIRE width={24} height={24} />
                        <Text
                            style={{
                                ...FONTS.h4Bold,
                                color: COLORS.BLACK,
                                marginLeft: 10,
                            }}
                        >
                            Pengaduan
                        </Text>
                    </View>
                    <TextComponent numberOfLines={4} seeMore={true}>
                        {propsItem?.message}
                    </TextComponent>
                    {/* title location */}
                    <View
                        style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            marginTop: 24,
                        }}
                    >
                        <ICONS.PIN width={24} height={24} />
                        <Text
                            style={{
                                ...FONTS.h4Bold,
                                color: COLORS.BLACK,
                                marginLeft: 10,
                            }}
                        >
                            Lokasi
                        </Text>
                    </View>
                    <Text
                        style={{
                            ...FONTS.body5,
                            color: COLORS.GRAY,
                            marginTop: 12,
                        }}
                    >
                        {propsItem?.detail_place}
                        {'\n'}
                        {propsItem?.incident_place}
                    </Text>
                    {/* title time */}
                    <View
                        style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            marginTop: 24,
                        }}
                    >
                        <ICONS.CLOCK width={24} height={24} />
                        <Text
                            style={{
                                ...FONTS.h4Bold,
                                color: COLORS.BLACK,
                                marginLeft: 10,
                            }}
                        >
                            {propsItem?.time_of_occurrence} -{' '}
                            {propsItem?.incident_date}
                        </Text>
                    </View>
                    {/* proof photo */}
                    <View
                        style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            marginTop: 24,
                        }}
                    >
                        <ICONS.SPARKLES width={24} height={24} />
                        <Text
                            style={{
                                ...FONTS.h4Bold,
                                color: COLORS.BLACK,
                                marginLeft: 10,
                            }}
                        >
                            Foto Bukti
                        </Text>
                    </View>
                    {propsItem?.files ? (
                        <CarouselContainer
                            contentContainerStyle={{
                                marginTop: 24,
                                paddingHorizontal: 16,
                            }}
                            dotColor={[
                                COLORS.GREEN100,
                                COLORS.GREEN600,
                                COLORS.GREEN100,
                            ]}
                            datas={propsItem?.files}
                        />
                    ) : null}
                </TouchableOpacity>
            </ScrollView>
        );
    };

    const renderSheetContentFinish = () => {
        return (
            <BottomSheetScrollView
                style={{
                    flex: 1,
                    backgroundColor: COLORS.WHITE,
                }}
                contentContainerStyle={{
                    backgroundColor: COLORS.WHITE,
                    paddingHorizontal: 16,
                    paddingBottom: 24,
                }}
            >
                <Text
                    style={{
                        ...FONTS.largeTitleBold,
                        color: COLORS.BLACK,
                    }}
                >
                    Laporan{'\n'}Sudah{'\n'}Ditanggapi
                </Text>

                {/* title pengaduan */}
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        marginTop: 24,
                        marginBottom: 12,
                    }}
                >
                    <View
                        style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                        }}
                    >
                        <ICONS.BISSEP width={24} height={24} />
                        <Text
                            style={{
                                ...FONTS.h4Bold,
                                color: COLORS.YELLOW,
                                marginLeft: 10,
                            }}
                        >
                            Tanggapan
                        </Text>
                    </View>
                    {/* <TouchableHighlight
                        underlayColor="rgba(0,0,0,0.5)"
                        onPress={() => toggleSubmitResponse(propsItem.id)}
                        style={{
                            borderRadius: 9999,
                        }}
                    >
                        {isLoadingSend ? (
                            <View
                                style={{
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    backgroundColor: COLORS.GRAY7,
                                    paddingVertical: 4,
                                    paddingHorizontal: 8,
                                    borderRadius: 9999,
                                }}
                            >
                                <ActivityIndicator
                                    size="small"
                                    color={COLORS.GREEN500}
                                />
                            </View>
                        ) : (
                            <View
                                style={{
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    backgroundColor: COLORS.GRAY7,
                                    paddingVertical: 4,
                                    paddingHorizontal: 8,
                                    borderRadius: 9999,
                                }}
                            >
                                <ICONS.SCISSORS width={24} height={24} />
                                <Text
                                    style={{
                                        ...FONTS.h4Bold,
                                        color: COLORS.BLACK,
                                        marginLeft: 10,
                                    }}
                                >
                                    Memperbaharui
                                </Text>
                            </View>
                        )}
                    </TouchableHighlight> */}
                </View>

                <TextComponent
                    numberOfLines={4}
                    seeMore={true}
                    containerStyle={{ marginBottom: 24 }}
                >
                    {propsItemFinish?.message}
                </TextComponent>

                {/* title time */}
                {propsItemFinish?.files.length >= 1 ? (
                    <View
                        style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            marginBottom: 16,
                        }}
                    >
                        <ICONS.SPARKLES width={24} height={24} />
                        <Text
                            style={{
                                ...FONTS.h4Bold,
                                color: COLORS.BLACK,
                                marginLeft: 10,
                            }}
                        >
                            Foto Bukti
                        </Text>
                    </View>
                ) : null}
                {/* slider proof photos */}
                {propsItemFinish?.files.length >= 1 ? (
                    <CarouselContainer
                        contentContainerStyle={{
                            paddingHorizontal: 16,
                        }}
                        dotColor={[
                            COLORS.GREEN100,
                            COLORS.GREEN600,
                            COLORS.GREEN100,
                        ]}
                        datas={propsItemFinish?.files}
                    />
                ) : null}

                {renderDevider()}

                {/* title pengaduan */}
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                    }}
                >
                    <ICONS.FIRE width={24} height={24} />
                    <Text
                        style={{
                            ...FONTS.h4Bold,
                            color: COLORS.BLACK,
                            marginLeft: 10,
                        }}
                    >
                        Pengaduan
                    </Text>
                </View>
                <TextComponent numberOfLines={4} seeMore={true}>
                    {propsItemFinish?.report?.message}
                </TextComponent>
                {/* title location */}
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        marginTop: 24,
                    }}
                >
                    <ICONS.PIN width={24} height={24} />
                    <Text
                        style={{
                            ...FONTS.h4Bold,
                            color: COLORS.BLACK,
                            marginLeft: 10,
                        }}
                    >
                        Lokasi
                    </Text>
                </View>
                <Text
                    style={{
                        ...FONTS.body5,
                        color: COLORS.GRAY,
                        marginTop: 12,
                    }}
                >
                    {propsItemFinish?.report?.detail_place}
                    {'\n'}
                    {propsItemFinish?.report?.incident_place}
                </Text>
                {/* title time */}
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        marginTop: 24,
                    }}
                >
                    <ICONS.CLOCK width={24} height={24} />
                    <Text
                        style={{
                            ...FONTS.h4Bold,
                            color: COLORS.BLACK,
                            marginLeft: 10,
                        }}
                    >
                        {propsItemFinish?.report?.time_of_occurrence} -{' '}
                        {propsItemFinish?.report?.incident_date}
                    </Text>
                </View>
                {/* proof photo */}
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        marginTop: 24,
                    }}
                >
                    <ICONS.SPARKLES width={24} height={24} />
                    <Text
                        style={{
                            ...FONTS.h4Bold,
                            color: COLORS.BLACK,
                            marginLeft: 10,
                        }}
                    >
                        Foto Bukti
                    </Text>
                </View>
                {propsItemFinish?.report?.files ? (
                    <CarouselContainer
                        contentContainerStyle={{
                            marginTop: 24,
                            paddingHorizontal: 16,
                        }}
                        dotColor={[
                            COLORS.GREEN100,
                            COLORS.GREEN600,
                            COLORS.GREEN100,
                        ]}
                        datas={propsItemFinish?.report?.files}
                    />
                ) : null}
            </BottomSheetScrollView>
        );
    };

    const renderDevider = () => {
        return (
            <View
                style={{
                    height: 1,
                    backgroundColor: COLORS.WHITE3,
                    marginBottom: 24,
                }}
            />
        );
    };

    // sheet open
    function handleForPropsSheetProcess(item) {
        setPropsItem(item);
        sheetReport.current.open();
    }
    const handleSnapPressForResponse = React.useCallback(index => {
        sheetResponse.current?.snapToIndex(index);
    }, []);
    function handleForPropsSheetResponse(item) {
        setPropsItemFinish(item);
        // sheetResponse.current.open();
        handleSnapPressForResponse(0);
    }

    React.useEffect(() => {
        getProfile();
        getReportsFinish();
        getRepliesProcess();
    }, []);

    // React.useEffect(() => {
    //     const unsubscribe = navigation.addListener('focus', () => {
    //         getReportsFinish();
    //         getRepliesProcess();
    //     });

    //     return unsubscribe;
    // }, [navigation]);

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: COLORS.WHITE }}>
            <RBSheet
                ref={sheetReport}
                closeOnDragDown={true}
                closeOnPressMask={true}
                keyboardAvoidingViewEnabled={true}
                height={700}
                onClose={() => {}}
                customStyles={{
                    // for backdrop
                    // wrapper: {
                    //     backgroundColor: 'transparent',
                    // },
                    draggableIcon: {
                        backgroundColor: '#000',
                    },
                    container: {
                        borderTopRightRadius: 16,
                        borderTopLeftRadius: 16,
                    },
                }}
            >
                {renderSheetContent()}
            </RBSheet>

            <BottomSheet
                ref={sheetResponse}
                index={-1}
                snapPoints={snapPoints}
                enablePanDownToClose={true}
                enableContentPanningGesture={true}
                activeOffsetY={[-1, 1]}
                failOffsetX={[-5, 5]}
                animateOnMount={true}
                backdropComponent={backdropProps => (
                    <BottomSheetBackdrop
                        {...backdropProps}
                        enableTouchThrough={true}
                        disappearsOnIndex={-1}
                    />
                )}
            >
                {renderSheetContentFinish()}
            </BottomSheet>
            {/* <RBSheet
                ref={sheetResponse}
                closeOnDragDown={true}
                closeOnPressMask={true}
                keyboardAvoidingViewEnabled={true}
                height={700}
                customStyles={{
                    draggableIcon: {
                        backgroundColor: '#000',
                    },
                    container: {
                        borderTopRightRadius: 16,
                        borderTopLeftRadius: 16,
                    },
                }}
            >
                {renderSheetContentFinish()}
            </RBSheet> */}

            <ScrollView
                style={{ zIndex: -999 }}
                contentContainerStyle={{
                    flex: 1,
                    backgroundColor: COLORS.WHITE,
                    paddingTop: 16,
                }}
            >
                <Header />

                <SlidingTabContainer
                    contentContainerStyle={{
                        marginTop: 8,
                        // paddingHorizontal: 16,
                    }}
                    reports={reportsProcess}
                    reportsFinish={reportsFinish}
                    getReportsProcess={() => getRepliesProcess()}
                    getReportsFinish={() => getReportsFinish()}
                    sheetProcess={item => handleForPropsSheetProcess(item)}
                    sheetFinish={item => handleForPropsSheetResponse(item)}
                />

                <StatusBar
                    barStyle="dark-content"
                    backgroundColor={COLORS.WHITE}
                />
            </ScrollView>
        </SafeAreaView>
    );
};

export default OfficerHomeScreen;
