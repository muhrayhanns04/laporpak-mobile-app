import BottomSheet, { BottomSheetBackdrop } from '@gorhom/bottom-sheet';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import React from 'react';
import { View, SafeAreaView, StatusBar, ScrollView, Alert } from 'react-native';
import { SheetContentNik } from '../components/Settings';
import SettingItem from '../components/Settings/SettingItem';
import { COLORS, ICONS } from '../constants';
import { setBaseUrl, setDefaultHeader } from '../constants/config';
import { UserContext } from '../context/UserContext';
import AnimatedLottieView from 'lottie-react-native';

const SettingsScreen = ({ navigation }) => {
    const { user, setUser } = React.useContext(UserContext);
    const [isLoading, setIsLoading] = React.useState(false);
    const sheetReport = React.useRef(null);
    const snapPoints = React.useMemo(() => ['30%', '90%'], []);

    const handleSnapPress = React.useCallback(index => {
        sheetReport.current?.snapToIndex(index);
    }, []);

    const getProfile = async () => {
        setBaseUrl();
        setDefaultHeader();
        try {
            setIsLoading(true);
            const response = await axios.get(`/profile`);

            let item = response?.data?.results;

            setIsLoading(false);
            setUser({
                ...user,
                username: item?.username,
                nik: item?.nik,
                name: item?.name,
                telp: item?.telp,
                level: item?.level,
            });
        } catch (error) {
            setIsLoading(false);
            console.log(error);
            console.log(error?.response?.data?.message);
        }
    };

    const handleLogout = async () => {
        setBaseUrl();
        setDefaultHeader();
        try {
            setIsLoading(true);
            const token = await AsyncStorage.getItem('@token');

            if (token == null || token === '') {
                navigation.replace('LoginScreen');
            } else {
                await axios.post(`/logout`);

                setUser({
                    sername: '',
                    password: '',
                    nik: '',
                    name: '',
                    telp: '',
                    level: 'masyarakat',
                });
                await AsyncStorage.removeItem('@token');
                await AsyncStorage.removeItem('@level');
                await AsyncStorage.removeItem('@isLogin');

                setIsLoading(false);
                navigation.replace('LoginScreen');
                Alert.alert(
                    'Berhasil',
                    'Berhasil keluar, sampai jumpa lagi 😊',
                );
            }
        } catch (error) {
            setIsLoading(false);
            console.log(error?.response?.data?.message);

            setUser({
                sername: '',
                password: '',
                nik: '',
                name: '',
                telp: '',
                level: 'masyarakat',
            });
            await AsyncStorage.removeItem('@token');
            await AsyncStorage.removeItem('@level');
            await AsyncStorage.removeItem('@isLogin');

            setIsLoading(false);
            navigation.replace('LoginScreen');
            Alert.alert('Berhasil', 'Berhasil keluar, sampai jumpa lagi 😊');
        }
    };

    React.useEffect(() => {
        setIsLoading(false);
        getProfile();
    }, []);

    if (isLoading) {
        return (
            <SafeAreaView
                style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: COLORS.WHITE,
                    flexDirection: 'column',
                }}
            >
                <View style={{ width: 150, height: 150 }}>
                    <AnimatedLottieView
                        source={require('../assets/lottie/loading.json')}
                        autoPlay={true}
                        loop={true}
                        resizeMode="cover"
                    />
                </View>
            </SafeAreaView>
        );
    } else {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: COLORS.WHITE }}>
                <StatusBar
                    barStyle="dark-content"
                    backgroundColor={COLORS.WHITE}
                />
                <BottomSheet
                    ref={sheetReport}
                    index={-1}
                    snapPoints={snapPoints}
                    enablePanDownToClose={true}
                    enableContentPanningGesture={true}
                    backdropComponent={backdropProps => (
                        <BottomSheetBackdrop
                            {...backdropProps}
                            enableTouchThrough={true}
                            // to activated backdrop
                            disappearsOnIndex={-1}
                        />
                    )}
                >
                    <SheetContentNik />
                </BottomSheet>
                <ScrollView
                    style={{ zIndex: -999 }}
                    contentContainerStyle={{
                        flex: 1,
                        backgroundColor: COLORS.WHITE,
                        paddingTop: 8,
                        paddingHorizontal: 16,
                    }}
                >
                    <View
                        style={{
                            backgroundColor: COLORS.GRAY7,
                            padding: 16,
                            marginVertical: 16,
                            borderRadius: 16,
                        }}
                    >
                        {/* NIK */}
                        {user.level !== 'masyarakat' ? null : (
                            <SettingItem
                                isLoading={isLoading}
                                value={user.nik}
                                icon={() => (
                                    <ICONS.THUNDER width={24} height={24} />
                                )}
                                containerStyle={{ marginBottom: 24 }}
                            />
                        )}
                        {/* Fullname */}
                        <SettingItem
                            isLoading={isLoading}
                            value={user.name}
                            icon={() => (
                                <ICONS.FULLNAME width={24} height={24} />
                            )}
                            containerStyle={{ marginBottom: 24 }}
                        />
                        {/* Username */}
                        <SettingItem
                            isLoading={isLoading}
                            value={user.username}
                            icon={() => (
                                <ICONS.USERNAME width={24} height={24} />
                            )}
                            containerStyle={{ marginBottom: 24 }}
                        />
                        {/* CALL */}
                        <SettingItem
                            isLoading={isLoading}
                            value={user.telp}
                            icon={() => <ICONS.CALL width={24} height={24} />}
                        />
                    </View>

                    <View
                        style={{
                            backgroundColor: COLORS.GRAY7,
                            padding: 16,
                            borderRadius: 16,
                        }}
                    >
                        <SettingItem
                            value="Keluar"
                            onPress={handleLogout}
                            icon={() => <ICONS.LOGOUT width={24} height={24} />}
                        />
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
};

export default SettingsScreen;
