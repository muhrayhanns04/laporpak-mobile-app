import axios from 'axios';
import React from 'react';
import {
    View,
    Text,
    SafeAreaView,
    ScrollView,
    TextInput,
    ActivityIndicator,
    TouchableOpacity,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Ripple from 'react-native-material-ripple';
import RBSheet from 'react-native-raw-bottom-sheet';
import SearchSheetReplyFinish from '../../components/Free/BottomSheet/SearchSheetReplyFinish';
import SheetReplyFinish from '../../components/Free/BottomSheet/SheetReplyFinish';
import SheetReportProcess from '../../components/Free/BottomSheet/SheetReportProcess';
import { COLORS, FONTS, ICONS, SIZES } from '../../constants';
import { setBaseUrl } from '../../constants/config';
import { capitalize, formatDate, nameCencor } from '../../helper';

const ICONSIZE = SIZES.width / 16.3;

const SearchScreen = () => {
    const [reports, setReports] = React.useState([]);
    const [isLoading, setIsLoading] = React.useState(false);
    const [searchKey, setSearchKey] = React.useState(null);

    // Process
    const sheetReport = React.useRef(null);
    const [propsItem, setPropsItem] = React.useState(null);
    function handleForPropsSheetProcess(item) {
        setPropsItem(item);
        sheetReport.current.open();
    }

    const sheetResponse = React.useRef(null);
    const [propsItemFinish, setPropsItemFinish] = React.useState(null);
    function handleForPropsSheetResponse(item) {
        setPropsItemFinish(item);
        sheetResponse.current.open();
    }

    const getRepliesProcess = async () => {
        setBaseUrl();
        try {
            setIsLoading(true);
            const response = await axios.get(`/complaints/free`, {
                params: {
                    sort: 'byNIK',
                    value: searchKey != '' ? searchKey : 1,
                    type: 'free',
                },
            });

            setIsLoading(false);
            setReports(response.data.results);
            console.log(response.data.results);
        } catch (error) {
            setIsLoading(false);
            console.log(error?.response?.data?.message);
        }
    };

    const getReportFinish = async id => {
        setBaseUrl();
        try {
            setIsLoading(true);
            const response = await axios.get(`/complaints/free`, {
                params: {
                    sort: 'status_specific',
                    value: id,
                    type: 'free',
                },
            });

            setIsLoading(false);
            handleForPropsSheetResponse(response.data.results);
        } catch (error) {
            setIsLoading(false);
            console.log(error?.response?.data?.message);
        }
    };

    return isLoading ? (
        <SafeAreaView
            style={{
                flex: 1,
                backgroundColor: COLORS.WHITE,
                justifyContent: 'center',
                alignItems: 'center',
            }}
        >
            <ActivityIndicator size="large" color={COLORS.GREEN600} />
        </SafeAreaView>
    ) : (
        <SafeAreaView
            style={{ flex: 1, backgroundColor: COLORS.WHITE, paddingTop: 16 }}
        >
            <RBSheet
                ref={sheetReport}
                closeOnDragDown={true}
                closeOnPressMask={true}
                keyboardAvoidingViewEnabled={true}
                height={700}
                customStyles={{
                    draggableIcon: {
                        backgroundColor: '#000',
                    },
                    container: {
                        borderTopRightRadius: 16,
                        borderTopLeftRadius: 16,
                    },
                }}
            >
                <SheetReportProcess propsItem={propsItem} />
            </RBSheet>

            <RBSheet
                ref={sheetResponse}
                closeOnDragDown={true}
                closeOnPressMask={true}
                keyboardAvoidingViewEnabled={true}
                height={700}
                customStyles={{
                    draggableIcon: {
                        backgroundColor: '#000',
                    },
                    container: {
                        borderTopRightRadius: 16,
                        borderTopLeftRadius: 16,
                    },
                }}
            >
                <SearchSheetReplyFinish propsItemFinish={propsItemFinish} />
            </RBSheet>

            <View
                style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    paddingVertical: 4,
                    paddingHorizontal: 16,
                    backgroundColor: COLORS.GRAY4,
                    marginBottom: 24,
                    marginHorizontal: 16,
                    borderRadius: 9999,
                }}
            >
                <TextInput
                    placeholder="Masukkan NIK, Contoh: 9473956868776751"
                    placeholderTextColor={COLORS.GRAY}
                    style={{
                        flex: 1,
                        ...FONTS.body13,
                        color: COLORS.BLACK,
                    }}
                    keyboardType="web-search"
                    value={searchKey}
                    onChangeText={setSearchKey}
                    onSubmitEditing={() => {
                        getRepliesProcess();
                    }}
                />

                <TouchableOpacity
                    onPress={() => {
                        getRepliesProcess();
                    }}
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        zIndex: 999999,
                        marginLeft: 16,
                    }}
                >
                    <ICONS.SEARCH
                        width={ICONSIZE}
                        height={ICONSIZE}
                        style={{ color: COLORS.GRAY }}
                    />
                </TouchableOpacity>
            </View>
            {reports.length <= 0 ? (
                <SafeAreaView
                    style={{
                        flex: 0.9,
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}
                >
                    <ICONS.SEARCHILLUS
                        width={300}
                        height={300}
                        style={{ marginBottom: 24 }}
                    />
                    <Text
                        style={{
                            ...FONTS.h4Bold,
                            color: COLORS.BLACK,
                            marginBottom: 8,
                        }}
                    >
                        Masukkan NIK
                    </Text>
                    <Text
                        style={{
                            ...FONTS.body13,
                            color: COLORS.BLACK,
                            textAlign: 'center',
                            width: SIZES.width / 1.3,
                        }}
                    >
                        Silahkan Masukkan NIK yang benar, salah satu angka akan
                        mempengaruhi proses pencarian
                    </Text>
                </SafeAreaView>
            ) : (
                <ScrollView style={{ paddingHorizontal: 16 }}>
                    {reports?.map((item, index) => {
                        return item?.status === 'proses' ? (
                            <Ripple
                                onPress={() => {
                                    handleForPropsSheetProcess(item);
                                }}
                                style={{ zIndex: -1 }}
                            >
                                <LinearGradient
                                    key={index}
                                    colors={['#E1F7E8', '#F7EEE1']}
                                    start={{ x: 0, y: 0 }}
                                    end={{ x: 1, y: 0 }}
                                    style={{
                                        position: 'relative',
                                        flexDirection: 'row',
                                        alignItems: 'center',
                                        borderRadius: 15,
                                        paddingVertical: 16,
                                        paddingHorizontal: 24,
                                        marginBottom: 16,
                                    }}
                                >
                                    <View
                                        style={{
                                            position: 'absolute',
                                            left: -4,
                                            width: 6,
                                            borderRadius: 999,
                                            height: 74,
                                            backgroundColor: COLORS.GREEN600,
                                            zIndex: 9999,
                                        }}
                                    />
                                    <View>
                                        <Text
                                            style={{
                                                ...FONTS.h4SemiBold,
                                                color: COLORS.GREEN800,
                                            }}
                                        >
                                            {item &&
                                                nameCencor(
                                                    item?.user?.username,
                                                )}
                                        </Text>
                                        <Text
                                            style={{
                                                ...FONTS.h4SemiBold,
                                                color: COLORS.GREEN800,
                                            }}
                                        >
                                            {item &&
                                                capitalize(
                                                    item?.type_of_report,
                                                )}
                                        </Text>

                                        <Text
                                            style={{
                                                ...FONTS.h4SemiBold,
                                                color: COLORS.BLACK,
                                                marginVertical: 8,
                                            }}
                                            numberOfLines={1}
                                        >
                                            {item.message}
                                        </Text>

                                        <Text
                                            style={{
                                                ...FONTS.h4,
                                                color: COLORS.GRAY,
                                            }}
                                            numberOfLines={1}
                                        >
                                            {formatDate(item.created_at)}
                                        </Text>
                                    </View>
                                </LinearGradient>
                            </Ripple>
                        ) : (
                            <Ripple
                                onPress={() => {
                                    getReportFinish(item.id);
                                }}
                                style={{ zIndex: -1 }}
                            >
                                <LinearGradient
                                    key={index}
                                    colors={['#FFFFFF', '#E1F7E8']}
                                    start={{ x: 0, y: 0 }}
                                    end={{ x: 1, y: 0 }}
                                    style={{
                                        position: 'relative',
                                        flexDirection: 'row',
                                        alignItems: 'center',
                                        borderRadius: 15,
                                        paddingVertical: 16,
                                        paddingHorizontal: 24,
                                        marginBottom: 16,
                                    }}
                                >
                                    <View
                                        style={{
                                            position: 'absolute',
                                            left: -4,
                                            width: 6,
                                            borderRadius: 999,
                                            height: 74,
                                            backgroundColor: COLORS.GREEN600,
                                            zIndex: 9999,
                                        }}
                                    />
                                    <View>
                                        <Text
                                            style={{
                                                ...FONTS.h4SemiBold,
                                                color: COLORS.GREEN800,
                                            }}
                                        >
                                            {item &&
                                                nameCencor(
                                                    item?.user?.username,
                                                )}
                                        </Text>
                                        <Text
                                            style={{
                                                ...FONTS.h4SemiBold,
                                                color: COLORS.GREEN800,
                                            }}
                                        >
                                            {item &&
                                                capitalize(
                                                    item?.type_of_report,
                                                )}
                                        </Text>

                                        <Text
                                            style={{
                                                ...FONTS.h4SemiBold,
                                                color: COLORS.BLACK,
                                                marginVertical: 8,
                                            }}
                                            numberOfLines={1}
                                        >
                                            {item.message}
                                        </Text>

                                        <Text
                                            style={{
                                                ...FONTS.h4,
                                                color: COLORS.GRAY,
                                            }}
                                            numberOfLines={1}
                                        >
                                            {formatDate(item.created_at)}
                                        </Text>
                                    </View>
                                </LinearGradient>
                            </Ripple>
                        );
                    })}
                </ScrollView>
            )}
        </SafeAreaView>
    );
};

export default SearchScreen;
