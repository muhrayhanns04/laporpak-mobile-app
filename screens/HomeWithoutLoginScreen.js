import React from 'react';
import { ScrollView, View, Text, StatusBar, SafeAreaView } from 'react-native';
import Ripple from 'react-native-material-ripple';
import CarouselContainer from '../components/carousel/CarouselContainer';
import { Header } from '../components/Home';
import SlidingTabContainer from '../components/SlidingTab/SlidingTabContainer';
import { COLORS, FONTS, ICONS, SIZES } from '../constants';
import BottomSheet, {
    BottomSheetBackdrop,
    BottomSheetScrollView,
} from '@gorhom/bottom-sheet';
import axios from 'axios';
import { setBaseUrl, setDefaultHeader } from '../constants/config';
import TextComponent from '../components/Home/TextComponent';
import { UserContext } from '../context/UserContext';
import AsyncStorage from '@react-native-async-storage/async-storage';

const ICONSIZE = SIZES.width / 16.3;
const CIRCLESIZE = SIZES.width / 9.8;

const HomeScreen = ({ navigation }) => {
    const sheetReport = React.useRef(null);
    const sheetResponse = React.useRef(null);
    const [propsItem, setPropsItem] = React.useState(null);
    const [propsItemFinish, setPropsItemFinish] = React.useState(null);
    const [reportsProcess, setReportsProcess] = React.useState(null);
    const [reportsFinish, setReportsFinish] = React.useState(null);
    const [isLoading, setIsLoading] = React.useState(false);
    const snapPoints = React.useMemo(() => ['30%', '90%'], []);

    const { user, setUser } = React.useContext(UserContext);

    // Fetching
    const getProfile = async () => {
        setBaseUrl();
        setDefaultHeader();
        try {
            setIsLoading(true);
            const response = await axios.get(`/complaints/filter?sort=byToken`);

            let item = response.data.results;

            setUser({
                ...user,
                username: item.username,
                nik: item.nik,
                name: item.name,
                telp: item.telp,
            });

            setIsLoading(false);
        } catch (error) {
            setIsLoading(false);
            console.log(error);
            console.log(error?.response?.data?.message);
        }
    };
    const getReportsProcess = async () => {
        setBaseUrl();
        setDefaultHeader();
        try {
            let token = await AsyncStorage.getItem('@token');
            setIsLoading(true);
            const response = await axios.get(`/complaints/free`, {
                params: {
                    sort: 'status',
                    value: 'proses',
                    type: 'free',
                },
            });

            setIsLoading(false);
            setReportsProcess(response.data.results);
        } catch (error) {
            setIsLoading(false);
            console.log(error?.response?.data?.message);
        }
    };
    const getReportsFinish = async () => {
        setBaseUrl();
        setDefaultHeader();
        try {
            let token = await AsyncStorage.getItem('@token');

            setIsLoading(true);
            const response = await axios.get(`/complaints/free`, {
                params: {
                    sort: 'status',
                    value: 'selesai',
                    type: 'free',
                },
            });

            setIsLoading(false);
            setReportsFinish(response.data.results);
        } catch (error) {
            setIsLoading(false);
            console.log(error?.response?.data?.message);
        }
    };

    // Handle open & close bottom sheet
    const handleSnapPress = React.useCallback(index => {
        sheetReport.current?.snapToIndex(index);
    }, []);
    const handleSnapPressForResponse = React.useCallback(index => {
        sheetResponse.current?.snapToIndex(index);
    }, []);

    // sheet content
    const renderSheetContent = () => {
        return (
            <BottomSheetScrollView
                style={{
                    flex: 1,
                    paddingBottom: 24,
                }}
                contentContainerStyle={{
                    backgroundColor: COLORS.WHITE,
                    paddingBottom: 24,
                    paddingHorizontal: 16,
                }}
            >
                <Text style={{ ...FONTS.largeTitleBold, color: COLORS.BLACK }}>
                    Laporan{'\n'}Warga{'\n'}LaporPak
                </Text>
                {/* title pengaduan */}
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        marginTop: 24,
                    }}
                >
                    <ICONS.FIRE width={24} height={24} />
                    <Text
                        style={{
                            ...FONTS.h4Bold,
                            color: COLORS.BLACK,
                            marginLeft: 10,
                        }}
                    >
                        Pengaduan
                    </Text>
                </View>
                <TextComponent numberOfLines={4} seeMore={true}>
                    {propsItem?.message}
                </TextComponent>
                {/* title location */}
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        marginTop: 24,
                    }}
                >
                    <ICONS.PIN width={24} height={24} />
                    <Text
                        style={{
                            ...FONTS.h4Bold,
                            color: COLORS.BLACK,
                            marginLeft: 10,
                        }}
                    >
                        Lokasi
                    </Text>
                </View>
                <Text
                    style={{
                        ...FONTS.body5,
                        color: COLORS.GRAY,
                        marginTop: 12,
                    }}
                >
                    {propsItem?.detail_place}
                    {'\n'}
                    {propsItem?.incident_place}
                </Text>
                {/* title time */}
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        marginTop: 24,
                    }}
                >
                    <ICONS.CLOCK width={24} height={24} />
                    <Text
                        style={{
                            ...FONTS.h4Bold,
                            color: COLORS.BLACK,
                            marginLeft: 10,
                        }}
                    >
                        {propsItem?.time_of_occurrence} -{' '}
                        {propsItem?.incident_date}
                    </Text>
                </View>
                {/* title time */}
                {propsItem?.files.length >= 1 ? (
                    <View
                        style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            marginTop: 24,
                        }}
                    >
                        <ICONS.SPARKLES width={24} height={24} />
                        <Text
                            style={{
                                ...FONTS.h4Bold,
                                color: COLORS.BLACK,
                                marginLeft: 10,
                            }}
                        >
                            Foto Bukti
                        </Text>
                    </View>
                ) : null}

                {propsItem?.files ? (
                    <CarouselContainer
                        contentContainerStyle={{
                            marginTop: 24,
                            paddingHorizontal: 16,
                        }}
                        dotColor={[
                            COLORS.GREEN100,
                            COLORS.GREEN600,
                            COLORS.GREEN100,
                        ]}
                        datas={propsItem?.files}
                    />
                ) : null}
            </BottomSheetScrollView>
        );
    };
    const renderSheetContentForResponse = () => {
        return (
            <BottomSheetScrollView
                style={{
                    flex: 1,
                    paddingBottom: 24,
                }}
                contentContainerStyle={{
                    backgroundColor: COLORS.WHITE,
                    paddingBottom: 24,
                    paddingHorizontal: 16,
                }}
            >
                <Text style={{ ...FONTS.largeTitleBold, color: COLORS.BLACK }}>
                    Kami{'\n'}Sudah{'\n'}Tanggapi
                </Text>
                {/* title pengaduan */}
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        marginTop: 24,
                    }}
                >
                    <ICONS.BISSEP width={24} height={24} />
                    <Text
                        style={{
                            ...FONTS.h4Bold,
                            color: COLORS.YELLOW,
                            marginLeft: 10,
                        }}
                    >
                        Tanggapan
                    </Text>
                </View>
                <TextComponent
                    numberOfLines={4}
                    seeMore={true}
                    containerStyle={{ marginBottom: 24 }}
                >
                    {propsItemFinish?.reply?.message}
                </TextComponent>

                {/* slider proof photos */}
                {propsItemFinish?.reply?.files.length >= 1 ? (
                    <CarouselContainer
                        contentContainerStyle={{
                            paddingHorizontal: 16,
                        }}
                        dotColor={[
                            COLORS.GREEN100,
                            COLORS.GREEN600,
                            COLORS.GREEN100,
                        ]}
                        datas={propsItemFinish?.files}
                    />
                ) : null}

                {renderDevider()}

                {/* title pengaduan */}
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                    }}
                >
                    <ICONS.FIRE width={24} height={24} />
                    <Text
                        style={{
                            ...FONTS.h4Bold,
                            color: COLORS.BLACK,
                            marginLeft: 10,
                        }}
                    >
                        Pengaduan
                    </Text>
                </View>
                <TextComponent numberOfLines={4} seeMore={true}>
                    {propsItemFinish?.message}
                </TextComponent>
                {/* title location */}
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        marginTop: 24,
                    }}
                >
                    <ICONS.PIN width={24} height={24} />
                    <Text
                        style={{
                            ...FONTS.h4Bold,
                            color: COLORS.BLACK,
                            marginLeft: 10,
                        }}
                    >
                        Lokasi
                    </Text>
                </View>
                <Text
                    style={{
                        ...FONTS.body5,
                        color: COLORS.GRAY,
                        marginTop: 12,
                    }}
                >
                    {propsItemFinish?.detail_place}
                    {'\n'}
                    {propsItemFinish?.incident_place}
                </Text>
                {/* title time */}
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        marginTop: 24,
                    }}
                >
                    <ICONS.CLOCK width={24} height={24} />
                    <Text
                        style={{
                            ...FONTS.h4Bold,
                            color: COLORS.BLACK,
                            marginLeft: 10,
                        }}
                    >
                        {propsItemFinish?.time_of_occurrence} -{' '}
                        {propsItemFinish?.incident_date}
                    </Text>
                </View>
                {/* title time */}
                {propsItem?.files.length >= 1 ? (
                    <View
                        style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            marginTop: 24,
                        }}
                    >
                        <ICONS.SPARKLES width={24} height={24} />
                        <Text
                            style={{
                                ...FONTS.h4Bold,
                                color: COLORS.BLACK,
                                marginLeft: 10,
                            }}
                        >
                            Foto Bukti
                        </Text>
                    </View>
                ) : null}

                {propsItemFinish?.files ? (
                    <CarouselContainer
                        contentContainerStyle={{
                            marginTop: 24,
                            paddingHorizontal: 16,
                        }}
                        dotColor={[
                            COLORS.GREEN100,
                            COLORS.GREEN600,
                            COLORS.GREEN100,
                        ]}
                        datas={propsItemFinish?.files}
                    />
                ) : null}
            </BottomSheetScrollView>
        );
    };
    const renderDevider = () => {
        return (
            <View
                style={{
                    height: 1,
                    backgroundColor: COLORS.WHITE3,
                    marginBottom: 24,
                }}
            />
        );
    };

    // sheet open
    function handleForPropsSheetProcess(item) {
        setPropsItem(item);
        handleSnapPress(0);
    }
    function handleForPropsSheetResponse(item) {
        setPropsItemFinish(item);
        handleSnapPressForResponse(0);
    }

    React.useEffect(() => {
        setDefaultHeader();
        getProfile();
        getReportsFinish();
        getReportsProcess();
    }, []);

    // React.useEffect(() => {
    //     const unsubscribe = navigation.addListener('focus', () => {
    //         getReportsFinish();
    //         getReportsProcess();
    //     });

    //     return unsubscribe;
    // }, [navigation]);

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: COLORS.WHITE }}>
            <BottomSheet
                ref={sheetReport}
                index={-1}
                snapPoints={snapPoints}
                enablePanDownToClose={true}
                enableContentPanningGesture={true}
                backdropComponent={backdropProps => (
                    <BottomSheetBackdrop
                        {...backdropProps}
                        enableTouchThrough={true}
                        disappearsOnIndex={-1}
                    />
                )}
                // to make slider working perfectly
                activeOffsetY={[-1, 1]}
                failOffsetX={[-5, 5]}
                animateOnMount={true}
            >
                {renderSheetContent()}
            </BottomSheet>
            <BottomSheet
                ref={sheetResponse}
                index={-1}
                snapPoints={snapPoints}
                enablePanDownToClose={true}
                enableContentPanningGesture={true}
                backdropComponent={backdropProps => (
                    <BottomSheetBackdrop
                        {...backdropProps}
                        enableTouchThrough={true}
                        disappearsOnIndex={-1}
                    />
                )}
                // to make slider working perfectly
                activeOffsetY={[-1, 1]}
                failOffsetX={[-5, 5]}
                animateOnMount={true}
            >
                {renderSheetContentForResponse()}
            </BottomSheet>

            <ScrollView
                style={{ zIndex: -999 }}
                contentContainerStyle={{
                    flex: 1,
                    backgroundColor: COLORS.WHITE,
                    paddingTop: 16,
                }}
            >
                <Header />
                {/* {reportsProcess?.map((item, index) => {
                return <Text>{index}</Text>;
            })} */}

                <Ripple
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        borderWidth: 1,
                        borderColor: COLORS.GRAY5,
                        paddingVertical: 6,
                        paddingRight: 6,
                        paddingLeft: 16,
                        borderRadius: 9999,
                        justifyContent: 'space-between',
                        marginHorizontal: 16,
                        marginTop: 24,
                    }}
                    rippleContainerBorderRadius={9999}
                    onPress={() => navigation.navigate('FormReportSreen')}
                >
                    <Text style={{ ...FONTS.body4, color: COLORS.GRAY8 }}>
                        Tulisakan laporan hari ini...
                    </Text>

                    <View
                        style={{
                            width: CIRCLESIZE,
                            height: CIRCLESIZE,
                            flexDirection: 'column',
                            justifyContent: 'center',
                            alignItems: 'center',
                            backgroundColor: COLORS.GREEN600,
                            borderRadius: 9999,
                        }}
                    >
                        <ICONS.CREATE width={ICONSIZE} height={ICONSIZE} />
                    </View>
                </Ripple>

                <SlidingTabContainer
                    contentContainerStyle={{
                        marginTop: 8,
                        paddingHorizontal: 16,
                    }}
                    reports={reportsProcess}
                    reportsFinish={reportsFinish}
                    getReportsProcess={() => getReportsProcess()}
                    getReportsFinish={() => getReportsFinish()}
                    sheetProcess={item => handleForPropsSheetProcess(item)}
                    sheetFinish={item => handleForPropsSheetResponse(item)}
                />

                <StatusBar
                    barStyle="dark-content"
                    backgroundColor={COLORS.WHITE}
                />
            </ScrollView>
        </SafeAreaView>
    );
};

export default HomeScreen;
