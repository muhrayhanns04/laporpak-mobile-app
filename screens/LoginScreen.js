import React from 'react';
import {
    View,
    Text,
    ScrollView,
    TextInput,
    StatusBar,
    TouchableOpacity,
    Alert,
    TouchableWithoutFeedback,
} from 'react-native';
import GreenButton from '../components/GreenButton';
import KeyboardAvoidWrapper from '../components/KeyboardAvoidWrapper';
import { COLORS, FONTS, IMAGES, SIZES } from '../constants';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { UserContext } from '../context/UserContext';
import axios from 'axios';
import { setBaseUrl, setDefaultHeader } from '../constants/config';
import { CommonActions } from '@react-navigation/native';

const LoginScreen = ({ navigation }) => {
    const [isSecureEntry, setIsSecureEntry] = React.useState(true);
    const { user, setUser } = React.useContext(UserContext);
    const [isLoading, setIsLoading] = React.useState(false);

    const handleSubmitLogin = async () => {
        setIsLoading(true);
        setBaseUrl();
        setDefaultHeader();
        try {
            const response = await axios.post('/login', {
                username: user.username,
                password: user.password,
            });

            const token = response.data.token;
            const level = response.data.results.level;

            if (token && level.toLowerCase() == 'masyarakat') {
                await AsyncStorage.setItem('@token', token);
                await AsyncStorage.setItem('@level', level);
                await AsyncStorage.setItem('@isLogin', '1');

                navigation.dispatch(
                    CommonActions.reset({
                        index: 0,
                        routes: [{ name: 'HomeScreen' }],
                    }),
                );
                setIsLoading(false);
            } else if (token && level.toLowerCase() == 'petugas') {
                await AsyncStorage.setItem('@token', token);
                await AsyncStorage.setItem('@level', level);
                await AsyncStorage.setItem('@isLogin', '1');
                navigation.dispatch(
                    CommonActions.reset({
                        index: 0,
                        routes: [{ name: 'OfficerHomeScreen' }],
                    }),
                );

                setIsLoading(false);
            } else if (token && level.toLowerCase() == 'admin') {
                await AsyncStorage.setItem('@token', token);
                await AsyncStorage.setItem('@level', level);
                await AsyncStorage.setItem('@isLogin', '1');

                navigation.dispatch(
                    CommonActions.reset({
                        index: 0,
                        routes: [{ name: 'AdminHomeScreen' }],
                    }),
                );
            } else {
                setIsLoading(false);
                Alert.alert(
                    'Maaf',
                    'Ada bug, silahkan laporan ke pihak IT LaporPak',
                );
            }
        } catch (error) {
            setIsLoading(false);
            console.log(error);
            Alert.alert('Peringatan', error.response?.data?.message);
        }
    };

    return (
        <KeyboardAvoidWrapper>
            <ScrollView
                contentContainerStyle={{
                    flex: 1,
                    paddingHorizontal: 16,
                    paddingTop: 24,
                    backgroundColor: COLORS.WHITE,
                }}
            >
                {/* Heading */}
                <Text
                    style={{
                        ...FONTS.h24Bold,
                        color: COLORS.black,
                        textAlign: 'center',
                    }}
                >
                    Masuk{' '}
                    <Text style={{ color: COLORS.GREEN600 }}>Sebagai</Text>
                </Text>
                <IMAGES.LOGIN
                    width={SIZES.width / 1.25}
                    height={SIZES.width / 1.7}
                    style={{ marginTop: 42 }}
                />
                {/* Content */}
                <View style={{ marginVertical: 42 }}>
                    <TextInput
                        placeholder="Nama Pengguna"
                        style={{
                            paddingHorizontal: 16,
                            paddingLeft: 16,
                            paddingRight: 42,
                            backgroundColor: COLORS.GRAY7,
                            borderWidth: 2,
                            borderColor: COLORS.lightGRAY3,
                            borderRadius: 8,
                            ...FONTS.h4,
                            color: COLORS.GRAY,
                            marginBottom: 16,
                        }}
                        value={user.username}
                        onChangeText={value =>
                            setUser({ ...user, username: value })
                        }
                    />
                    <View
                        style={{ flexDirection: 'row', alignItems: 'center' }}
                    >
                        <TextInput
                            placeholder="Kata sandi"
                            style={{
                                width: '100%',
                                paddingHorizontal: 16,
                                backgroundColor: COLORS.GRAY7,
                                borderWidth: 2,
                                borderColor: COLORS.lightGRAY3,
                                borderRadius: 8,
                                ...FONTS.h4,
                                color: COLORS.GRAY,
                            }}
                            maxLength={11}
                            secureTextEntry={isSecureEntry ? true : false}
                            keyboardType="default"
                            value={user.password}
                            onChangeText={value =>
                                setUser({ ...user, password: value })
                            }
                        />
                        <TouchableOpacity
                            onPress={() => setIsSecureEntry(!isSecureEntry)}
                            style={{
                                position: 'absolute',
                                top: '30%',
                                right: 16,
                            }}
                        >
                            <Text
                                style={{
                                    ...FONTS.h4,
                                    color: COLORS.GREEN600,
                                }}
                            >
                                {isSecureEntry ? 'Tampilkan' : 'Sembunyikan'}
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
                {/* Button action */}
                <GreenButton
                    isLoading={isLoading}
                    title="Masuk"
                    onPress={handleSubmitLogin}
                />

                <Text
                    style={{
                        ...FONTS.body5,
                        color: COLORS.BLACK,
                        textAlign: 'center',
                        alignItems: 'center',
                        flexDirection: 'row',
                        marginTop: 16,
                        paddingVertical: 4,
                        alignSelf: 'center',
                        width: SIZES.width / 1.6,
                    }}
                >
                    Tidak memiliki akun?{' '}
                    <TouchableWithoutFeedback
                        onPress={() => navigation.navigate('RegisterScreen')}
                    >
                        <Text
                            style={{
                                ...FONTS.h6,
                                color: COLORS.GREEN600,
                            }}
                        >
                            Daftar
                        </Text>
                    </TouchableWithoutFeedback>{' '}
                    atau{' '}
                    <TouchableWithoutFeedback
                        onPress={() => navigation.replace('FreeHomeScreen')}
                    >
                        <Text
                            style={{
                                ...FONTS.h6,
                                color: COLORS.GREEN600,
                            }}
                        >
                            Masuk Tanpa Akun
                        </Text>
                    </TouchableWithoutFeedback>
                </Text>
                <StatusBar
                    barStyle="dark-content"
                    backgroundColor={COLORS.WHITE}
                />
            </ScrollView>
        </KeyboardAvoidWrapper>
    );
};

export default LoginScreen;
