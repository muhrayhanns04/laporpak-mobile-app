import {
    BottomSheetModal,
    BottomSheetModalProvider,
} from '@gorhom/bottom-sheet';
import axios from 'axios';
import React from 'react';
import {
    View,
    Text,
    ScrollView,
    TextInput,
    StatusBar,
    TouchableOpacity,
    Alert,
    SafeAreaView,
} from 'react-native';
import Ripple from 'react-native-material-ripple';
import RBSheet from 'react-native-raw-bottom-sheet';
import GreenButton from '../components/GreenButton';
import KeyboardAvoidWrapper from '../components/KeyboardAvoidWrapper';
import { COLORS, FONTS, ICONS, IMAGES, SIZES } from '../constants';
import { setBaseUrl, setDefaultHeader } from '../constants/config';
import { UserContext } from '../context/UserContext';
import { capitalize, errorsValidation } from '../helper';

const RegisterScreen = ({ navigation }) => {
    const [isSecureEntry, setIsSecureEntry] = React.useState(true);
    const [isLoading, setIsLoading] = React.useState(false);
    const { user, setUser } = React.useContext(UserContext);
    const [formErrors, setFormErrors] = React.useState({});
    const [message, setMessage] = React.useState(null);
    const [type, setType] = React.useState(false);
    const bottomSheetModalRef = React.useRef(null);

    const handleSubmitSignup = async () => {
        setIsLoading(true);
        setBaseUrl();
        setDefaultHeader();
        try {
            const response = await axios.post('/register', {
                username: user.username,
                password: user.password,
                name: user.name,
                nik: user.nik,
                telp: user.telp,
                level: user.level,
            });

            const level = response.data.results.level;

            bottomSheetModalRef.current.open();
            setMessage(
                'Silahkan masuk dengan menggunakan email yang sudah didaftarkan',
            );
            setType(true);
            setIsLoading(false);
        } catch (error) {
            console.log(error);
            bottomSheetModalRef.current.open();
            setMessage(error.response?.data?.message);
            setType(false);
            setIsLoading(false);
        }
    };

    return (
        <KeyboardAvoidWrapper>
            <BottomSheetModalProvider>
                <SafeAreaView
                    style={{
                        flex: 1,
                    }}
                >
                    <RBSheet
                        ref={bottomSheetModalRef}
                        closeOnDragDown={true}
                        keyboardAvoidingViewEnabled={true}
                        height={325}
                        customStyles={{
                            wrapper: {
                                backgroundColor: COLORS.TRANSPARENT,
                                marginBottom: 50,
                                marginHorizontal: 24,
                            },
                            draggableIcon: {
                                backgroundColor: COLORS.GREEN600,
                            },
                            container: {
                                backgroundColor: COLORS.WHITE,
                                borderRadius: 16,

                                shadowColor: '#000',
                                shadowOffset: {
                                    width: 0,
                                    height: 2,
                                },
                                shadowOpacity: 0.25,
                                shadowRadius: 3.84,

                                elevation: 5,
                            },
                        }}
                    >
                        <View
                            style={{
                                flexDirection: 'column',
                                alignItems: 'center',
                                flex: 1,
                                paddingVertical: 16,
                                paddingHorizontal: 16,
                                justifyContent: 'space-between',
                            }}
                        >
                            <Text
                                style={{
                                    ...FONTS.h4Bold,
                                    textAlign: 'center',
                                    width: SIZES.width / 1.5,
                                    color: COLORS.BLACK,
                                    textTransform: 'uppercase',
                                }}
                            >
                                {message}
                            </Text>

                            {!type ? (
                                <ICONS.MAINTENANCE width={120} height={120} />
                            ) : (
                                <ICONS.SEEDLING width={120} height={120} />
                            )}

                            <Ripple
                                style={{
                                    backgroundColor: COLORS.GREEN600,
                                    paddingVertical: 13,
                                    borderRadius: 8,
                                    width: '100%',
                                }}
                                rippleColor={COLORS.WHITE}
                                rippleDuration={500}
                                onPress={() => {
                                    bottomSheetModalRef.current.close();
                                    setMessage('');
                                    setType(false);
                                }}
                            >
                                <Text
                                    style={{
                                        ...FONTS.h13,
                                        textAlign: 'center',
                                        color: COLORS.WHITE,
                                    }}
                                >
                                    Close
                                </Text>
                            </Ripple>
                        </View>
                    </RBSheet>

                    <ScrollView
                        style={{
                            zIndex: -1,
                        }}
                        contentContainerStyle={{
                            flex: 1,
                            paddingHorizontal: 16,
                            paddingVertical: 24,
                            backgroundColor: COLORS.WHITE,
                        }}
                    >
                        <IMAGES.LOGIN
                            width={SIZES.width / 1.25}
                            height={SIZES.width / 1.7}
                            style={{ marginTop: 16 }}
                        />

                        <View style={{ marginTop: 32 }}>
                            <View style={{ marginBottom: 16 }}>
                                <TextInput
                                    keyboardType="number-pad"
                                    placeholder="Masukkan NIK"
                                    onFocus={() =>
                                        setFormErrors(errorsValidation(user))
                                    }
                                    onBlur={() => {
                                        setFormErrors(errorsValidation(user));
                                    }}
                                    onChange={() =>
                                        setFormErrors(errorsValidation(user))
                                    }
                                    style={{
                                        paddingHorizontal: 16,
                                        paddingLeft: 16,
                                        paddingRight: 42,
                                        backgroundColor: COLORS.GRAY7,
                                        borderWidth: 2,
                                        borderColor: COLORS.lightGRAY3,
                                        borderRadius: 8,
                                        ...FONTS.h4,
                                        color: COLORS.GRAY,
                                        marginBottom: 4,
                                    }}
                                    value={user.nik}
                                    onChangeText={value =>
                                        setUser({ ...user, nik: value })
                                    }
                                />
                                {formErrors?.nik ? (
                                    <Text
                                        style={{
                                            ...FONTS.body6,
                                            color: COLORS.RED600,
                                        }}
                                    >
                                        {formErrors?.nik}
                                    </Text>
                                ) : null}
                            </View>

                            <View style={{ marginBottom: 16 }}>
                                <TextInput
                                    placeholder="Masukkan Nomer Telepon"
                                    onFocus={() =>
                                        setFormErrors(errorsValidation(user))
                                    }
                                    onBlur={() =>
                                        setFormErrors(errorsValidation(user))
                                    }
                                    onChange={() =>
                                        setFormErrors(errorsValidation(user))
                                    }
                                    keyboardType="number-pad"
                                    style={{
                                        paddingHorizontal: 16,
                                        paddingLeft: 16,
                                        paddingRight: 42,
                                        backgroundColor: COLORS.GRAY7,
                                        borderWidth: 2,
                                        borderColor: COLORS.lightGRAY3,
                                        borderRadius: 8,
                                        ...FONTS.h4,
                                        color: COLORS.GRAY,
                                        marginBottom: 8,
                                    }}
                                    value={user.telp}
                                    onChangeText={value =>
                                        setUser({ ...user, telp: value })
                                    }
                                />
                                {formErrors?.telp ? (
                                    <Text
                                        style={{
                                            ...FONTS.body6,
                                            color: COLORS.RED600,
                                        }}
                                    >
                                        {formErrors?.telp}
                                    </Text>
                                ) : null}
                            </View>

                            <View style={{ marginBottom: 16 }}>
                                <TextInput
                                    placeholder="Masukkan Nama Lengkap"
                                    onFocus={() =>
                                        setFormErrors(errorsValidation(user))
                                    }
                                    onBlur={() =>
                                        setFormErrors(errorsValidation(user))
                                    }
                                    onChange={() =>
                                        setFormErrors(errorsValidation(user))
                                    }
                                    style={{
                                        paddingHorizontal: 16,
                                        paddingLeft: 16,
                                        paddingRight: 42,
                                        backgroundColor: COLORS.GRAY7,
                                        borderWidth: 2,
                                        borderColor: COLORS.lightGRAY3,
                                        borderRadius: 8,
                                        ...FONTS.h4,
                                        color: COLORS.GRAY,
                                        marginBottom: 8,
                                    }}
                                    value={user.name}
                                    onChangeText={value =>
                                        setUser({ ...user, name: value })
                                    }
                                />
                                {formErrors?.name ? (
                                    <Text
                                        style={{
                                            ...FONTS.body6,
                                            color: COLORS.RED600,
                                        }}
                                    >
                                        {formErrors?.name}
                                    </Text>
                                ) : null}
                            </View>

                            <View style={{ marginBottom: 16 }}>
                                <TextInput
                                    placeholder="Masukkan Nama Pengguna"
                                    onFocus={() =>
                                        setFormErrors(errorsValidation(user))
                                    }
                                    onBlur={() => {
                                        console.log('Blur');
                                        setFormErrors(errorsValidation(user));
                                    }}
                                    onChange={() =>
                                        setFormErrors(errorsValidation(user))
                                    }
                                    style={{
                                        paddingHorizontal: 16,
                                        paddingLeft: 16,
                                        paddingRight: 42,
                                        backgroundColor: COLORS.GRAY7,
                                        borderWidth: 2,
                                        borderColor: COLORS.lightGRAY3,
                                        borderRadius: 8,
                                        ...FONTS.h4,
                                        color: COLORS.GRAY,
                                        marginBottom: 8,
                                    }}
                                    value={user.username}
                                    onChangeText={value =>
                                        setUser({ ...user, username: value })
                                    }
                                />
                                {formErrors?.username ? (
                                    <Text
                                        style={{
                                            ...FONTS.body6,
                                            color: COLORS.RED600,
                                        }}
                                    >
                                        {formErrors?.username}
                                    </Text>
                                ) : null}
                            </View>

                            <View style={{ marginBottom: 16 }}>
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        alignItems: 'center',
                                        marginBottom: 8,
                                    }}
                                >
                                    <TextInput
                                        placeholder="Kata sandi"
                                        onFocus={() =>
                                            setFormErrors(
                                                errorsValidation(user),
                                            )
                                        }
                                        onBlur={() =>
                                            setFormErrors(
                                                errorsValidation(user),
                                            )
                                        }
                                        onChange={() =>
                                            setFormErrors(
                                                errorsValidation(user),
                                            )
                                        }
                                        style={{
                                            width: '100%',
                                            paddingHorizontal: 16,
                                            backgroundColor: COLORS.GRAY7,
                                            borderWidth: 2,
                                            borderColor: COLORS.lightGRAY3,
                                            borderRadius: 8,
                                            ...FONTS.h4,
                                            color: COLORS.GRAY,
                                        }}
                                        maxLength={11}
                                        secureTextEntry={
                                            isSecureEntry ? true : false
                                        }
                                        keyboardType="default"
                                        value={user.password}
                                        onChangeText={value =>
                                            setUser({
                                                ...user,
                                                password: value,
                                            })
                                        }
                                    />
                                    <TouchableOpacity
                                        onPress={() =>
                                            setIsSecureEntry(!isSecureEntry)
                                        }
                                        style={{
                                            position: 'absolute',
                                            top: '30%',
                                            right: 16,
                                        }}
                                    >
                                        <Text
                                            style={{
                                                ...FONTS.h4,
                                                color: COLORS.GREEN600,
                                            }}
                                        >
                                            {isSecureEntry
                                                ? 'Tampilkan'
                                                : 'Sembunyikan'}
                                        </Text>
                                    </TouchableOpacity>
                                </View>
                                {formErrors?.password ? (
                                    <Text
                                        style={{
                                            ...FONTS.body6,
                                            color: COLORS.RED600,
                                        }}
                                    >
                                        {formErrors?.password}
                                    </Text>
                                ) : null}
                            </View>
                        </View>

                        {/* Button action */}
                        <GreenButton
                            isLoading={isLoading}
                            title="Daftar"
                            onPress={() => {
                                if (Object.keys(formErrors).length === 0) {
                                    handleSubmitSignup();
                                } else {
                                    setMessage(
                                        'Perhatikan ada input yang masih belum diisi',
                                    );
                                    setType(false);
                                    bottomSheetModalRef.current.open();
                                }
                            }}
                        />
                        <TouchableOpacity
                            onPress={() => navigation.navigate('LoginScreen')}
                        >
                            <Text
                                style={{
                                    ...FONTS.body5,
                                    color: COLORS.black,
                                    textAlign: 'center',
                                    marginTop: 8,
                                }}
                            >
                                Sudah memiliki akun?{' '}
                                <Text
                                    style={{
                                        ...FONTS.h6,
                                        color: COLORS.GREEN600,
                                    }}
                                >
                                    Login
                                </Text>
                            </Text>
                        </TouchableOpacity>

                        <StatusBar
                            barStyle="dark-content"
                            backgroundColor={COLORS.WHITE}
                        />
                    </ScrollView>
                </SafeAreaView>
            </BottomSheetModalProvider>
        </KeyboardAvoidWrapper>
    );
};

export default RegisterScreen;
