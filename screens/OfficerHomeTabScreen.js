import React from 'react';
import { View, StatusBar } from 'react-native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { COLORS, FONTS } from '../constants/index';
import {
    ReportFinishContainer,
    ReportProcessContainer,
} from '../components/officer/Report';
import Header from '../components/officer/Header';
import axios from 'axios';
import { setBaseUrl, setDefaultHeader } from '../constants/config';
import { UserContext } from '../context/UserContext';
import SubHeader from '../components/officer/SubHeader';

const Tab = createMaterialTopTabNavigator();

const OfficerHomeTabScreen = () => {
    const { user, setUser } = React.useContext(UserContext);

    const getProfile = async () => {
        setBaseUrl();
        setDefaultHeader();
        try {
            setIsLoading(true);
            const response = await axios.get(`/profile`);

            let item = response.data.results;

            setUser({
                ...user,
                username: item.username,
                nik: item.nik,
                name: item.name,
                telp: item.telp,
                level: item.level,
            });

            setIsLoading(false);
        } catch (error) {
            setIsLoading(false);
        }
    };

    React.useEffect(() => {
        getProfile();
    }, []);

    return (
        <View
            style={{ flex: 1, backgroundColor: COLORS.WHITE, paddingTop: 16 }}
        >
            <StatusBar barStyle="dark-content" backgroundColor={COLORS.WHITE} />
            <Header />
            <SubHeader />
            <Tab.Navigator
                screenOptions={{
                    tabBarLabelStyle: { ...FONTS.h7 },
                    tabBarIndicatorStyle: {
                        borderWidth: 2,
                        borderColor: COLORS.GREEN600,
                        borderRadius: 9999,
                    },
                }}
            >
                <Tab.Screen name="Proses" component={ReportProcessContainer} />
                <Tab.Screen
                    name="Sudah diTanggapi"
                    component={ReportFinishContainer}
                />
            </Tab.Navigator>
        </View>
    );
};

export default OfficerHomeTabScreen;
