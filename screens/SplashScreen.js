import React from 'react';
import { SafeAreaView, StatusBar, Text } from 'react-native';
import { COLORS, FONTS, ICONS } from '../constants';
import AsyncStorage from '@react-native-async-storage/async-storage';

const SplashScreen = ({ navigation }) => {
    React.useEffect(() => {
        (async () => {
            let isLogin = await AsyncStorage.getItem('@isLogin');
            let level = await AsyncStorage.getItem('@level');

            if (isLogin === '1' && level === 'masyarakat') {
                setTimeout(() => {
                    navigation.replace('HomeScreen');
                }, 500);
            } else if (isLogin === '1' && level === 'petugas') {
                setTimeout(() => {
                    navigation.replace('OfficerHomeScreen');
                }, 500);
            } else if (isLogin === '1' && level === 'admin') {
                setTimeout(() => {
                    navigation.replace('AdminHomeScreen');
                }, 500);
            } else {
                setTimeout(() => {
                    navigation.replace('LoginScreen');
                }, 1500);
            }
        })();
    }, []);

    return (
        <SafeAreaView
            style={{
                flex: 1,
                backgroundColor: COLORS.WHITE,
                justifyContent: 'center',
                alignItems: 'center',
            }}
        >
            <StatusBar
                barStyle="light-content"
                backgroundColor={COLORS.WHITE}
            />
            <ICONS.LAPORPAK width={111} height={145} />

            <Text
                style={{
                    ...FONTS.h13,
                    color: COLORS.GRAY,
                    position: 'absolute',
                    bottom: 24,
                }}
            >
                V 1.1.8
            </Text>
        </SafeAreaView>
    );
};

export default SplashScreen;
