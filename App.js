import React from 'react';
import { Easing } from 'react-native';
import {
    createStackNavigator,
    CardStyleInterpolators,
} from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import SplashScreen from './screens/SplashScreen';
import {
    FormReportScreen,
    HomeScreen,
    LoginScreen,
    RegisterScreen,
    SettingsScreen,
} from './screens';
import { COLORS, FONTS } from './constants';
import ReportProvider from './context/ReportContext';
import { FormCreateHeader } from './components/FormCreate';
import UserProvider from './context/UserContext';
import { enableScreens } from 'react-native-screens';
import CodePush from 'react-native-code-push';
import { SettingsHeader } from './components/Settings';
import HeaderLeftBack from './components/HeaderLeftBack';
import { setBaseUrl } from './constants/config';
import OfficerHomeTabScreen from './screens/OfficerHomeTabScreen';
import FreeHomeTabScreen from './screens/FreeHomeTabScreen';
import SearchScreen from './screens/Free/SearchScreen';
import AdminHomeTabScreen from './screens/AdminHomeTabScreen';
import HomeTabScreen from './screens/HomeTabScreen';
import moment from 'moment';
import 'moment/locale/id';
import HeaderLeftBackWhite from './components/HeaderLeftBackWhite';
import WebViewHeader from './components/WebView/WebViewHeader';
import WebViewComponent from './components/WebView/WebViewComponent';
moment.locale('id');
const Stack = createStackNavigator();
const CODE_PUSH_OPTION = {
    checkFrequency: CodePush.CheckFrequency.ON_APP_RESUME,
    mandatoryInstallMode: CodePush.InstallMode.IMMEDIATE,
    updateDialog: {
        appendReleaseDescription: true,
        title: 'Pembaruan tersedia!',
    },
};

const App = () => {
    enableScreens(false);
    const config = {
        animation: 'spring',
        config: {
            stiffness: 350,
            damping: 50,
            mass: 3,
            overshootClamping: false,
            restDisplacementThreshold: 0.01,
            restSpeedThreshold: 0.01,
        },
    };
    const closeConfig = {
        animation: 'timing',
        config: {
            duration: 200,
            easing: Easing.linear,
        },
    };

    React.useEffect(() => {
        setBaseUrl();
    }, []);

    React.useEffect(() => {
        CodePush.allowRestart();
        CodePush.checkForUpdate().then(update => {
            if (update) {
                CodePush.sync({
                    updateDialog: true,
                    installMode: CodePush.InstallMode.IMMEDIATE,
                    mandatoryInstallMode: CodePush.InstallMode.IMMEDIATE,
                });
            }
        });
        CodePush.sync({
            updateDialog: true,
            installMode: CodePush.InstallMode.IMMEDIATE,
            mandatoryInstallMode: CodePush.InstallMode.IMMEDIATE,
        });
    }, []);

    const make = 'For';
    const model = 'You=';
    const car = { make, model };
    console.log(car);

    return (
        <UserProvider>
            <ReportProvider>
                <NavigationContainer>
                    <Stack.Navigator initialRouteName={'SplashScreen'}>
                        <Stack.Screen
                            name="SplashScreen"
                            component={SplashScreen}
                            options={{ headerShown: false }}
                        />
                        <Stack.Screen
                            name="LoginScreen"
                            component={LoginScreen}
                            options={{ headerShown: false }}
                        />
                        <Stack.Screen
                            name="RegisterScreen"
                            component={RegisterScreen}
                            options={({ navigation }) => ({
                                headerShown: true,
                                headerTitle: 'Daftar',
                                headerTitleStyle: {
                                    ...FONTS.h24Bold,
                                    color: COLORS.black,
                                    textAlign: 'center',
                                },
                                headerTitleAlign: 'center',
                                headerStyle: {
                                    backgroundColor: COLORS.white,
                                },
                                headerLeft: () => <HeaderLeftBack />,
                                headerLeftContainerStyle: {
                                    paddingLeft: 24,
                                },
                            })}
                        />
                        <Stack.Screen
                            name="FormReportSreen"
                            component={FormReportScreen}
                            options={({ navigation }) => ({
                                headerShown: true,
                                headerTitle: () => <FormCreateHeader />,
                                headerTitleStyle: {
                                    ...FONTS.h5,
                                    color: COLORS.BLACK,
                                },
                                headerStyle: {
                                    backgroundColor: COLORS.WHITE,
                                },
                                headerTitleAlign: 'center',
                                headerLeft: () => <HeaderLeftBack />,
                                headerLeftContainerStyle: {
                                    paddingLeft: 16,
                                },
                            })}
                        />
                        <Stack.Screen
                            name="SettingsScreen"
                            component={SettingsScreen}
                            options={({ navigation }) => ({
                                headerShown: true,
                                headerTitle: () => <SettingsHeader />,
                                headerTitleStyle: {
                                    ...FONTS.h5,
                                    color: COLORS.BLACK,
                                },
                                headerStyle: {
                                    backgroundColor: COLORS.WHITE,
                                },
                                headerTitleAlign: 'center',
                                headerLeft: () => <HeaderLeftBack />,
                                headerLeftContainerStyle: {
                                    paddingLeft: 16,
                                },
                            })}
                        />

                        <Stack.Screen
                            name="WebViewComponent"
                            component={WebViewComponent}
                            options={({ navigation }) => ({
                                headerShown: true,
                                headerTitle: () => <WebViewHeader />,
                                headerTitleStyle: {
                                    ...FONTS.h5Bold,
                                    color: COLORS.WHITE,
                                },
                                headerStyle: {
                                    backgroundColor: COLORS.GREEN600,
                                },
                                headerTitleAlign: 'center',
                                headerLeft: () => <HeaderLeftBackWhite />,
                                headerLeftContainerStyle: {
                                    paddingLeft: 16,
                                },
                            })}
                        />

                        <Stack.Screen
                            name="HomeScreen"
                            component={HomeTabScreen}
                            options={{ headerShown: false }}
                        />

                        {/* Officers */}
                        <Stack.Screen
                            name="OfficerHomeScreen"
                            component={OfficerHomeTabScreen}
                            options={{ headerShown: false }}
                        />

                        {/* Admin */}
                        <Stack.Screen
                            name="AdminHomeScreen"
                            component={AdminHomeTabScreen}
                            options={{ headerShown: false }}
                        />

                        {/* Free */}
                        <Stack.Screen
                            name="FreeHomeScreen"
                            component={FreeHomeTabScreen}
                            options={{ headerShown: false }}
                        />
                        <Stack.Screen
                            name="SearchScreen"
                            component={SearchScreen}
                            options={{ headerShown: false }}
                        />
                    </Stack.Navigator>
                </NavigationContainer>
            </ReportProvider>
        </UserProvider>
    );
};

export default CodePush(CODE_PUSH_OPTION)(App);
